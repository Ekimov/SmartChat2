package com.smartchat.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.protobuf.InvalidProtocolBufferException;
import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.server.SmartChat;
import com.smartchat.server.WebSocketClient;
import com.smartchat.utils.AppConst;

import org.apache.http.message.BasicNameValuePair;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MainService extends Service {

    private WebSocketClient client;
    private SharedPreferences sharedPreferences;
    private String accessToken;
    private Handler handler;
    private SmartChat.Message message;
    private NotificationManager notificationManager;
    private ArrayList<FullMessageObject> fullMessage = new ArrayList<>();
    private FullMessageObject fullMessageObject;
    private SmartChat.PUUID user_puuid;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler(Looper.getMainLooper());
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Log.i("ST27i", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sharedPreferences = getApplicationContext().getSharedPreferences(AppConst.SMART_CHAT, getApplicationContext().MODE_PRIVATE);

        accessToken = sharedPreferences.getString("accessToken", "");

        UUID uuid = UUID.fromString(sharedPreferences.getString("userId", ""));

        user_puuid = SmartChat.PUUID.newBuilder()
                .setMostSignificantBits(uuid.getMostSignificantBits())
                .setLeastSignificantBits(uuid.getLeastSignificantBits()).build();

        if (accessToken.isEmpty()){
            //TODO STOP SERVICE
        }
        else {
            command();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void command() {

        if (accessToken.isEmpty()){

        }
        else {

            List<BasicNameValuePair> extraHeaders = Arrays.asList(
                    new BasicNameValuePair("Cookie", "session=abcd")
            );

            client = new WebSocketClient(URI.create("ws://54.93.222.229/api/v1/chat?accessToken=" + accessToken), new WebSocketClient.Listener() {
                @Override
                public void onConnect() {
                    Log.i("ST27i", "Connect");
                }

                @Override
                public void onMessage(String message) {
                    Log.i("ST27i", "String");
                }

                @Override
                public void onMessage(byte[] data) {
                    Log.i("ST27i", "Byte");

                    SmartChat.Response response = null;
                    try {
                        response = SmartChat.Response.parseFrom(data);
                    } catch (InvalidProtocolBufferException e) {
                        e.printStackTrace();
                    }



                    switch (response.getType()){

                        case MESSAGE:
                            fullMessageObject = new FullMessageObject();
                            try {
                                message = SmartChat.Message.parseFrom(response.getResponse());
                            } catch (InvalidProtocolBufferException e) {
                                e.printStackTrace();
                            }
                            if (message.getId().toString().equals(user_puuid.toString())){

                            }
                            else {
                                fullMessageObject.message = message.getBodies(0).getText();
                                fullMessageObject.puuid = message.getUserFrom();
                                fullMessage.add(fullMessageObject);
                                SmartChat.Request request = SmartChat.Request.newBuilder()
                                        .setType(SmartChat.CallType.GET_USER_BY_ID)
                                        .setRequest(SmartChat.GetUserByIdRequest.newBuilder().setUserId(message.getUserFrom()).build().toByteString())
                                        .build();

                                client.send(request.toByteArray());
                            }

                            break;
                        case GET_USER_BY_ID:
                            SmartChat.GetUserByIdResponse getUserByIdResponse = null;
                            try {
                                getUserByIdResponse = SmartChat.GetUserByIdResponse.parseFrom(response.getResponse());
                            } catch (InvalidProtocolBufferException e) {
                                e.printStackTrace();
                            }

                            SmartChat.User user = getUserByIdResponse.getUser();

                            Log.e("RESP", user.getNick());
                            Log.e("RESP", user.getFirstName());
                            Log.e("RESP", user.getLastName());

                            final String first_name = user.getFirstName()+" "+user.getLastName();
                            final String last_name = user.getFirstName()+" "+user.getLastName();

                            int fullMessageSize = fullMessage.size();
                            for (int i = 0; i < fullMessageSize; i++) {
                                if (user.getId().toString().equalsIgnoreCase(fullMessage.get(i).puuid.toString())){
                                    final String txt = fullMessage.get(i).message;
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            sendNotification(first_name, last_name, txt);
                                        }
                                    });

                                }
                            }
                            fullMessage.remove(0);
                            break;

                    }

                }

                private void sendNotification(String first_name, String last_name, String message) {

                    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setAutoCancel(true)
                            .setSound(alarmSound)
                            .setVibrate(new long[]{1000, 1000, 1000})

                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

                    if (first_name.isEmpty()&&last_name.isEmpty()){
                        mBuilder.setContentTitle("New message!");
                    }
                    else {
                        mBuilder.setContentTitle("New message from "+first_name+" "+last_name+"!");
                    }

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

                    mBuilder.setContentIntent(pIntent);

                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                @Override
                public void onDisconnect(int code, String reason) {
                    Log.i("ST27i", "Disconnect");
                }

                @Override
                public void onError(Exception error) {
                    Log.i("ST27i", "Error");
                    error.printStackTrace();
                }
            }, extraHeaders);
        }
        client.connect();
    }

    private class FullMessageObject{
        SmartChat.PUUID puuid;
        String message;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("ST27i", "Disconnect");
        client.disconnect();
    }
}

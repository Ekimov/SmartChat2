package com.smartchat.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.smartchat.objects.PostObject;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.LogTag;

import de.greenrobot.event.EventBus;

public class SMSReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";

    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            for (int i = 0; i < sms.length; ++i) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                String smsBody = smsMessage.getMessageBody().toString();
                String address = smsMessage.getOriginatingAddress();

                if (address.equalsIgnoreCase(AppConst.SMS_ADDRESS)){
                    updateUI(smsBody);
                }

                smsMessageStr += "SMS From: " + address + "\n";
                smsMessageStr += smsBody + "\n";
            }

            LogTag.d(smsMessageStr);

        }
    }

    private void updateUI(String smsBody){

        PostObject postObject = new PostObject();
        postObject.setObject(smsBody);
        postObject.setKey(AppConst.SMS_KEY);

        EventBus.getDefault().post(postObject);

    }
}
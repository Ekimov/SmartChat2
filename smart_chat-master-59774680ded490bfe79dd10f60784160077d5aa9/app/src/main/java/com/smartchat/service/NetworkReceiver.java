package com.smartchat.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkReceiver extends BroadcastReceiver{
    private static boolean connect;

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
            if (!connect) {
                Log.i("ST27i", "connected");
                connect = true;
                context.startService(new Intent(context, MainService.class));
            }

        }
        else {
            Log.i("ST27i", "disconnected");
            connect=false;
            context.stopService(new Intent(context, MainService.class));
        }

    }
}

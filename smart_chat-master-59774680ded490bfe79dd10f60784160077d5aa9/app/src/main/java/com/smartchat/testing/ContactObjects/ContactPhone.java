package com.smartchat.testing.ContactObjects;

public class ContactPhone {
	public String number;
	public String type;

	public ContactPhone(String number, String type) {
		this.number = number;
		this.type = type;
	}

    public String getNumber(){
        return this.number;
    }
}

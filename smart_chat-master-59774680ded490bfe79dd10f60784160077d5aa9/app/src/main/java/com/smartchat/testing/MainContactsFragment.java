package com.smartchat.testing;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

//import Server.breezy.Activity.BaseFragment;
//import Server.breezy.Activity.MainActivity;
//import Server.breezy.DebugFish.ContactItem;
//import Server.breezy.Objects.Auth;
//import Server.breezy.Objects.Contact;
//import Server.breezy.Objects.UserAuth;
//import Server.breezy.R;
//import Server.breezy.Server.ConnectHelper;
//import Server.breezy.Server.SmartChat;

import com.google.protobuf.InvalidProtocolBufferException;
import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.objects.Auth;
import com.smartchat.objects.Contact;
import com.smartchat.objects.PostObject;
import com.smartchat.objects.UserAuth;
import com.smartchat.objects.UserShort;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.testing.ContactObjects.ContactEmail;
import com.smartchat.testing.ContactObjects.ContactPhone;
import com.smartchat.testing.ContactObjects.MainContact;

import java.util.ArrayList;
import java.util.HashMap;

//import Testing.ContactObjects.ContactPhone;
//import Testing.ContactObjects.MainContact;
import de.greenrobot.event.EventBus;

/**
 * Created by Genka!  on 08.04.15.
 */
public class MainContactsFragment extends Fragment {

       /* -------Data for loading and adding new contacts -------*/

    private HashMap<String, Integer> myPhoneParsingAuth = new HashMap<>();
    private  HashMap<SmartChat.PUUID, Contact> myAppContactsResponseAuth = new HashMap<>();
    private ArrayList<UserAuth> authArrayFromServer = new ArrayList<UserAuth>();

    public ArrayList<Contact> tempPhoneContacts = new ArrayList<Contact>();
    public ArrayList<Contact> myAppContactsResponse = new ArrayList<>();
    ArrayList<MainContact> listContacts;

     /* -------Initialize new view -------*/

    ListView contactListView, contactListView2;
    private View view;
    TextView textview, textview2;
    private ConnectHelper connectHelper = MainActivity.connectHelper;

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.testing_main_fragment, null);
        Log.d("Genka", "----------- Initialize view elements, starting fragment -----------");
        initView();
        //connectHelper.getAllContacts();

        loadOwnContact();

        return view;
    }

    /* -------Check response from server!-------*/
    public void onEventMainThread(Object object) {
        PostObject postObject = (PostObject) object;

        switch (postObject.getKey()) {
            case SmartChat.CallType.CONTACTS_GET_VALUE:
                Log.d("Genka", "Response --- Get all contacts from server: Successful!  ---");
                myAppContactsResponse.clear();
                myAppContactsResponse.addAll((ArrayList<Contact>) postObject.getObject());
                break;
            case SmartChat.CallType.SEARCH_USERS_BY_AUTH_VALUE:
                Log.d("Genka", "Response --- Start writing contacts on server: Successful!  ---");
                authArrayFromServer.clear();
                authArrayFromServer.addAll((ArrayList<UserAuth>) postObject.getObject());
                Log.d("Genka", "Response --- Find contacts Size" + authArrayFromServer.size());
                for (int x = 0; x < authArrayFromServer.size(); x++){
                    Log.d("Genka", "Response --- Start " +   authArrayFromServer.get(x).getUserAuth());
                }
                break;

        }
    }

    /* -------Initialize new view elements-------*/
    private void initView(){

        textview = (TextView) view.findViewById(R.id.loading);
        textview2 = (TextView) view.findViewById(R.id.loading2);
        contactListView = (ListView) view.findViewById(R.id.list_view);
        contactListView2 = (ListView) view.findViewById(R.id.list_view2);
        contactListView.setVisibility(View.GONE);
        contactListView2.setVisibility(View.GONE);

    }

    /* -------Loading my own contact from ContactBook -------*/
    private void loadOwnContact(){
        Log.d("Genka", " ( --- Getting my own contacts! ---");
        listContacts = new ContactFetcher(getActivity()).fetchAll();
        ContactsAdapter adapterContacts = new ContactsAdapter(getActivity(), listContacts);
        contactListView.setAdapter(adapterContacts);

        writeContactsOnServer();
        contactListView.setVisibility(View.VISIBLE);
        textview.setVisibility(View.GONE);
    }

    /* ------- Write contacts on server!  -------*/
    private void writeContactsOnServer(){
        Log.d("Genka", "---  Starting write contacts on server ---");
        ArrayList<Auth> arrayAuthTo = new ArrayList<>();

        for (int x = 0; x < listContacts.size(); x++) {
            ArrayList<ContactPhone> contactPhones=  listContacts.get(x).getNumbers();
                    for (int i = 0; i < contactPhones.size(); i++ ) {
                            arrayAuthTo.add(new Auth(SmartChat.AuthType.PHONE_AUTH,
                                    contactPhones.get(i).getNumber()));
                        Log.d("Genka", "- Creating authoring request: Phone number working-");
                    }
        }
        for (int x = 0; x < listContacts.size(); x++) {
            ArrayList<ContactEmail> contactEmails=  listContacts.get(x).getEmails();
            for (int i = 0; i < contactEmails.size(); i++){
                arrayAuthTo.add(new Auth(SmartChat.AuthType.PHONE_AUTH,
                        contactEmails.get(i).getAddress()));
                Log.d("Genka", "- Creating authoring request: Email working -");
            }

        }


        Log.d("Genka", "- Start Request writing on server-");
        connectHelper.searchUsersByAuthRequest(arrayAuthTo);
    }

















}

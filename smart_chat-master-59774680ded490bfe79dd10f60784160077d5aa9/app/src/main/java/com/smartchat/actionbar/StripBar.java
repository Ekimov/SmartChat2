package com.smartchat.actionbar;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.utils.AppConst;

public class StripBar {

    private View view, strip_line_1, strip_line_2, strip_line_3;
    private TextView strip_text_1, strip_text_2, strip_text_3;
    private Context context;
    private Animation animation;
    private View[] views;
    private TextView[] textViews;


    public StripBar(Context context, View view, int number){

        this.view = view;
        this.context = context;
        init();
        setStrips(number);
    }

    private void init(){

        strip_text_1 = (TextView) view.findViewById(R.id.strip_text_1);
        strip_text_2 = (TextView) view.findViewById(R.id.strip_text_2);
        strip_text_3 = (TextView) view.findViewById(R.id.strip_text_3);

        strip_line_1 = view.findViewById(R.id.strip_line_1);
        strip_line_2 = view.findViewById(R.id.strip_line_2);
        strip_line_3 = view.findViewById(R.id.strip_line_3);

        animation = AnimationUtils.loadAnimation(context, R.anim.strip_line_animation);
        animation.setDuration(200);

        views = new View[]{strip_line_1, strip_line_2, strip_line_3};
        textViews = new TextView[]{strip_text_1, strip_text_2, strip_text_3};
    }

    private void setStrips(int number){

        switch (number){

            case AppConst.CHATS:
                strip_text_1.setText(AppConst.CHATS_STRIP[0]);
                strip_text_2.setText(AppConst.CHATS_STRIP[1]);
                strip_text_3.setText(AppConst.CHATS_STRIP[2]);
                break;
            case AppConst.LISTS:
                strip_text_1.setText(AppConst.LISTS_STRIP[0]);
                strip_text_2.setText(AppConst.LISTS_STRIP[1]);
                strip_text_3.setText(AppConst.LISTS_STRIP[2]);
                break;
            case AppConst.CONTACTS:
                strip_text_1.setText(AppConst.APP_CONTACTS);
                strip_text_2.setText(AppConst.PHONE_CONTACTS);
        }

    }

    public void changeStrip(int number){

        int size = textViews.length;

        for(int i = 0; i < size; i++){
            views[i].setVisibility(View.INVISIBLE);
            textViews[i].setTextColor(context.getResources().getColor(R.color.day_no_active_text));
            textViews[i].setTypeface(null, Typeface.NORMAL);
        }

        views[number].setVisibility(View.VISIBLE);
        views[number].setAnimation(animation);
        views[number].startAnimation(animation);

        textViews[number].setTextColor(context.getResources().getColor(R.color.day_action_bar));
        textViews[number].setTypeface(null, Typeface.BOLD);


    }
}

package com.smartchat.actionbar;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.smartchat.R;
import com.smartchat.utils.AppConst;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

public class TitleBar {

    private TextView title;
    private ImageView title_image_left, title_image_right;

    public TitleBar(TextView title, ImageView title_image_left, ImageView title_image_right, int number){

        this.title = title;
        this.title_image_left = title_image_left;
        this.title_image_right = title_image_right;

        change(number);
    }

    private void change(int number){

        switch (number){

            case AppConst.CHATS:
                title.setText("Welcome to Smartchat");
                title_image_left.setImageResource(R.mipmap.ic_profile_icon_about_me);
                title_image_right.setImageResource(R.mipmap.ic_playlist_add);
                break;
            case AppConst.LISTS:
                title.setText("SmartLists");
/*                title_image_left.setImageDrawable();*/
                title_image_left.setImageResource(R.mipmap.ic_playlist_add);
                break;
            case AppConst.FAVORITES:
                title.setText("Favorites");
                title_image_left.setImageResource(R.mipmap.ic_playlist_add);
                title_image_right.setImageResource(R.drawable.ic_search);
                break;
            case AppConst.CONTACTS:
                title.setText("Contacts");
/*                title_image_left.setImageDrawable();*/
                title_image_right.setImageResource(R.drawable.ic_search);
                break;
            case AppConst.PROFILE:
                title.setText("Profile");
/*                title_image_left.setImageDrawable();
                title_image_right.setImageResource(R.drawable.ic_search);*/
                break;
        }
    }





}

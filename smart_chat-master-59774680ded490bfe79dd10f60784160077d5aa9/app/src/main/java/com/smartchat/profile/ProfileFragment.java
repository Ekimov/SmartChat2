package com.smartchat.profile;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.objects.Auth;
import com.smartchat.objects.PostObject;
import com.smartchat.objects.Profile;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.ImageHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.CustomToast;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

//TODO URI

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private final static int TYPE_CAMERA_PICK = 113;
    private final static int TYPE_GALLERY_PICK = 213;
    private final static int TYPE_PHOTO_CROP = 313;
    public static String MY_TAG = "PROFILE";
    public boolean firstStart;
    private View view;
    private TextView actionBarName, saveButton, addAvatar;
    private ImageView backProfile, avatarImage, avatarImageBackground;
    private ProgressBar progressBar;
    private Profile profile;
    private Auth profileAuth;
    private ConnectHelper connectHelper = ((MainActivity) getActivity()).connectHelper;
    private ArrayList<SmartChat.Field> fieldsArray;
    private Uri uri, uriEdit;
    private ImageLoader imageLoader;
    private EditText nickname, aboutMe, email, city;
    private ImageView male, female;
    private String MALE = "male";
    private String FEMALE = "female";
    private String sex = "";
    private ToggleButton nightMode;
    private String accessToken = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile_profile_fragment, null);

        ((MainActivity)getActivity()).menuBarIsGone();

        init();
        Functions.getNightMode(getActivity(), nightMode);
        accessToken = getActivity().getSharedPreferences(AppConst.SMART_CHAT, Context.MODE_PRIVATE)
                .getString("accessToken", "");

        imageLoader = ImageLoader.getInstance();
        progressBar.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(R.color.day_white), PorterDuff.Mode.SRC_IN);
        progressBar.setVisibility(View.VISIBLE);

        if(firstStart){
            CustomToast.show(getActivity(), view.getResources().getString(R.string.toast_first_run));
        }

        connectHelper.getAuthRequest();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void init() {
        avatarImageBackground = (ImageView) view.findViewById(R.id.profile_avatar_background_img);
        avatarImage = (ImageView) view.findViewById(R.id.profile_avatar_img);

        addAvatar = (TextView) view.findViewById(R.id.profile_add_photo_text);
        addAvatar.setOnClickListener(this);

        backProfile = (ImageView) view.findViewById(R.id.profile_back_button);
        backProfile.setOnClickListener(this);

        actionBarName = (TextView) view.findViewById(R.id.profile_action_bar);

        saveButton = (TextView) view.findViewById(R.id.profile_save_button);
        saveButton.setOnClickListener(this);

        progressBar = (ProgressBar) view.findViewById(R.id.profile_progress);
        progressBar.setIndeterminate(true);

        nickname = (EditText) view.findViewById(R.id.profile_nickname_text);
        aboutMe = (EditText) view.findViewById(R.id.profile_about_me);
        email = (EditText) view.findViewById(R.id.profile_email);
        city = (EditText) view.findViewById(R.id.profile_city);

        male = (ImageView) view.findViewById(R.id.profile_male);
        female = (ImageView) view.findViewById(R.id.profile_female);
        male.setOnClickListener(this);
        female.setOnClickListener(this);

        nightMode = (ToggleButton) view.findViewById(R.id.profile_night_mode);
        nightMode.setOnClickListener(this);
    }

    public void onEventMainThread(Object object) {
        PostObject postObject = (PostObject) object;

        switch (postObject.getKey()) {
            case AppConst.IMAGE_STATUS:
                Log.d(MY_TAG, "IMAGE_STATUS  -> " + postObject);
                String resp = (String) postObject.getObject();
                if (resp.equals(AppConst.IMAGE_ERROR)) {
                    CustomToast.show(getActivity().getApplicationContext(), "Error loading image!");
                    return;
                }
                String link = ImageHelper.getDownloadUrl(resp, accessToken);
                imageLoader.displayImage(link, avatarImage, Functions.getDisplayImageOptions(), new ImageLoaderBlur());
                addFields(link, SmartChat.FieldType.IMAGE_LINK);
                Functions.fileDelbyUri(getActivity(), uriEdit);
                return;
            case SmartChat.CallType.SET_PROFILE_FIELD_VALUE:
                boolean isSave = (boolean) postObject.getObject();
                if (isSave) {
                    CustomToast.show(getActivity().getApplicationContext(), "Your settings have been saved!");
                } else {
                    CustomToast.show(getActivity().getApplicationContext(), "Sorry, your settings are not saved, try again!");
                }
                return;
            case SmartChat.CallType.PROFILE_GET_VALUE:
                progressBar.setVisibility(View.INVISIBLE);
                profile = (Profile) postObject.getObject();
                Log.d(MY_TAG, "GET VAL  -> " + profile);
                setProfileField();
                return;
            case SmartChat.CallType.AUTH_GET_VALUE:
                profileAuth = (Auth) postObject.getObject();
                Log.d(MY_TAG, "GET AUTH  -> " + profileAuth);
                setAuthField();
        }
    }

    private void setAuthField() {
        if (profileAuth != null) {

            if ((profileAuth.getType() == SmartChat.AuthType.PHONE_AUTH) && (!profileAuth.getValue().isEmpty())) {
                //       phone.setText(profileAuth.getValue());
            }
            if ((profileAuth.getType() == SmartChat.AuthType.EMAIL_AUTH) && (!profileAuth.getValue().isEmpty())) {
                email.setText(profileAuth.getValue());
            }
        }

        connectHelper.profileFieldGetRequest();
    }

    private void setProfileField() {

        if (profile == null) {
            profile = new Profile();
        }

        if (!profile.getNickName().isEmpty()) {
            nickname.setText(profile.getNickName());
            actionBarName.setText(profile.getNickName());
        }

        if (!profile.getAbout().isEmpty()) {
            aboutMe.setText(profile.getAbout());
        }

        if (!profile.getCity().isEmpty()) {
            city.setText(profile.getCity());
        }

        if (!profile.getSex().isEmpty()) {
            selectSex(profile.getSex());
        }

        if (!profile.getImageLink().isEmpty()) {
            imageLoader.displayImage(profile.getImageLink(), avatarImage, Functions.getDisplayImageOptions(), new ImageLoaderBlur());
        }
    }

    private void saveField() {

        String nickname = this.nickname.getText().toString();

        String city = this.city.getText().toString();

        String sex = this.sex;

        String email = this.email.getText().toString();

        String aboutme = this.aboutMe.getText().toString();

        if (!nickname.isEmpty()) {
            addFields(nickname, SmartChat.FieldType.NICKNAME);
        } else {
            CustomToast.show(getActivity(), view.getResources().getString(R.string.toast_first_run));
            return;
        }

        if (!sex.isEmpty()) {
            addFields(sex, SmartChat.FieldType.SEX);
        }

        if (!city.isEmpty())
            addFields(city, SmartChat.FieldType.CITY);

        if (!aboutme.isEmpty())
            addFields(aboutme, SmartChat.FieldType.ABOUT);

        if (!email.isEmpty()) {
            connectHelper.authAddRequest(SmartChat.AuthType.EMAIL_AUTH, email);
        }

        connectHelper.setAllFieldsRequest(fieldsArray);

        if(firstStart){
            CustomToast.show(getActivity(), "WELCOME");
        }
    }

    private void addFields(String value, SmartChat.FieldType fieldType) {
        if (fieldsArray == null) {
            fieldsArray = new ArrayList<>();
        }
        fieldsArray.add(SmartChat.Field.newBuilder().setType(fieldType).setValue(value).build());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_back_button:
                getActivity().onBackPressed();
                break;
            case R.id.profile_save_button:
                saveField();
                break;
            case R.id.profile_add_photo_text:
                selectAvatar();
                break;
            case R.id.profile_male:
                selectSex(MALE);
                break;
            case R.id.profile_female:
                selectSex(FEMALE);
                break;
            case R.id.profile_night_mode:
                Functions.setNightMode(getActivity(), nightMode.isChecked());
                break;
        }
    }

    //region Avatar

    private void selectAvatar() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(getResources().getString(R.string.add_image_alert));

        alertDialog.setMessage(getResources().getString(R.string.choose_alert));

        //alertDialog.setIcon(R.drawable.ic_media);

        alertDialog.setPositiveButton(getResources().getString(R.string.make_alert),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, System.currentTimeMillis() + ".jpg");
                        uri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        startActivityForResult(intent, TYPE_CAMERA_PICK);
                    }
                });

        alertDialog.setNegativeButton(getResources().getString(R.string.gallery_alert),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, TYPE_GALLERY_PICK);
                    }
                });

        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case TYPE_CAMERA_PICK:
                    progressBar.setVisibility(View.VISIBLE);
                    startImageEditor();
                    break;
                case TYPE_GALLERY_PICK:
                    progressBar.setVisibility(View.VISIBLE);
                    uri = Uri.parse(data.getData().toString());
                    startImageEditor();
                    break;
                case TYPE_PHOTO_CROP:
                    progressBar.setVisibility(View.VISIBLE);
                    Functions.avatarUpload(getActivity(), accessToken, new PostObject(), uriEdit);
                    break;
            }
        }
    }

    private void startImageEditor() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, System.currentTimeMillis() + "_edit.jpg");
        uriEdit = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        int[] aspect = Functions.size(view.getContext(), 140);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", aspect[0]);
        intent.putExtra("aspectY", aspect[1]);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriEdit);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, TYPE_PHOTO_CROP);
    }

    //endregion

    private void selectSex(String gender) {
        if (gender.equals(MALE)) {
            male.setImageResource(R.mipmap.ic_profile_icon_male_on);
            female.setImageResource(R.mipmap.ic_profile_icon_woman_off);
        } else if (gender.equals(FEMALE)) {
            female.setImageResource(R.mipmap.ic_profile_icon_woman_on);
            male.setImageResource(R.mipmap.ic_profile_icon_male_off);
        }
        sex = gender;
    }


    public class ImageLoaderBlur extends SimpleImageLoadingListener {
        @Override
        public void onLoadingStarted(String imageUri, View view) {
            super.onLoadingStarted(imageUri, view);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            Bitmap blurLoadedImage = Blur.doBlur(loadedImage, 20, false);
            progressBar.setVisibility(View.INVISIBLE);
            avatarImageBackground.setImageBitmap(blurLoadedImage);

            super.onLoadingComplete(imageUri, view, loadedImage);
        }
    }
}

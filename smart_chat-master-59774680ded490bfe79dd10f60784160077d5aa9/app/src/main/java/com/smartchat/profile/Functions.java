package com.smartchat.profile;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ToggleButton;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.smartchat.activity.MainActivity;
import com.smartchat.objects.PostObject;
import com.smartchat.server.ImageHelper;
import com.smartchat.utils.AppConst;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Functions {

    public static DisplayImageOptions getDisplayImageOptions() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
//                .showImageForEmptyUri(R.drawable.ic_launcher)
//                .showImageOnFail(R.drawable.ic_launcher)
//                .showImageOnLoading(R.drawable.ic_launcher)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        return options;
    }


    public static String getUTC(){
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();
        return "UTC +" +  TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS);
    }

    public static String getLang(){
        String _lang = Locale.getDefault().getDisplayLanguage();
        if ((_lang == null) || (_lang.length() == 0)) {
            return "";
        }

        return Character.toUpperCase(_lang.charAt(0)) + (_lang.length() > 1 ? _lang.substring(1) : "") ;
    }

    public static String getCountry(Context context){
        return context.getResources().getConfiguration().locale.getDisplayCountry();
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (NullPointerException e){
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contentUri.toString();
    }

    public static void setNightMode(Activity activity, boolean isNight){
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(AppConst.NIGHT_MODE, isNight);
        editor.commit();
    }

    public static void getNightMode(Activity activity, ToggleButton toggleButton){
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        boolean isNight = sharedPreferences.getBoolean(AppConst.NIGHT_MODE, false);
        Log.d(ProfileFragment.MY_TAG, "NIGHT_MODE  -> " + isNight + " night");
        toggleButton.setChecked(isNight);
    }


    public  static int[] size(final Context context, final float dp) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        float height = dp * context.getResources().getDisplayMetrics().density;
        return new int[] {(int)(width / height), 1};
    }


    public static void fileDelbyUri(Activity activity, Uri uri){
        if (uri != null) {
            activity.getContentResolver().delete(uri, null, null);
        }
    }

    public static void avatarUpload(Activity activity, String accessToken, PostObject object, Uri uri){

        if(accessToken.isEmpty()) {
            return;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        String path = getRealPathFromURI(activity.getApplicationContext(), uri);

        ImageHelper img = new ImageHelper();
        img.uploadImage(object, accessToken, path, activity.getContentResolver().getType(uri), screenWidth, screenHeight);
    }

}

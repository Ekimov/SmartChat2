package com.smartchat.videolists;

import android.graphics.Bitmap;

import java.io.File;

public class ChildContent {

    private String description, textItem;
    private Bitmap image;
    public static File cache;

    public ChildContent(String _description) {
        this.description = _description;

    }

    public ChildContent(String _description, Bitmap _image) {
        this.description = _description;
        this.image = _image;

    }

    public ChildContent(String _description, Bitmap _image, String _textItem) {
        this.description = _description;
        this.image = _image;
        this.textItem = _textItem;

    }

    public ChildContent(String _description, Bitmap _image, String _textItem, File _cache) {
        this.description = _description;
        this.image = _image;
        this.textItem = _textItem;
        this.cache = _cache;
    }

    public String getDescription() {
        return description;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getTextItem() {
        return textItem;
    }

    public File getFile() {
        return cache;
    }

    public void setTextItem(String textItem) {
        this.textItem = textItem;
    }

    public void setDescription(String _description) {
        this.description = _description;
    }

    public void setImage(Bitmap _image) {
        this.image = _image;
    }
}

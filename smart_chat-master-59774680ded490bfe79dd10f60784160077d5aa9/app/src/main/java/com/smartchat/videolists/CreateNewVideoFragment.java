package com.smartchat.videolists;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;

import com.smartchat.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


public class CreateNewVideoFragment  extends Fragment implements View.OnClickListener {

    private ImageButton setLogoButton;
    private EditText etNameList;
    private EditText etItemList;
    private ImageButton addItem,edTextItem;
    private ImageButton doneBtn;
    private GridView itemsGridView;
    private ArrayList<ChildContent> items;
    private Bitmap bitmap;
    private CreateItemVideoListAdapter adapter;;
    private Intent videoIntent;
    private Uri uri;
    private String nameFile,path,cachePathFile;
    private long timeInmillisec;
    public AlertDialog.Builder builder;
    private File cache=null;
    private static final int REQUEST_VIDEO_CAPTURE = 1;
    private final static int TYPE_GALLERY_PICK = 2;
    private static final int TYPE_LOGO_PICK = 3;
    private static final String EMPTY_CACHE  ="Empty Cache";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        items = new ArrayList();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.lists_create_new_video_list_fragment, container, false);

        init(rootView);

        return rootView;

    }

    private void init(View rootView) {

        setLogoButton = (ImageButton) rootView.findViewById(R.id.listLogoButton);
        setLogoButton.setOnClickListener(this);
        etNameList = (EditText) rootView.findViewById(R.id.etName);
        addItem = (ImageButton) rootView.findViewById(R.id.addItemVideo);
        addItem.setOnClickListener(new AddImageButton());
        //doneBtn = (ImageButton) rootView.findViewById(R.id.editChildList);
        itemsGridView = (GridView) rootView.findViewById(R.id.videoGridView);
        adapter = new CreateItemVideoListAdapter(getActivity(), items);
        itemsGridView.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case REQUEST_VIDEO_CAPTURE:
                    requestVideo(data);
                    break;
                case TYPE_GALLERY_PICK:
                    requestGallery(data);
                    break;
                case TYPE_LOGO_PICK:
                    requestLogo(data);
            }
        }
    }

    public void getBitmapImage(String pathFile){
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        bitmap = ThumbnailUtils.createVideoThumbnail(pathFile, MediaStore.Images.Thumbnails.MINI_KIND);
    }

    public long getTimeVideo(String path){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        String heightl=retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String widhtl=retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        // Log.d("igor", "retriver height "+heightl+"x"+widhtl);
        return Long.parseLong(time);
    };

    @Override
    public void onClick(View v) {
       switch (v.getId()){
            case R.id.listLogoButton:
                Log.d("prepare", "button logo click" );
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, TYPE_LOGO_PICK);
                break;
//            case R.id.editChildList:
//                //doneEdit();
//                break;
        }
    }

    public void requestVideo(Intent data){
        getFileData(data);
//        if (timeInmillisec > 10000) {
//            Toast toast = Toast.makeText(getActivity(), "Sorry,the length of this video more than 10 seconds!",
//                    Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//
//            toast.show();
//        } else {
            getBitmapImage(path);
            items.add(new ChildContent(path, bitmap, nameFile));
            adapter.notifyDataSetChanged();
            Log.d("ChatFragment", "Child item 0 position ");
      ///  }

    }

    public void requestGallery(Intent data){
        getFileData(data);
//        if (timeInmillisec > 10000) {
//            Toast toast = Toast.makeText(getActivity(), "Sorry,the length of this video more than 10 seconds!",
//                    Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
//        } else {
            getBitmapImage(path);
          //  fileInCacheDown();
       // }
        items.add(new ChildContent(path, bitmap,nameFile));
        adapter.notifyDataSetChanged();
    }

    public void requestLogo(Intent data){
        Log.d("igor", "logo click ");
        try {
            setLogoButton.setImageBitmap(getLightImage(data.getData()));
            bitmap = null;
        } catch (OutOfMemoryError e) {
            Log.d("igor", "Logo pick out of memory ");
            setLogoButton.setImageResource(R.drawable.ic_launcher);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getFileData(Intent data) {
        uri = Uri.parse(data.getData().toString());
        path = getRealPathFromURI(getActivity(), uri);
        nameFile =path.substring(path.lastIndexOf("/") + 1);
        timeInmillisec = getTimeVideo(path);
    }

    private void fileInCacheDown(){
        File selectFile = new File(path);
        if (cache!=null) {
            clearCache();
            cache = new File(getActivity().getCacheDir(), nameFile);
            try {
                InputStream is = new FileInputStream(selectFile);
                OutputStream os = new FileOutputStream(cache);
                byte[] dat = new byte[is.available()];
                is.read(dat);
                os.write(dat);
                is.close();
                os.close();
                Log.d("prepare", "cache file " + cache.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            getBitmapImage(path);
            items.add(new ChildContent(path, bitmap,nameFile,cache));
            adapter.notifyDataSetChanged();
        }
        else {
            cache = new File(getActivity().getCacheDir(), nameFile);
            try {
                InputStream is = new FileInputStream(selectFile);
                OutputStream os = new FileOutputStream(cache);
                byte[] dat = new byte[is.available()];
                is.read(dat);
                os.write(dat);
                is.close();
                os.close();
                Log.d("prepare", "cache file " + cache.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            getBitmapImage(path);
            Log.d("igor", "cache file " + cache.getName());
            items.add(new ChildContent(path, bitmap, nameFile,cache));
            adapter.notifyDataSetChanged();
        }

    }

    private class AddImageButton implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(getResources().getString(R.string.add_image_alert));

            alertDialog.setMessage(getResources().getString(R.string.choose_alert));

            alertDialog.setIcon(R.mipmap.ic_media);

            alertDialog.setPositiveButton(getResources().getString(R.string.make_alert),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                            startActivityForResult(videoIntent, REQUEST_VIDEO_CAPTURE);

                        }
                    });

            alertDialog.setNegativeButton(getResources().getString(R.string.gallery_alert),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            videoIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(videoIntent, TYPE_GALLERY_PICK);

                        }
                    });

            alertDialog.show();
        }
    }


    public void clearCache() {
        deleteCache(getActivity());
    }
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    private Bitmap getLightImage (Uri uri) throws FileNotFoundException {
        InputStream in = getActivity().getContentResolver().openInputStream(uri);


        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(in, null, o);
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int origWidth = o.outWidth;
        int origHeight = o.outHeight;
        int bytesPerPixel = 2;
        int maxSize = 700 * 700 * bytesPerPixel;
        int desiredWidth = 300;
        int desiredHeight = 300;
        int desiredSize = desiredWidth * desiredHeight * bytesPerPixel;
        if (desiredSize < maxSize) maxSize = desiredSize;
        int scale = 1;
        int origSize = origWidth * origHeight * bytesPerPixel;
        if (origWidth > origHeight) {
            scale = Math.round((float) origHeight / (float) desiredHeight);
        } else {
            scale = Math.round((float) origWidth / (float) desiredWidth);
        }

        o = new BitmapFactory.Options();
        o.inSampleSize = scale;
        o.inPreferredConfig = Bitmap.Config.RGB_565;

        in = getActivity().getContentResolver().openInputStream(uri);
        bitmap = BitmapFactory.decodeStream(in, null, o);
        return bitmap;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Video.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (NullPointerException e){
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contentUri.toString();
    }

}

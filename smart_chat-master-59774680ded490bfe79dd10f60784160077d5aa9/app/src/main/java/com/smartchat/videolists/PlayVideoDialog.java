package com.smartchat.videolists;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.VideoView;

import com.smartchat.R;

import java.io.File;
import java.util.ArrayList;

public class PlayVideoDialog {
    private Uri uri;
    private Activity activity;
    MediaController mController;
    FrameLayout frLayout,frTitle;
    public AlertDialog.Builder builder;
    public AlertDialog alertDialog;
    public File cache;
    private String nameItem,content;
    private ImageButton btnSave;
    private EditText edText;
    private ArrayList<ChildContent> items;
    private CreateItemVideoListAdapter adapter;
    private int position;
    private VideoView video;

    public PlayVideoDialog(Activity activity,Uri _uri,ArrayList<ChildContent> _items, int position,CreateItemVideoListAdapter _adapter){
        this.activity=activity;
        this.uri=_uri;
        this.items=_items;
        this.position=position;
        this.adapter=_adapter;
    }


    public void show(){
        builder = new AlertDialog.Builder(activity)
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                        if (keyCode == KeyEvent.KEYCODE_BACK &&
                                event.getAction() == KeyEvent.ACTION_UP &&
                                !event.isCanceled()) {
                            if(frLayout.getChildCount()>1)
                                ((ViewGroup) mController.getParent()).removeView(mController);
                            dialog.cancel();
                            return true;
                        }
                        return false;
                    }
                });

        View vDialog = activity.getLayoutInflater().inflate(R.layout.play_video_list_item, null);
        builder.setView(vDialog);
        init(vDialog);
        setData();

        alertDialog= builder.create();
        alertDialog.getWindow().setDimAmount(0);
        alertDialog.show();

    }
    public void dismissDialog() {
        alertDialog.dismiss();
    }

    public void init(View rootView){
        video = (VideoView)rootView.findViewById(R.id.vView);

        frLayout= (FrameLayout)rootView.findViewById(R.id.linDialog);
        edText=(EditText)rootView.findViewById(R.id.edTextItem);
        btnSave=(ImageButton)rootView.findViewById(R.id.btnSaveText);

        mController=new MediaController(activity){
            @Override
            public void hide() {
                super.hide();
                try {
                    if(frLayout.getChildCount()>1)
                        mController.setVisibility(GONE);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

        };
    }
    public void setData(){
        edText.setText(items.get(position).getTextItem());
        video.setVideoURI(uri);
        video.setKeepScreenOn(true);
        video.requestFocus();
        video.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!mController.isShowing()){
                    Log.d("createt", "Touch video");
                    mController.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.get(position).setTextItem(edText.getText().toString());
                adapter.notifyDataSetChanged();
                dismissDialog();
            }
        });
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.d("create","MediaPlayer.OnPreparedListener");
                ViewGroup.LayoutParams lp = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(lp);
                lp2.gravity = Gravity.BOTTOM;
                mController.setLayoutParams(lp2);
                ((ViewGroup) mController.getParent()).removeView(mController);
                frLayout.addView(mController);
                video.setMediaController(mController);
                mController.setAnchorView(video);
                mp.start();
                mController.show();
                Log.d("create","video getCurrentPosition "+video.getCurrentPosition());
                video.start();

            }
        });
    }
}

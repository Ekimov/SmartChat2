package com.smartchat.videolists;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.smartchat.R;
import java.util.ArrayList;



public class CreateItemVideoListAdapter extends BaseAdapter {

    private ArrayList<ChildContent> items;
    private Activity context;
    private View myView;

    public CreateItemVideoListAdapter(Activity _ctx, ArrayList<ChildContent> _items) {
        this.context = _ctx;
        this.items = _items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;

    }

    public class VideoHolder {
        public ImageView imView;
        public ImageButton del;
        public TextView textContent;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        myView = convertView;
        Log.d("count", "items size "+items.size());
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            VideoHolder videoHolder = new VideoHolder();
            myView = inflater.inflate(R.layout.list_create_video_item, null);
            videoHolder.imView = (ImageView) myView.findViewById(R.id.itemView);
            videoHolder.textContent=(TextView)myView.findViewById(R.id.textContent);
            videoHolder.del = (ImageButton) myView.findViewById(R.id.delItem);
            myView.setTag(videoHolder);
        }

        VideoHolder holder = (VideoHolder) myView.getTag();
        holder.textContent.setText(items.get(position).getTextItem());
        holder.imView.setImageBitmap(items.get(position).getImage());
        holder.imView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("igor", "items.get(position).getDescription().substring(items.get(position).getDescription().lastIndexOf(\"/\") + 1) "+items.get(position).getDescription().substring(items.get(position).getDescription().lastIndexOf("/") + 1));
                String nameItem=getNameItem(position);
//                if(ChildContent.cache.getName()!=null&& nameItem.equals(ChildContent.cache.getName())){
//                    PlayVideoDialog fragment=new PlayVideoDialog(context,Uri.parse(items.get(position).getFile().toString()),items,position, CreateItemVideoListAdapter.this);
//                    //PlayDialogFragment playDialogFragment= new PlayDialogFragment();
//                    //playDialogFragment.setArguments(context,Uri.parse(items.get(position).getFile().toString()),items,position, CreateItemVideoListAdapter.this);
//                    fragment.show();
//
//                    Log.d("igor", "Reproduced directly from the cache ");
//                }
//                else{
                    PlayVideoDialog fragment=new PlayVideoDialog(context,Uri.parse(items.get(position).getDescription()),items,position, CreateItemVideoListAdapter.this);
                    fragment.show();
               // }
            }
        });

        Log.d("create", "position " +position);

        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.remove(position);
                notifyDataSetChanged();
                Log.d("fuck", "position "+ position);
            }
        });

        return myView;
    }


    private String getNameItem(int position){

        return items.get(position).getDescription().substring(items.get(position).getDescription().lastIndexOf("/") + 1);
    }
}

package com.smartchat.server;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.smartchat.objects.PostObject;
import com.smartchat.utils.AppConst;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.greenrobot.event.EventBus;


public class ImageHelper {
    private static final String TAG = "ImageHelper";
    private String accessToken;
    private String filePath;
    private String status;
    private String UUID;
    private String MIME;
    private String Response;

    private byte[] byteArray;
    private Bitmap imageBitmap;
    private PostObject myObject;
//    public ImageHelper(String filePath,String MIME, String accessToken,) {
//        this.filePath = filePath;
//        this.accessToken = accessToken;
//        this.MIME = MIME;
////        getByteArray(filePath);
//        getFullImageByteArray(filePath);
//    }

    public ImageHelper() {
    }

    /* private Bitmap getImageBitmap (String UUID,String accessToken){
         downloadImageBitmap(UUID,accessToken);
         if (Response==null)
             return null;
         else return imageBitmap;
     }
     private void downloadImageBitmap(String UUID, String accessToken){
         Bitmap bitmap;
         String downloadURL = "http://54.93.222.229/api/v1/"+UUID+"?accessToken="+accessToken;
         ImageLoader.getInstance().loadImage(downloadURL, new ImageLoadingListener() {
             @Override
             public void onLoadingStarted(String s, View view) {
                 imageBitmap=null;
             }

             @Override
             public void onLoadingFailed(String s, View view, FailReason failReason) {
                 Response = s;
             }

             @Override
             public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                 imageBitmap = bitmap;
             }

             @Override
             public void onLoadingCancelled(String s, View view) {
                 imageBitmap = null;
             }
         });
     }*/
    public static String getDownloadUrl(String UUID, String accessToken) {
        return AppConst.RELEASE_GET_IMAGE_LINK + UUID + "?accessToken=" + accessToken;
    }

    private void getFullImageByteArray(String filePath, int maxWidth, int maxHeight) {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        bitmap = scaleBitmap(bitmap, maxWidth, maxHeight);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byteArray = stream.toByteArray();
    }

/*
    public byte[] getByteArray(String filePath) {
        Bitmap currentBitmap = decodeImage(filePath);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        currentBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArray = stream.toByteArray();
        return byteArray;
    }
*/

    public void uploadImage(PostObject object, String accessToken, String filePath, String MIME, int maxWidth, int maxHeight) {
        getFullImageByteArray(filePath, maxWidth, maxHeight);
        myObject = object;
        new UploadAsync().execute(accessToken, MIME);
    }

/*
    public void uploadImageByte(PostObject object, String accessToken, byte[] bitmap, String MIME) {
        byteArray = bitmap;
        myObject =object;
        new UploadAsync().execute(accessToken, MIME);
    }
*/

    /*public String removeImage(String UUID,String accessToken){
        new RemoveAsync().execute(UUID,accessToken);
        return Response;
    }
    private class RemoveAsync extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            DefaultHttpClient client = new DefaultHttpClient();
            String url = "http://54.148.194.39/api/v1/files/"+params[0]+"?accessToken="+params[1];
            Log.d(TAG,"Delete URL "+ url);
            HttpDelete httpDelete = new HttpDelete(url);
            JSONObject response = null;
            try {
                HttpResponse httpResponse = client.execute(httpDelete);
                HttpEntity httpEntity = httpResponse.getEntity();
                InputStream inputStream = httpEntity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
                StringBuilder sb = new StringBuilder();
                String responseString = null;
                while((responseString = bufferedReader.readLine())!=null){
                    sb.append(responseString+"\n");
                }
                inputStream.close();
                Log.d(TAG, "Remove response "+ sb.toString());
                response = new JSONObject(sb.toString());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }catch (JSONException e){
                e.printStackTrace();
                return null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            if (response!=null&&!(response.optString("status").equals("0")))
                Response = response.optString("errorMessage");
            else {
                Response = response.optString("status");
            }
        }
    }*/
    public static Bitmap scaleBitmap(Bitmap bm, int maxWidth, int maxHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        Log.v("Pictures", "Width and height are " + width + "--" + height);

        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int)(height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int)(width / ratio);
        } else {
            // square
            height = maxHeight;
            width = maxWidth;
        }

        Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);

        bm = Bitmap.createScaledBitmap(bm, width, height, true);
        return bm;
    }

    public static Bitmap decodeImage(String picturePath) {
        try {
            File file = new File(picturePath);
            // Get image size
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, opts);

            final int MIN_SIZE = 150;

            int scale = 1;
            while (((opts.outWidth / scale) >> 1) >= MIN_SIZE
                    && ((opts.outHeight / scale) >> 1) >= MIN_SIZE) {
                scale <<= 1;
            }

            BitmapFactory.Options opts2 = new BitmapFactory.Options();
            opts2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, opts2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    private class UploadAsync extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            DefaultHttpClient client = new DefaultHttpClient();
            String url = AppConst.RELEASE_UPLOAD_IMAGE + params[0];

            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader(HTTP.CONTENT_TYPE, params[1]);
            httpPost.setEntity(new ByteArrayEntity(byteArray));
            JSONObject response = null;
            try {
                HttpResponse httpResponse = client.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                InputStream inputStream = httpEntity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String responseString = null;
                while ((responseString = bufferedReader.readLine()) != null) {
                    sb.append(responseString + "\n");
                }
                inputStream.close();
                response = new JSONObject(sb.toString());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);

            if (response == null) {
                Response = AppConst.IMAGE_ERROR;
            } else if (response.optString("status").equals("0")) {
                Response = AppConst.IMAGE_ERROR;
            } else {
                Response = response.optString("uuid");
            }
            Log.d(TAG, "resp in async -> " + Response);
            myObject.setObject(Response);
            myObject.setKey(AppConst.IMAGE_STATUS);
            EventBus.getDefault().post(myObject);
        }
    }

}

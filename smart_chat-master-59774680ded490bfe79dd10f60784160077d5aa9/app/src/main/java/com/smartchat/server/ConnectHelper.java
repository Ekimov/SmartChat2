package com.smartchat.server;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.protobuf.InvalidProtocolBufferException;

import org.apache.http.message.BasicNameValuePair;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.smartchat.activity.MainActivity;
import com.smartchat.objects.*;
import com.smartchat.objects.Auth;
import com.smartchat.server.SmartChat.*;
import com.smartchat.server.SmartChat.ChatRoom;
import com.smartchat.server.SmartChat.Comment;
import com.smartchat.server.SmartChat.Field;
import com.smartchat.server.SmartChat.ListItem;
import com.smartchat.server.SmartChat.Message;
import com.smartchat.server.SmartChat.MessageBody;
import com.smartchat.server.SmartChat.User;
import com.smartchat.server.SmartChat.UserAuth;
import com.smartchat.server.SmartChat.UserShort;
import com.smartchat.utils.AppConst;

import java.util.UUID;

import de.greenrobot.event.EventBus;

public class ConnectHelper {

    private WebSocketClient client;
    private String msg, accessToken;
    private MainActivity activity;
    private DataManager dm = new DataManager();
    private PostObject postObject;

    public ConnectHelper(Activity _activity){
        activity = (MainActivity)_activity;
    }

    public ConnectHelper(String accessToken){

        this.accessToken = accessToken;}

    private void sendPostData(Object object, int key) {
        postObject = new PostObject();
        postObject.setObject(object);
        postObject.setKey(key);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(postObject);
            }
        });
    }

    public void connect(){

        accessToken = activity.getSharedPreferences(AppConst.SMART_CHAT,Context.MODE_PRIVATE)
                .getString("accessToken", "");

        System.out.println("accessToken: " + accessToken);

        List<BasicNameValuePair> extraHeaders = Arrays.asList(
                new BasicNameValuePair("Cookie", "session=abcd")
        );
            client = new WebSocketClient(URI.create(AppConst.RELEASE+accessToken), new WebSocketClient.Listener() {

            @Override
            public void onConnect() {
                NetworkConnection.setConnected(true);
                Log.d("TAG", "Connected!");
            }

            @Override
            public void onMessage(String message) {
                Log.d("TAG", "MESSAGE str!");

                msg = message;

                Log.d("TAG", msg);
            }

            @Override
            public void onMessage(byte[] data) {
                Log.d("TAG", "MESSAGE byte!");
                Response resp = null;
                //sendPostData();

                try {
                    resp = Response.parseFrom(data);
                    Log.d("TAG", resp.getType().toString());
                    Log.d("TAG", resp.getStatus().getSuccess()+"");
                    Log.e("TAG", "Response "+resp.toString());
                    checkStatus(resp);
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                    System.out.println(resp.toString());
                }
                Log.d("TAG", ">>" + resp.getType().toString());
            }

            @Override
            public void onDisconnect(int code, String reason) {
                NetworkConnection.setConnected(false);
                Log.d("TAG", "Disconnect!");

            }

            @Override
            public void onError(Exception error) {
                error.printStackTrace();
                Log.d("TAG", "Error! ");
            }
        }, extraHeaders);
        client.connect();
    }


    //TODO CHANGE PUID!

    // ============================================================================================
    // Chat message (START)
    // ============================================================================================

    // TODO THIS NEED ????
    public void sendMessageUser(String msg, boolean privateState) {

        PUUID id = generatePUUID();
        Log.d("TAG", id+"");

    }

    public void sendMessageRoomRequest(PUUID message_id, PUUID chat_room_id, String data,
                                       int security_level, int time_destroy, int message_date) {
        Request request = null;
        if(security_level == 0) {
            if (time_destroy == 0) {
                request = SmartChat.Request.newBuilder()
                        .setType(CallType.SEND_MESSAGE)
                        .setRequest(
                                SendMessageRequest.newBuilder()
                                        .setMessageId(message_id).setChatRoomId(chat_room_id)
                                        .addBodies(SmartChat.MessageBody.newBuilder()
                                                .setText(data)
                                                .setType(MessageDataType.TEXT).build())
                                        .build().toByteString()).build();
            } else {
                request = SmartChat.Request.newBuilder()
                        .setType(CallType.SEND_MESSAGE)
                        .setRequest(
                                SendMessageRequest.newBuilder()
                                        .setTimeDestroy(time_destroy).setMessageDate(message_date)
                                        .setMessageId(message_id).setChatRoomId(chat_room_id)
                                        .addBodies(SmartChat.MessageBody.newBuilder()
                                                .setText(data)
                                                .setType(MessageDataType.TEXT).build())
                                        .build().toByteString()).build();
            }
        } else {
            if (time_destroy == 0) {
                request = SmartChat.Request.newBuilder()
                        .setType(CallType.SEND_MESSAGE)
                        .setRequest(
                                SendMessageRequest.newBuilder()
                                        .setMessageId(message_id).setChatRoomId(chat_room_id)
                                        .setSecurityLevel(security_level)
                                        .addBodies(SmartChat.MessageBody.newBuilder()
                                                .setText(data)
                                                .setType(MessageDataType.TEXT).build())
                                        .build().toByteString()).build();
            } else {
                request = SmartChat.Request.newBuilder()
                        .setType(CallType.SEND_MESSAGE)
                        .setRequest(
                                SendMessageRequest.newBuilder()
                                        .setTimeDestroy(time_destroy).setMessageDate(message_date)
                                        .setSecurityLevel(security_level)
                                        .setMessageId(message_id).setChatRoomId(chat_room_id)
                                        .addBodies(SmartChat.MessageBody.newBuilder()
                                                .setText(data)
                                                .setType(MessageDataType.TEXT).build())
                                        .build().toByteString()).build();
            }
        }
        client.send(request.toByteArray());
    }

    // ============================================================================================
    // Chat message (END)
    // ============================================================================================

    // ============================================================================================
    // Comments operations (START)
    // ============================================================================================

    public void commentAddRequest(PUUID listPUUID, String content){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.COMMENT_ADD_VALUE))
                .setRequest(AddCommentRequest.newBuilder().setListId(listPUUID).setContent(content).build().toByteString())
                .build();

        client.send(request.toByteArray());
    }

    public void commentGetRequest(PUUID listID, int offset, int limit){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.COMMENT_GET_VALUE))
                .setRequest(GetCommentsRequest.newBuilder().setListId(listID).setOffset(offset).setLimit(limit).build().toByteString())
                .build();

        client.send(request.toByteArray());

    }

    public void commentRemoveRequest(PUUID listID, PUUID commentID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.COMMENT_REMOVE_VALUE))
                .setRequest(RemoveCommentRequest.newBuilder().setListId(listID).setId(commentID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void commentUpdateRequest(PUUID listID, PUUID commentID, String text){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.COMMENT_UPDATE_VALUE))
                .setRequest(UpdateCommentRequest.newBuilder().setListId(listID).setCommentId(commentID).setText(text).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    // ============================================================================================
    // Comments (END)
    // ============================================================================================


    // ============================================================================================
    // History operations (START)
    // ============================================================================================

    // TODO THIS NEED
    public void historyAddRequest(long date, String content, PUUID roomID, boolean privateState){

       /* Request request = Request.newBuilder().setType(CallType.valueOf(CallType.HISTORY_ADD_VALUE))
                .setRequest(AddHistoryRequest.newBuilder().setDate(date).setContent(content).setChatRoomId(roomID).setPrivate(privateState).build().toByteString()).build();
*/
        //client.send(request.toByteArray());
    }

    public void historyGetRequest(int offset, int limit){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.COMMENT_GET_VALUE))
                .setRequest(GetHistoryRequest.newBuilder().setOffset(offset).setLimit(limit).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    // ============================================================================================
    // History operations (END)
    // ============================================================================================


    // ============================================================================================
    // ChatRooms operations (START)
    // ============================================================================================

    public void getAllRoomsForUserRequest(int limit, int offset){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.GET_ALL_ROOMS_FOR_USER_VALUE))
                .setRequest(GetAllRoomsForUserRequest.newBuilder().setLimit(limit).setOffset(offset).build().toByteString()).build();
        client.send(request.toByteArray());
    }

    public void createChatRoomRequest(String name, String description, long timeToDestroy, int type, PUUID puuid, int security, List<PUUID> members){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CREATE_CHATROOM_VALUE))
                .setRequest(CreateChatRoomRequest.newBuilder()
                        .setName(name)
                        .setChatDescription(description)
                        .setTimeDestroy(timeToDestroy)
                        .setChatType(type)
                                //.setFileImageId(puuid)
                        .setSecurityLevel(security)
                        .addAllMembers(members)
                        .build().toByteString()).build();
        client.send(request.toByteArray());
    }


    public void getChatRoomHistoryRequest(PUUID puuidChatRoom, int offset, int limit){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CHAT_ROOM_HISTORY_GET_VALUE))
                .setRequest(GetChatRoomHistoryRequest.newBuilder().setChatRoomId(puuidChatRoom).build().toByteString()).build();
        client.send(request.toByteArray());
    }

    public void changeInfoChatRoomRequest(PUUID puuidChatRoom, PUUID imageId, String name,
                                          String autor, String description, int type, long timeDestroy,
                                          int securityLevel) {
        
        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CHANGE_INFO_CHATROOM_VALUE))
                .setRequest(ChangeChatRoomInfoRequest.newBuilder()
                        .setChatRoom(com.smartchat.server.SmartChat.ChatRoom.newBuilder()
                            .setId(puuidChatRoom)
                            .setName(name)
                            //.setTimeDestroy(timeDestroy)
                            .setChatType(type)  // 0 - public
                            .setChatDescription(description)
                            //.setFileImageId(imageId)
                            .setSecurityLevel(securityLevel)
                        )
                        .build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void removeChatRoomRequest(PUUID chatRoomID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.REMOVE_CHATROOM_VALUE))
                .setRequest(RemoveChatRoomRequest.newBuilder().setId(chatRoomID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void getAllUsersForRoomRequest(PUUID puuid, int limit, int offset){

        Request request = Request.newBuilder().setType(CallType.GET_ALL_USERS_FOR_ROOM)
                .setRequest(GetAllUsersForRoomRequest.newBuilder().setId(puuid).setLimit(limit).setOffset(offset).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void leaveUserFromChatRoomRequest(PUUID chatRoomID, PUUID userID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LEAVE_USER_FROM_ROOM_VALUE))
                .setRequest(LeaveUserFromRoomRequest.newBuilder().setId(chatRoomID).setUserId(userID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void joinUserToChatRoom(PUUID chatRoomID, PUUID userID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.JOIN_USER_TO_ROOM_VALUE))
                .setRequest(JoinUserToRoomRequest.newBuilder().setId(chatRoomID).setUserId(userID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    // ============================================================================================
    // ChatRooms operations (END)
    // ============================================================================================


    // ============================================================================================
    // Contacts operations (START)
    // ============================================================================================

    public void getAllContacts(){
        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CONTACTS_GET_VALUE)).build();
        client.send(request.toByteArray());
    }

    public void removeContactRequest(PUUID contactID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CONTACT_REMOVE_VALUE))
                .setRequest(RemoveContactRequest.newBuilder().setId(contactID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void getAllContactFieldsRequest(PUUID ID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CONTACT_FIELDS_GET_VALUE))
                .setRequest(GetAllContactFieldsRequest.newBuilder().setId(ID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void setContactFieldsRequest(PUUID contact_puuid, PUUID user_puuid, ArrayList<SmartChat.Field> fieldArrayList){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CONTACT_SET_FIELDS_VALUE)).setRequest(SetFieldsRequest.newBuilder().setContactId(contact_puuid).setUserId(user_puuid).addAllFields(fieldArrayList).build().toByteString()).build();
        client.send(request.toByteArray());
    }

    public void contactCreateRequest() {
        Request request = Request.newBuilder()
                .setType(CallType.valueOf(CallType.CONTACT_CREATE_VALUE))
                .build();

        client.send(request.toByteArray());
    }

    public void setFieldsRequest(String value, int fieldType, PUUID local_puuid){

        SmartChat.Field field = SmartChat.Field.newBuilder().setType(FieldType.valueOf(fieldType)).setValue(value).build();

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.SET_PROFILE_FIELD_VALUE))
                .setRequest(SetProfileFieldRequest.newBuilder().addFields(field).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void setAllFieldsRequest(ArrayList<SmartChat.Field> fieldArrayList){
       // Field field = Field.newBuilder().setType(FieldType.valueOf(fieldType)).setValue(value).build();

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.SET_PROFILE_FIELD_VALUE)).setRequest(SetProfileFieldRequest.newBuilder().addAllFields(fieldArrayList).build().toByteString()).build();

        client.send(request.toByteArray());
    }


    public void removeFieldRequest(PUUID contactID, short fieldType){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.CONTACT_REMOVE_FIELD_VALUE))
                .setRequest(RemoveFieldRequest.newBuilder().setContactId(contactID).setType(FieldType.valueOf(fieldType)).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void searchUsersByAuthRequest(ArrayList<Auth> authList) {
        int authListSize = authList.size();
        for (int i = 0; i < authListSize; i++) {
            Auth auth = authList.get(i);
            SmartChat.Auth.Builder authBuilder = null;
            if(auth.getType() == AuthType.EMAIL_AUTH) {
                authBuilder = SmartChat.Auth.newBuilder().setType(AuthType.EMAIL_AUTH).setValue(auth.getValue());
            }
            if(auth.getType() == AuthType.PHONE_AUTH) {
                authBuilder = SmartChat.Auth.newBuilder().setType(AuthType.PHONE_AUTH).setValue(auth.getValue());
            }
            SearchUsersByAuthRequest searchUsersByAuthRequest = SearchUsersByAuthRequest.newBuilder().addAuths(authBuilder).build();
            Request request = Request.newBuilder().setType(CallType.valueOf(CallType.SEARCH_USERS_BY_AUTH_VALUE))
                    .setRequest(searchUsersByAuthRequest.toByteString())
                    .build();
            client.send(request.toByteArray());
        }
    }

    public void testSearchUsersByAuthRequest(ArrayList<Auth> authArray) {
        ArrayList<SmartChat.Auth> authSC = new ArrayList<>();
        int size = authArray.size();
        for (int i = 0; i < size; i++) {
            Auth auth = authArray.get(i);
            authSC.add(SmartChat.Auth.newBuilder().setType(auth.getType()).setValue(auth.getValue()).build());
        }
        SearchUsersByAuthRequest searchUsersByAuthRequest = SearchUsersByAuthRequest.newBuilder().addAllAuths(authSC).build();
        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.SEARCH_USERS_BY_AUTH_VALUE))
                .setRequest(searchUsersByAuthRequest.toByteString())
                .build();
        client.send(request.toByteArray());
    }

    // ============================================================================================
    // Contacts operations (END)
    // ============================================================================================


    // ============================================================================================
    // List operations (START)
    // ============================================================================================

    public void listGetRequest(){
        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LISTS_GET_VALUE)).build();
        client.send(request.toByteArray());
    }

    public void listAddRequest(String name, String description){

        Request request = Request.newBuilder().setType(CallType.LIST_ADD).setRequest(AddListRequest.newBuilder().setName(name).setDescription(description).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void listUpdateRequest(PUUID listID, String text, String description){

        Request request = Request.newBuilder().setType(CallType.LIST_UPDATE).setRequest(UpdateListRequest.newBuilder().setListId(listID).setName(text).setDescription(description).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void listRemoveRequest(PUUID listID){

        Request request = Request.newBuilder().setType(CallType.LIST_REMOVE).setRequest(RemoveListRequest.newBuilder().setListId(listID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void publicListGetRequest(int limit, int offset){

        Request request = Request.newBuilder().setType(CallType.PUBLIC_LIST_GET).setRequest(GetPublicListsRequst.newBuilder().setLimit(limit).setOffset(offset).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    // ============================================================================================
    // List operations (END)
    // ============================================================================================


    // ============================================================================================
    // List item operations (START)
    // ============================================================================================

    public void listAllItemsGetRequest(PUUID listID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LIST_ITEMS_GET_VALUE))
                .setRequest(GetListItemsRequest.newBuilder().setListId(listID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void lisItemGetRequest(PUUID listID, PUUID itemID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LIST_ITEM_GET_VALUE))
                .setRequest(GetListItemRequest.newBuilder().setListId(listID).setId(itemID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void  listItemTextAddRequest(PUUID listID, String text){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LIST_ITEM_ADD_VALUE))
                .setRequest(AddListItemRequest.newBuilder().setListId(listID).setText(text).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void listItemTextUpdateRequest(PUUID listID, PUUID itemID, String text){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LIST_ITEM_UPDATE_VALUE))
                .setRequest(UpdateListItemRequest.newBuilder().setListId(listID).setId(itemID).setText(text).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void listItemTextRemoveRequest(PUUID listID, PUUID itemID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.LIST_ITEM_REMOVE_VALUE))
                .setRequest(RemoveListItemRequest.newBuilder().setListId(listID).setId(itemID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    // ============================================================================================
    // List item operations (END)
    // ============================================================================================

//TODO LIKES
    // ============================================================================================
    // Like (START)
    // ============================================================================================

    /*private void likesGetRequest(){

        Response response = Response.newBuilder().setType(CallType.valueOf(CallType.))
                .setResponse().build();

    }*/

    // ============================================================================================
    // Like (START)
    // ============================================================================================


    // ============================================================================================
    // Profile operations (START)
    // ============================================================================================

    public void userByAuthGetRequest(AuthType authType, String value){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.GET_USER_BY_AUTH_VALUE))
                .setRequest(GetUserByAuthRequest.newBuilder().setAuthType(authType).setValue(value).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void userByIDGetRequest(PUUID userID){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.GET_USER_BY_ID_VALUE))
                .setRequest(GetUserByIdRequest.newBuilder().setUserId(userID).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void getAuthRequest(){
        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.AUTH_GET_VALUE)).build();
        client.send(request.toByteArray());
    }

    public void authAddRequest(AuthType type, String value){
        SmartChat.Auth.Builder builder = SmartChat.Auth.newBuilder().setValue(value);
        switch (type) {
            case PHONE_AUTH:
                builder.setType(AuthType.PHONE_AUTH);
                break;
            case EMAIL_AUTH:
                builder.setType(AuthType.EMAIL_AUTH);
                break;
        }
        byte[] requestToServerInBytes = SmartChat.Request.newBuilder()
                .setType(CallType.AUTH_ADD)
                .setRequest(
                        AddAuthRequest.newBuilder()
                                .setAuth(builder.build())
                                .build()
                                .toByteString()
                ).build()
                .toByteArray();

        client.send(requestToServerInBytes);
    }

    public void authConfirmRequest(String value, String code){
        // Confirm auth-field with code (code from SMS or Email)
        /*message ConfirmAuthRequest {
            required Auth auth = 1;
            required string code = 2;
        }*/
        Request request = Request.newBuilder().setType(CallType.AUTH_CONFIRM)
                .setRequest(ConfirmAuthRequest.newBuilder().setAuth(SmartChat.Auth.newBuilder()
                        .setType(AuthType.EMAIL_AUTH).setValue(value).build())
                        .setCode(code).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void authFieldRemoveRequest(){
        // Remove auth-field
        /*message RemoveAuthRequest {
            required Auth auth = 1;
        }*/
        /*Request request = Request.newBuilder().setType(CallType.valueOf(CallType.AUTH_REMOVE_VALUE))
                .setRequest(RemoveAuthRequest.newBuilder().setAuth(Auth.newBuilder().setValue(value).build()).build().toByteString()).build();

        client.send(request.toByteArray());*/
    }

    private void authUserByIdGetRequest(int index, String value){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.GET_USER_BY_ID_VALUE))
                .setRequest(GetAuthByUserIdResponse.newBuilder().setAuth(index, SmartChat.Auth.newBuilder()
                        .setValue(value).build()).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    public void profileFieldSetRequest(int index, String value){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.SET_PROFILE_FIELD_VALUE))
                .setRequest(SetProfileFieldRequest.newBuilder().setFields(index, SmartChat.Field.newBuilder()
                        .setValue(value).build()).build().toByteString()).build();
        client.send(request.toByteArray());
    }

    public void profileFieldGetRequest(){
        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.PROFILE_GET_VALUE)).build();
        client.send(request.toByteArray());
    }

    public void profileFieldRemoveRequest(int index, int type){

        Request request = Request.newBuilder().setType(CallType.valueOf(CallType.REMOVE_PROFILE_FIELD_VALUE))
                .setRequest(RemoveProfileFieldRequest.newBuilder()
                        .setFields(index, FieldType.valueOf(type)).build().toByteString()).build();

        client.send(request.toByteArray());
    }

    // ============================================================================================
    // Profile operations (EDIT)
    // ============================================================================================

    public PUUID generatePUUID(){

        long m_uuid = UUID.randomUUID().getMostSignificantBits();
        long l_uuid = UUID.randomUUID().getLeastSignificantBits();

        PUUID puuid = PUUID.newBuilder().setMostSignificantBits(m_uuid).setLeastSignificantBits(l_uuid).build();

        return puuid;
    }

    public PUUID setPUUID(long most, long least){

        long m_uuid = most;
        long l_uuid = least;

        PUUID puuid = PUUID.newBuilder().setMostSignificantBits(m_uuid).setLeastSignificantBits(l_uuid).build();

        return puuid;
    }

    public class Data {
        public Object object;
        public short key;
        public String msg;
    }

    // ============================================================================================
    // Responses to client application from server application (Start)
    // ============================================================================================

    // Check status
    private void checkStatus(Response response) {
        if (response.getStatus().getSuccess()) {
            switchResponses(response);
        } else {
            switchErrorResponse(response);
        }
    }

    // Switch Error response
    private void switchErrorResponse(Response r) {
        ErrorCode code = r.getStatus().getError().getCode();
        switch (code) {
            case INTERNAL_SERVER_ERROR:
                AppConst.LogTag.e("INTERNAL_SERVER_ERROR");
                break;
            case USER_NOT_FOUND:
                AppConst.LogTag.e("USER_NOT_FOUND");
                break;
            case WRONG_REQUEST:
                AppConst.LogTag.e("WRONG_REQUEST");
                break;
            case NO_HISTORY:
                AppConst.LogTag.e("NO_HISTORY");
                break;
            case USER_NOT_EXIST:
                AppConst.LogTag.e("USER_NOT_EXIST");
                break;
            case USER_WITH_SUCH_TEL_EXIST:
                AppConst.LogTag.e("USER_WITH_SUCH_TEL_EXIST");
                break;
            case INCORECT_JSON:
                AppConst.LogTag.e("INCORRECT_JSON");
                break;
            case INVALID_SMS_CODE:
                AppConst.LogTag.e("INVALID_SMS_CODE");
                break;
            case INVALID_DATA:
                AppConst.LogTag.e("INVALID_DATA");
                break;
            case NO_SUCH_ACCESS_TOKEN:
                AppConst.LogTag.e("NO_SUCH_ACCESS_TOKEN");
                break;
            case ITEM_NOT_EXIST:
                AppConst.LogTag.e("ITEM_NOT_EXIST");
                break;
            case TOO_MANY_REG_ATTEMPTS:
                AppConst.LogTag.e("TOO_MANY_REG_ATTEMPTS");
                break;
            case SMS_CODE_IS_TOO_OLD:
                AppConst.LogTag.e("SMS_CODE_IS_TOO_OLD");
                break;
            case INVALID_PHONE:
                AppConst.LogTag.e("INVALID_PHONE");
                break;
            case FILE_NOT_EXISTS:
                AppConst.LogTag.e("FILE_NOT_EXISTS");
                break;
            case FILE_MOVE_ERROR:
                AppConst.LogTag.e("FILE_MOVE_ERROR");
                break;
            case INTERNAL_DATABASE_ERROR:
                AppConst.LogTag.e("INTERNAL_DATABASE_ERROR");
                break;
            case INNACTIVE_TOKEN:
                AppConst.LogTag.e("INACTIVE_TOKEN");
                break;
            case TOKEN_VALUE_NOT_EXIST:
                AppConst.LogTag.e("TOKEN_VALUE_NOT_EXIST");
                break;
        }
    }

    // Switch responses
    private void switchResponses(Response _response) {
        switch (_response.getType()) {
            case LIST_ADD:
                listAddResponse(_response);
                break;
            case LISTS_GET:
                listGetResponse(_response);
                break;
            case LIST_UPDATE:
                listUpdateResponse(_response);
                break;
            case LIST_REMOVE:
                listRemoveResponse(_response);
                break;
            case LIST_ITEM_ADD:
                listItemAddResponse(_response);
                break;
            case LIST_ITEMS_GET:
                listItemsGetResponse(_response);
                break;
            case LIST_ITEM_UPDATE:
                listItemUpdateResponse(_response);
                break;
            case LIST_ITEM_REMOVE:
                listItemRemoveResponse(_response);
                break;
            case PUBLIC_LIST_GET:
                publicListGetResponse(_response);
                break;
            case COMMENT_ADD:
                commentAddResponse(_response);
                break;
            case COMMENT_GET:
                commentGetResponse(_response);
                break;
            case COMMENT_UPDATE:
                commentUpdateResponse(_response);
                break;
            case COMMENT_REMOVE:
                commentRemoveResponse(_response);
                break;
            case HISTORY_ADD:
                historyAddResponse(_response);
                break;
            case HISTORY_GET:
                historyGetResponse(_response);
                break;
            case SEND_MESSAGE:
                sendMessageResponse(_response);
                break;
            //TODO Delete?
           /* case SEND_LIST_ITEM:
                sendListItemResponse(_response);
                break;*/
            case UNKNOWN:
                unknownResponse(_response);
                break;
            case MESSAGE:
                messageResponse(_response);
                break;
            case LIST_ITEM_GET:
                listItemGetResponse(_response);
                break;
            case CREATE_CHATROOM:
                createChatRoomResponse(_response);
                break;
            case CHAT_ROOM_HISTORY_GET:
                getChatRoomHistoryResponse(_response);
                break;
            case REMOVE_CHATROOM:
                removeChatRoomResponse(_response);
                break;
            case GET_ALL_ROOMS_FOR_USER:
                getAllRoomsForUserResponse(_response);
                break;
            case LEAVE_USER_FROM_ROOM:
                leaveUserFromRoomResponse(_response);
                break;
            case JOIN_USER_TO_ROOM:
                joinUserToRoomResponse(_response);
                break;
            case GET_ALL_USERS_FOR_ROOM:
                getAllUsersForRoomResponse(_response);
                break;
            case CONTACT_CREATE:
                contactCreateResponse(_response);
                break;
            case CONTACT_REMOVE:
                contactRemoveResponse(_response);
                break;
            case CONTACT_SET_FIELDS:
                contactSetFieldResponse(_response);
                break;
            case CONTACT_REMOVE_FIELD:
                contactRemoveFieldResponse(_response);
                break;
            case CONTACT_FIELDS_GET:
                contactFieldsGetResponse(_response);
                break;
            case CONTACTS_GET:
                getAllContactsResponse(_response);
                break;
            case AUTH_ADD:
                authAddResponse(_response);
                break;
            case AUTH_CONFIRM:
                authConfirmResponse(_response);
                break;
            case AUTH_REMOVE:
                authRemoveResponse(_response);
                break;
            case AUTH_GET:
                authGetResponse(_response);
                break;
            case GET_USER_BY_ID:
                getUserByIdResponse(_response);
                break;
            case GET_USER_BY_AUTH:
                getUserByAuthResponse(_response);
                break;
            case SET_PROFILE_FIELD:
                setProfileFieldResponse(_response);
                break;
            case PROFILE_GET:
                getProfileFieldsResponse(_response);
                break;
            case REMOVE_PROFILE_FIELD:
                removeProfileFieldResponse(_response);
                break;
            case REMOVE_PROFILE:
                removeProfileResponse(_response);
                break;
            case SEARCH_USERS_BY_AUTH:
                searchUsersByAuthResponse(_response);
                break;
        }
    }

    // =========== Lists response ===========

    private void listAddResponse(Response rsp) {
        PUUID puuidList = null;
        try {
            puuidList = AddListResponse.parseFrom(rsp.getResponse()).getId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        sendPostData(puuidList, CallType.LIST_ADD_VALUE);
    }

    private void listGetResponse(Response rsp) {
        ArrayList<com.smartchat.objects.List> lists = DataManager.listGetDataResponse(rsp);
        AppConst.LogTag.i(lists.toString());
        sendPostData(lists, CallType.LISTS_GET_VALUE);
    }

    private void listUpdateResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
        sendPostData(null, CallType.LIST_UPDATE_VALUE);
    }

    private void listRemoveResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
        sendPostData(null, CallType.LIST_REMOVE_VALUE);
    }

    private void publicListGetResponse(Response rsp){
        ArrayList<com.smartchat.objects.List> lists = DataManager.listGetDataResponse(rsp);
        sendPostData(lists, CallType.PUBLIC_LIST_GET_VALUE);
    }

    // =========== List Items response ===========

    private void listItemAddResponse(Response rsp) {
        PUUID puuidItem = null;
        try {
            puuidItem = AddListItemResponse.parseFrom(rsp.getResponse()).getId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        sendPostData(puuidItem, CallType.LIST_ITEM_ADD_VALUE);
    }

    private void listItemsGetResponse(Response rsp) {
        final ArrayList<com.smartchat.objects.ListItem> itemsList = DataManager.listItemsGetDataResponse(rsp);
        sendPostData((Object)itemsList, CallType.LIST_ITEMS_GET_VALUE);
    }

    private void listItemUpdateResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
        sendPostData(null, CallType.LIST_ITEM_UPDATE_VALUE);
    }

    private void listItemRemoveResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
        sendPostData(null, CallType.LIST_ITEM_REMOVE_VALUE);
    }

    private void sendListItemResponse(Response rsp) {
        PUUID messageId = null;
        try {
            messageId = SendListItemResponse.parseFrom(rsp.getResponse()).getMessageId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(messageId.toString());
    }

    private void listItemGetResponse(Response rsp) {
        List<com.smartchat.server.SmartChat.ListItem> listItems = null;
        try {
            listItems = GetListItemsResponse.parseFrom(rsp.getResponse()).getItemsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(listItems.toString());
    }

    // =========== Comment response ===========

    private void commentAddResponse(Response rsp) {
        PUUID id = null;
        try {
            id = AddCommentResponse.parseFrom(rsp.getResponse()).getId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(id.toString());
    }

    private void commentGetResponse(Response rsp) {

        List<com.smartchat.server.SmartChat.Comment> comments = null;
        try {
            comments = GetCommentsResponse.parseFrom(rsp.getResponse()).getCommentsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(comments.toString());
    }

    private void commentUpdateResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void commentRemoveResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    // =========== History response ===========

    private void historyAddResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void historyGetResponse(Response rsp) {
        ArrayList<com.smartchat.objects.Message> messages = DataManager.historyGetDataResponse(rsp);
        AppConst.LogTag.i(messages.toString());
        // TODO SEND THIS
    }

    // =========== Message response ===========

    private void sendMessageResponse(Response rsp) {
        PUUID id = null;
        try {
            id = SendMessageResponse.parseFrom(rsp.getResponse()).getMessageId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(id.toString());
        sendPostData(null, CallType.SEND_MESSAGE_VALUE);
    }

    private void messageResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);

        sendPostData(DataManager.getMessageResponse(rsp), CallType.MESSAGE_VALUE);
    }

    // =========== Unknown response ===========

    private void unknownResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    // =========== ChatRoom response ===========

    private void createChatRoomResponse(Response rsp) {
        AppConst.LogTag.i(rsp.toString());
        PUUID id = null;
        try {
            id = CreateChatRoomResponse.parseFrom(rsp.getResponse()).getId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        sendPostData(id, CallType.CREATE_CHATROOM_VALUE);
    }

    private void getChatRoomHistoryResponse(Response rsp){
        ArrayList<com.smartchat.objects.Message> history = DataManager.historyGetDataResponse(rsp);
        sendPostData(history, CallType.CHAT_ROOM_HISTORY_GET_VALUE);
    }

    private void removeChatRoomResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
        sendPostData(null, CallType.REMOVE_CHATROOM_VALUE);
    }

    private void getAllRoomsForUserResponse(Response rsp) {
        AppConst.LogTag.i(rsp.toString());
        ArrayList<com.smartchat.objects.ChatRoom> chatRooms = DataManager.getAllRoomsForUserDataResponse(rsp);
        AppConst.LogTag.i(chatRooms.toString());
        sendPostData(chatRooms, CallType.GET_ALL_ROOMS_FOR_USER_VALUE);
    }

    private void leaveUserFromRoomResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void joinUserToRoomResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void getAllUsersForRoomResponse(Response rsp) {
        ArrayList<com.smartchat.objects.UserShort> userShorts = DataManager.getAllUsersForRoomDataResponse(rsp);
        AppConst.LogTag.i("CHAT: " + userShorts.toString());
        sendPostData(userShorts, CallType.GET_ALL_USERS_FOR_ROOM_VALUE);
    }

    // =========== Contact response ===========

    private void contactCreateResponse(Response rsp) {
        PUUID puuidContact = null;
        try {
            puuidContact = CreateContactResponse.parseFrom(rsp.getResponse()).getId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(puuidContact.toString());
    }

    private void contactRemoveResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void contactSetFieldResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void contactRemoveFieldResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void contactFieldsGetResponse(Response rsp) {
        List<SmartChat.Field> fields = null;
        try {
            fields = GetAllContactFieldsResponse.parseFrom(rsp.getResponse()).getFieldsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(fields.toString());
    }

    private void getAllContactsResponse(Response rsp) {
        ArrayList<com.smartchat.objects.Contact> contactArrayList = dm.getAllContactsResponse(rsp);
        sendPostData(contactArrayList, CallType.CONTACTS_GET_VALUE);
    }

    // =========== Auth response ===========

    private void authAddResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);

    }

    private void authConfirmResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void authRemoveResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void authGetResponse(Response rsp) {
        sendPostData(DataManager.getAuthFieldsResponse(rsp), CallType.AUTH_GET_VALUE);
    }

    // =========== User response ===========

    private void getUserByIdResponse(Response rsp) {
        com.smartchat.server.SmartChat.User user = null;
        try {
            user = GetUserByIdResponse.parseFrom(rsp.getResponse()).getUser();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(user.toString());
    }

    private void getUserByAuthResponse(Response rsp) {
        com.smartchat.server.SmartChat.User user = null;
        try {
            user = GetUserByAuthResponse.parseFrom(rsp.getResponse()).getUser();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        AppConst.LogTag.i(user.toString());
    }

    private void setProfileFieldResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        Log.d("PROFILE", "profile fields response " + rsp.toString());
        sendPostData(status, CallType.SET_PROFILE_FIELD_VALUE);
    }

    private void getProfileFieldsResponse(Response rsp) {
        sendPostData(DataManager.getAllFieldsResponse(rsp),CallType.PROFILE_GET_VALUE);
    }

    private void removeProfileFieldResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }

    private void removeProfileResponse(Response rsp) {
        boolean status = rsp.getStatus().getSuccess();
        AppConst.LogTag.i("status " + status);
    }



    private void searchUsersByAuthResponse(Response rsp) {

        ArrayList<com.smartchat.objects.UserAuth> userAuth =
                DataManager.searchUsersByAuthDataReponse(rsp);

        Log.d("Genka", ".... Try to get response ..... " );
        if (userAuth != null){
            sendPostData(userAuth, CallType.SEARCH_USERS_BY_AUTH_VALUE);
            Log.d("Genka", ".... Response not null!!!!!!!! ...... " + userAuth.size());
        }

    }

    // ============================================================================================
    // Responses to client application from server application (END)
    // ============================================================================================

}

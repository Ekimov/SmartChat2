package com.smartchat.server;

import android.util.Log;

import com.smartchat.objects.Auth;
import com.smartchat.objects.ChatRoom;
import com.smartchat.objects.Contact;
import com.smartchat.objects.ContactMessage;
import com.smartchat.objects.Field;
import com.smartchat.objects.List;
import com.smartchat.objects.ListItem;
import com.smartchat.objects.Message;
import com.smartchat.objects.MessageBody;
import com.smartchat.objects.Profile;
import com.smartchat.objects.UserAuth;
import com.smartchat.objects.UserShort;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.LogTag;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;

public class DataManager {

    // Return ArrayList with List
    public static ArrayList<List> listGetDataResponse(SmartChat.Response _rsp) {
        ArrayList<List> lists = new ArrayList<List>();

        java.util.List<SmartChat.List> smClists = null;
        try {
            smClists = SmartChat.GetListsResponse.parseFrom(_rsp.getResponse()).getListsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        int smClistsSize = smClists.size();
        for (int i = 0; i < smClistsSize; i++) {
            SmartChat.List list = smClists.get(i);
            lists.add(new List(list.getId(), list.getName(), AppConst.LIST_DEF_DESCRIPTION,
                    AppConst.LIST_DEF_IMAGE_LIST_SOURCE, AppConst.LIST_DEF_AUTOR,
                    AppConst.LIST_CONTENT_TYPE_TEXT_ONLY, AppConst.LIST_DEF_LANGUAGE,
                    AppConst.LIST_DEF_TOPIC_CODE, AppConst.LIST_DEF_RANK,
                    AppConst.LIST_DEF_STATUS, AppConst.LIST_DEF_BUYTYPE));
        }

        return lists;
    }

    // Return ArrayList with ListItem
    public static ArrayList<ListItem> listItemsGetDataResponse(SmartChat.Response _rsp) {
        ArrayList<ListItem> listItems = new ArrayList<ListItem>();

        java.util.List<SmartChat.ListItem> itemsList = null;
        try {
            itemsList = SmartChat.GetListItemsResponse.parseFrom(_rsp.getResponse()).getItemsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        int itemsListSize = itemsList.size();
        for (int i = 0; i < itemsListSize; i++) {
            SmartChat.ListItem item = itemsList.get(i);
            listItems.add(new ListItem(item.getId(), item.getText(), null,
                    AppConst.LIST_CONTENT_TYPE_TEXT_ONLY, AppConst.LIST_DEF_RANK));
        }

        /*for(SmartChat.ListItem item : itemsList) {
            listItems.add(new ListItem(item.getId(), item.getText(), null,
                    AppConst.LIST_CONTENT_TYPE_TEXT_ONLY, AppConst.LIST_DEF_RANK));
        }*/

        return listItems;
    }

    // Return ArrayList with Contact
    public static ArrayList<Contact> contactsGetDataResponse(SmartChat.Response _rsp) {
        ArrayList<Contact> contactItems = new ArrayList<Contact>();

        java.util.List<SmartChat.Contact> itemsList = null;
        try {
            itemsList = SmartChat.GetAllContactsResponse.parseFrom(_rsp.getResponse()).getContactsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

       /* for(SmartChat.Contact i : itemsList) {
            contactItems.add(new Contact(i.getId(), i.));
        }
*/
        return contactItems;
    }

    // Return ArrayList with Room
    public static ArrayList<ChatRoom> getAllRoomsForUserDataResponse(SmartChat.Response _rsp) {
        ArrayList<ChatRoom> chatRooms = new ArrayList<ChatRoom>();

        java.util.List<SmartChat.ChatRoom> chatRoomList = null;
        try {
            chatRoomList = SmartChat.GetAllRoomsForUserResponse.parseFrom(_rsp.getResponse()).getChatRoomsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        for (SmartChat.ChatRoom i : chatRoomList) {
            ChatRoom room = new ChatRoom(i.getId(), i.getName());
            room.setTimeDestroy(AppConst.DEF_LIFETIME);
            room.setAutor(AppConst.LIST_DEF_AUTOR);
            room.setSequrityLevel(0);
            room.setType(i.getChatType());
            //room.setImageId();
            room.setLastMessage(AppConst.DEF_LAST_MESSAGE);
            room.setLastMessageStatus(AppConst.DEF_LAST_MESSAGE_STATUS);
            room.setDescription(i.getChatDescription());
            room.setCountUnreadMessage(AppConst.DEF_COUNT_UNREAD_MESSAGE);
            chatRooms.add(room);
        }
        return chatRooms;
    }

    //Return MessageBody object
    public static MessageBody getMessageResponse(SmartChat.Response _rsp) {

        SmartChat.Message message = null;
        try {
            message = SmartChat.Message.parseFrom(_rsp.getResponse());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        Log.d("MyTest", message.toString());

        MessageBody messageBody = new MessageBody(0);
        messageBody.setData(message.getBodiesList().get(0).getText());
        messageBody.setDate(message.getMessageDate());
        messageBody.setCheck(message.getId());
        messageBody.setChatRoom(message.getChatRoomId());
        messageBody.setFrom(message.getUserFrom());
        return messageBody;
    }

    // Return ArrayList with history Message
    public static ArrayList<Message> historyGetDataResponse(SmartChat.Response _rsp) {
        ArrayList<Message> messages = new ArrayList<Message>();

        java.util.List<SmartChat.Message> messageList = null;
        try {
            messageList = SmartChat.GetHistoryResponse.parseFrom(_rsp.getResponse()).getMessagesList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        for (SmartChat.Message i : messageList) {
            ArrayList<MessageBody> messageBodies = new ArrayList<MessageBody>();
            java.util.List<SmartChat.MessageBody> msBodies = i.getBodiesList();
            for (int j = 0; j < msBodies.size(); j++) {
                SmartChat.MessageBody body = msBodies.get(j);
                MessageBody messageBody = new MessageBody(body.getType().getNumber());
                messageBody.setData(body.getText());
                messageBody.setFrom(i.getUserFrom());
                messageBodies.add(messageBody);
            }
            messages.add(new Message(i.getId(), i.getChatRoomId(),
                    new ContactMessage(i.getUserFrom(), AppConst.LIST_DEF_NAME,
                            AppConst.LIST_DEF_IMAGE_LIST_SOURCE), i.getSecurityLevel(),
                    i.getTimeDestroy(), i.getMessageDate(), messageBodies, i.hasTimeDestroy()));
        }
        return messages;
    }

    // Return ArrayList with UserShort
    public static ArrayList<UserShort> getAllUsersForRoomDataResponse(SmartChat.Response _rsp) {
        /*For download use
        http://54.148.194.39/api/v1/files/UUID_OF_FILE?accessToken=ACCESS_TOKEN_UUID
        // , where UUID_OF_FILE - unique identifier returned from server,
        // ACCESS_TOKEN_UUID - unique access token.

        Server will directly return the file. In the near future it will be return a HTTP-redirect ( 307 Temporary Redirect ) to file location URL on server.*/

        ArrayList<UserShort> userShorts = new ArrayList<UserShort>();

        java.util.List<SmartChat.UserShort> userShortList = null;
        try {
            userShortList = SmartChat.GetAllUsersForRoomResponse.parseFrom(_rsp.getResponse()).getUsersList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        for (SmartChat.UserShort i : userShortList) {
            userShorts.add(new UserShort(i.getUserId().getLeastSignificantBits(), i.getUserId().getMostSignificantBits(), i.getName(),
                    "http://54.148.194.39/api/v1/files/" + i.getFileImageId() + "?accessToken="));
        }
        return userShorts;
    }

    public static Auth getAuthFieldsResponse (SmartChat.Response _rsp){
        java.util.List<SmartChat.Auth> auth = null;
        try {
            auth = SmartChat.GetAuthResponse.parseFrom(_rsp.getResponse()).getAuthList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        //auth.get(0).get
        Log.d("AUTHTEST", auth.toString());
        AppConst.LogTag.i(auth.toString());

        SmartChat.AuthType type = null;
        String value = "";

        if(auth.size() > 0){
            type = auth.get(0).getType();
            value = auth.get(0).getValue();
        }

        Auth profileAuth = new Auth(type, value);

        return profileAuth;
    }

    // Return ArrayList with Fields
    public static Profile getAllFieldsResponse(SmartChat.Response _rsp) {

        SmartChat.PUUID puuid = null;
        SmartChat.GetAllFieldsResponse fieldsResponse = null;
        java.util.List<SmartChat.Field> profileList = null;

        try {
            fieldsResponse = SmartChat.GetAllFieldsResponse.parseFrom(_rsp.getResponse());
            profileList = fieldsResponse.getFieldsList();
            puuid = SmartChat.GetAllFieldsResponse.parseFrom(_rsp.getResponse()).getUserId();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        Profile profile = new Profile(puuid);

        profile.setFavoriteListId(fieldsResponse.getFavoriteListId());

        int count = profileList.size();
        AppConst.LogTag.d(count + "");
        for (int i = 0; i < count; i++) {
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.LOGIN_NAME_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "LOGIN_NAME_VALUE");
                    profile.setLoginName(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.NICKNAME_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "NICKNAME_VALUE");
                    profile.setNickName(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.FIRSTNAME_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "FIRSTNAME_VALUE");
                    profile.setFirstName(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.LASTNAME_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "LASTNAME_VALUE");
                    profile.setLastName(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.SEX_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "SEX_VALUE");
                    profile.setSex(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.BIRTHDATE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "BIRTHDATE_VALUE");
                    profile.setBirthDate(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.IMAGE_LINK_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "IMAGE_LINK_VALUE");
                    profile.setImageLink(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.EMAIL_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "EMAIL_VALUE");
                    profile.setEmail(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.PHONE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "PHONE_VALUE");
                    profile.setPhone(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.LANGUAGE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "LANGUAGE_VALUE");
                    profile.setLanguage(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.TIMEZONE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "TIMEZONE_VALUE");
                    profile.setTimezone(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.COUNTRY_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "COUNTRY_VALUE");
                    profile.setCountry(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.ABOUT_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "ABOUT_VALUE");
                    profile.setAbout(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.WEBSITE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "WEBSITE_VALUE");
                    profile.setWebsite(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.STREET_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "STREET_VALUE");
                    profile.setStreet(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.CITY_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "CITY_VALUE");
                    profile.setCity(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.POST_CODE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "POST_CODE_VALUE");
                    profile.setPostCode(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.APPARTAMENT_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "APPARTAMENT_VALUE");
                    profile.setAppartaments(profileList.get(i).getValue());
                }

            }
            if (profileList.get(i).getType().getNumber() == SmartChat.FieldType.HOUSE_VALUE) {
                if (profileList.get(i).hasValue()) {
                    Log.d("Get", "HOUSE_VALUE");
                    profile.setHouse(profileList.get(i).getValue());
                }

            }
/*            if(profileList.get(i).getType().getNumber() == SmartChat.FieldType.LIS) {
                if(profileList.get(i).hasValue()) {
                    Log.d("Get", "IMAGE_ID_VALUE");
                    profile.setImageLink(profileList.get(i).getValue());
                }

            }*/
        }

        return profile;
    }

    public static ArrayList< UserAuth> searchUsersByAuthDataReponse(SmartChat.Response rsp) {
        ArrayList <UserAuth> userAuthResponse = new ArrayList<>();
        java.util.List<SmartChat.UserAuth> userAuth = null;
        try {
            userAuth = SmartChat.SearchUsersByAuthReponse.parseFrom(rsp.getResponse()).getAuthList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            Log.d("Genka", "InvalidProtocolBufferException" );
        }

        for (int i=0; i<userAuth.size(); i++) {
            UserAuth userAuthObject = new UserAuth();
            SmartChat.UserAuth userAuthSmartChat = userAuth.get(i);
            SmartChat.UserShort userShort = userAuthSmartChat.getUser();
            Log.d("Genka", "Response find users" + userShort.getName());
            SmartChat.Auth serverAuth = userAuthSmartChat.getAuth();
            Auth clientAuth = new Auth(serverAuth.getType(), serverAuth.getValue(), serverAuth.getConfirmed());

            userAuthObject.setUsPuuid(userAuth.get(i).getUser().getUserId());
            userAuthObject.setUserAuth(clientAuth);

            userAuthResponse.add(userAuthObject);
            Log.d("Test2", userAuthObject.getUserAuth().getValue());
        }
        return userAuthResponse;
    }

/*    public static UserAuth searchUsersByAuthDataReponse(SmartChat.Response rsp) {
        UserAuth userAuthObject = new UserAuth();
        java.util.List<SmartChat.UserAuth> userAuth = null;
        try {
            userAuth = SmartChat.SearchUsersByAuthReponse.parseFrom(rsp.getResponse()).getAuthList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        int userAuthSize = userAuth.size();
        if (userAuthSize==0){
            return null;
        }
        SmartChat.UserAuth userAuthSmartChat = userAuth.get(0);
        SmartChat.Auth serverAuth = userAuthSmartChat.getAuth();
        Auth clientAuth = new Auth(serverAuth.getType(), serverAuth.getValue(), serverAuth.getConfirmed());

        userAuthObject.setUsPuuid(userAuth.get(0).getUser().getUserId());
        userAuthObject.setUserAuth(clientAuth);

        Log.d("Test2", userAuthObject.getUserAuth().getValue());

        return userAuthObject;
    }*/

    //Return all users contact
    public ArrayList<Contact> getAllContactsResponse(SmartChat.Response rsp) {
        ArrayList<Contact> contactArrayList = new ArrayList<Contact>();
        java.util.List<SmartChat.Contact> contacts = null;
        try {
            contacts = SmartChat.GetAllContactsResponse.parseFrom(rsp.getResponse()).getContactsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        int size = contacts.size();
        for (int i = 0; i < size; i++) {
            SmartChat.Contact o = contacts.get(i);
            contactArrayList.add(new Contact(o.getId(), o.getUserId(), checkAllFields(o.getFieldsList())));
        }

        return contactArrayList;
    }

    private ArrayList<Field> checkAllFields(java.util.List<SmartChat.Field> fields) {
        ArrayList<Field> fieldArrayList = new ArrayList<>();

        int sizeField = fields.size();
        for (int j = 0; j < sizeField; j++) {
            SmartChat.Field f = fields.get(j);
            String value = f.getValue();
            SmartChat.FieldType type = f.getType();
            if (!value.isEmpty()) {
                fieldArrayList.add(new Field(type, value));
            }
        }

        return fieldArrayList;
    }

    public static String getValueOfType(ArrayList<Field> fields, SmartChat.FieldType fieldType) {
        String value = null;
        int size = fields.size();

        for (int i = 0; i < size; i++) {
            switch (fieldType) {
                case LOGIN_NAME:
                    value = fields.get(i).getValue();
                    break;
                case NICKNAME:
                    value = fields.get(i).getValue();
                    break;
                case FIRSTNAME:
                    value = fields.get(i).getValue();
                    break;
                case LASTNAME:
                    value = fields.get(i).getValue();
                    break;
                case SEX:
                    value = fields.get(i).getValue();
                    break;
                case BIRTHDATE:
                    value = fields.get(i).getValue();
                    break;
                case IMAGE_LINK:
                    value = fields.get(i).getValue();
                    break;
                case EMAIL:
                    value = fields.get(i).getValue();
                    break;
                case PHONE:
                    value = fields.get(i).getValue();
                    break;
                case LANGUAGE:
                    value = fields.get(i).getValue();
                    break;
                case TIMEZONE:
                    value = fields.get(i).getValue();
                    break;
                case COUNTRY:
                    value = fields.get(i).getValue();
                    break;
                case ABOUT:
                    value = fields.get(i).getValue();
                    break;
                case WEBSITE:
                    value = fields.get(i).getValue();
                    break;
                case STREET:
                    value = fields.get(i).getValue();
                    break;
                case CITY:
                    value = fields.get(i).getValue();
                    break;
                case POST_CODE:
                    value = fields.get(i).getValue();
                    break;
                case APPARTAMENT:
                    value = fields.get(i).getValue();
                    break;
                case HOUSE:
                    value = fields.get(i).getValue();
                    break;
                case IMAGE_ID:
                    value = fields.get(i).getValue();
                    break;
            }
        }

        return value;
    }
}
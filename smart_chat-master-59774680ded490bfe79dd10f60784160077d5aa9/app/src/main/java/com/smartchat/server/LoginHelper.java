package com.smartchat.server;


import android.util.Log;


import com.smartchat.utils.AppConst;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class LoginHelper {

    private InputStream is = null;
    private JSONObject jObj = null;
    private String json, url;
    private ArrayList<String> list;

    private final String URL_reg = AppConst.RELEASE_REG;
    private final String URL_conf = AppConst.RELEASE_CONF;

    public ArrayList<String> makeLogin(String key, String data, String id, int CONST) {

        try {

        list = new ArrayList<String>();
        jObj = new JSONObject();

        switch (key){

            case AppConst.LOGIN_REG:

                url = URL_reg;

                switch (CONST) {

                    case AppConst.PHONE:
                        jObj.put("mobile", data);
                        break;
                    case AppConst.EMAIL:
                        jObj.put("email", data);
                        break;

                }

                break;

            case AppConst.LOGIN_CONF:

                url = URL_conf;
                jObj.put("accessToken", id);
                jObj.put("code", data);

                break;
        }

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");

            httpPost.setEntity(new StringEntity(jObj.toString()));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            System.out.println(">>>>>>>>   "+json);
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        try {

        String status, accessToken, errorMessage;
        status = jObj.getString("status");
        list.add(status);

        switch (key){

            case AppConst.LOGIN_REG:
                if (status.equals("0")) {
                    accessToken = jObj.getString("accessToken");
                    list.add(accessToken);
                }
                else {
                    errorMessage = jObj.getString("errorMessage");
                    list.add(errorMessage);
                }
                break;
            case AppConst.LOGIN_CONF:
                if (!status.equals("0")) {
                    errorMessage = jObj.getString("errorMessage");
                    list.add(errorMessage);
                }
                else {
                    list.add(null);
                    list.add(jObj.getString("userId"));
                    //list.add(jObj.getString("favoriteListId"));
                }

        }
            return list;

        } catch (JSONException ex){
            ex.printStackTrace();
        }
        return null;
    }

}

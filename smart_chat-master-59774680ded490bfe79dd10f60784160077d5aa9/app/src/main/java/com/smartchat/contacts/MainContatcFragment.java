package com.smartchat.contacts;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.actionbar.StripBar;
import com.smartchat.actionbar.TitleBar;
import com.smartchat.activity.MainActivity;
import com.smartchat.chats.ChatListFragment;
import com.smartchat.profile.ProfileFragment;
import com.smartchat.utils.AppConst;

/**
 * Created by tropic4 on 14.04.15.
 */
public class MainContatcFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private TextView title;
    private ImageView title_image_left, title_image_right;
    private TitleBar titleBar;
    private StripBar stripBar;


    private String tabsConstants[] = {
            AppConst.APP_CONTACTS,
            AppConst.PHONE_CONTACTS};


    private EditText searchText;

@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    pagerAdapter = new PagerAdapter(getChildFragmentManager());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.title_image_left:

                Fragment fragment = new ProfileFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, fragment);
                ft.addToBackStack(null);
                ft.commit();

                break;

            case R.id.title_image_right:

                break;
        }

    }

    private class PagerAdapter extends FragmentPagerAdapter  {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {

            return new ContactFragment().newInstance(position);
        }

//        public Fragment getItem(int position) {
//            Fragment fragment = new ContactFragment();
//            Bundle bundle = new Bundle();
//            bundle.putInt("tabsConstants", tabsConstants[position]);
//            fragment.setArguments(bundle);
//            return fragment;
//        }

        @Override
        public int getCount() {
            return tabsConstants.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabsConstants[position];
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((MainActivity) getActivity()).menuBarIsVisible();

        view = inflater.inflate(R.layout.contacts_contacts_frament, container, false);

        init();
        setActionBar();
        setListener();

        return view;

    }

    private void init() {

        pager = (ViewPager) view.findViewById(R.id.contact_pager);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                stripBar.changeStrip(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(pagerAdapter);

//        pager.setPageTransformer(true, new ZoomOutPageTransformer());

        title = (TextView) view.findViewById(R.id.title);
        title_image_left = (ImageView) view.findViewById(R.id.title_image_left);
        title_image_right = (ImageView) view.findViewById(R.id.title_image_right);
    }

    private void setActionBar(){

        titleBar = new TitleBar(title, title_image_left, title_image_right, AppConst.CONTACTS);
        stripBar = new StripBar(getActivity(), view, AppConst.CONTACTS);
    }

    private void setListener(){

        title_image_left.setOnClickListener(this);
        title_image_right.setOnClickListener(this);
    }


//        View v = inflater.inflate(R.layout.contact_fragment, null);
//        ((MainActivity) getActivity()).menuMiniVisible();
//
//        searchText = (EditText) v.findViewById(R.id.edit_searching);
//
//
//        //viewpager  to contactFragments and refresh action bar
//        mPager = (ViewPager) v.findViewById(R.id.contactViewPager);
//        mPager.setAdapter(pagerAdapter);
//        onPageChangeListener = new MyViewPager(getActivity(), mPager, tabsName, v);
//        mPager.setOnPageChangeListener(onPageChangeListener);
//        //select first tab
//        onPageChangeListener.onPageSelected(0);
//
//        ImageView addContactButton = (ImageView) v.findViewById(R.id.action_bar_right_button);
//        addContactButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (getContactFragment == null)
//                    getContactFragment = new GetContactFragment();
//                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,
//                        R.anim.slide_back_in_left, R.anim.slide_back_out_right);
//                transaction.add(R.id.frgmCont, getContactFragment);
//                transaction.addToBackStack("tag");
//                transaction.commit();
//
//            }
//        });
//
//        ImageView profileImage = (ImageView) v.findViewById(R.id.action_bar_left_button);
//        profileImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Fragment profile = new ProfileFragment();
//                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.setCustomAnimations(R.anim.slide_back_in_left, R.anim.slide_back_out_right,
//                        R.anim.slide_in_left, R.anim.slide_out_right);
//                transaction.replace(R.id.frgmCont, profile);
//                transaction.addToBackStack("tag");
//                transaction.commit();
//            }
//        });
//
//        return v;
//    }


}

package com.smartchat.contacts;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.actionbar.TitleBar;
import com.smartchat.activity.MainActivity;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.LogTag;

public class ContactsFragment extends Fragment{

    private View view;
    private TextView title;
    private ImageView title_image_left, title_image_right;
    private TitleBar titleBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((MainActivity)getActivity()).menuBarIsVisible();

        view = inflater.inflate(R.layout.contacts_contacts_frament, container, false);

        init();
        setActionBar();

        return view;
    }

    private void init(){

        title = (TextView) view.findViewById(R.id.title);
        title_image_left = (ImageView) view.findViewById(R.id.title_image_left);
        title_image_right = (ImageView) view.findViewById(R.id.title_image_right);
    }

    private void setActionBar(){

        titleBar = new TitleBar(title, title_image_left, title_image_right, AppConst.CONTACTS);
    }
}

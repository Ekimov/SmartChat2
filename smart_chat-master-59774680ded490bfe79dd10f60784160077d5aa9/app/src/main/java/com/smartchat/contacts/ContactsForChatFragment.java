package com.smartchat.contacts;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.contacts.adapter.ContactsForChatAdapter;
import com.smartchat.objects.Contact;
import com.smartchat.objects.PostObject;
import com.smartchat.objects.UserShort;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.DataManager;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.ColorResorces;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class ContactsForChatFragment extends Fragment implements View.OnClickListener {

    private ArrayList<UserShort> userShortArrayList = new ArrayList<UserShort>();
    private Map<String, Integer> mapIndex;
    private ArrayList<UserShort> contacts = new ArrayList<UserShort>();

    private View v;
    private ListView myContactList;
    private ImageButton addNewContacts;
    private ProgressBar getAllContactsProgressBar;
    private LinearLayout contentLayout, addNewContactsLayout;
    private ContactsForChatAdapter adapter;
    private ConnectHelper connectHelper = MainActivity.connectHelper;

    public interface NewUserShortArrayListener {
        void onFinishGetContact(ArrayList<UserShort> users);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.contacts_for_chat_fragment, null);

        myContactList = (ListView) v.findViewById(R.id.my_contact_list);
        getAllContactsProgressBar = (ProgressBar) v.findViewById(R.id.getAllContactsProgressBar);
        getAllContactsProgressBar.setIndeterminate(true);

        contentLayout = (LinearLayout) v.findViewById(R.id.contentLayout);
        addNewContactsLayout = (LinearLayout) v.findViewById(R.id.addNewContactsLayout);

        addNewContacts = (ImageButton) v.findViewById(R.id.addNewContacts);
        addNewContacts.setOnClickListener(this);

        setBaseVisibility();

        ((MainActivity) getActivity()).menuBarIsGone();
        setAdapter();
        initActionBar();
        // TODO del this
        getAllContactOfServer();

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getAllContactOfServer();
        getBaseArguments();
    }

    private void setBaseVisibility() {
        getAllContactsProgressBar.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.VISIBLE);
        addNewContactsLayout.setVisibility(View.GONE);
    }

    private void noContactsVisibility() {
        getAllContactsProgressBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        addNewContactsLayout.setVisibility(View.VISIBLE);
    }

    private void getBaseArguments() {
        try {
            Bundle arg = getArguments();
            userShortArrayList.clear();
            ArrayList<UserShort> us = arg.getParcelableArrayList(AppConst.CNC_USER_SHORT_ARRAY);
            userShortArrayList.addAll(us);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    // Called in Android UI's main thread
    public void onEventMainThread(Object object) {
        PostObject myObject = (PostObject) object;
        switch (myObject.getKey()) {

            case SmartChat.CallType.CONTACTS_GET_VALUE:
                setDataAndUpdate(((ArrayList<Contact>) myObject.getObject()));
                break;
        }
    }

    private void setDataAndUpdate(ArrayList<Contact> contactArrayList) {
        int first = contacts.size();
        if (userShortArrayList != null && !contactArrayList.isEmpty()) {
            int size = contactArrayList.size();
            for (int i = 0; i < size; i++) {
                Contact c = contactArrayList.get(i);
                SmartChat.PUUID globalPuuid = c.getContactGlobalId();
                contacts.add(new UserShort(globalPuuid.getLeastSignificantBits(),
                        globalPuuid.getMostSignificantBits(),
                        DataManager.getValueOfType(c.getFields(), SmartChat.FieldType.FIRSTNAME),
                        AppConst.LIST_DEF_IMAGE_LIST_SOURCE));
            }
            // check to coincidence
            if (userShortArrayList != null && !userShortArrayList.isEmpty()) {
                int si = contacts.size();
                int s = userShortArrayList.size();
                for (int i = 0; i < si; i++) {
                    UserShort userShort = contacts.get(i);
                    long least = userShort.getLeast();
                    long most = userShort.getMost();
                    for (int k = 0; k < s; k++) {
                        UserShort us = userShortArrayList.get(k);
                        if (least == us.getLeast() && most == us.getMost()) {
                            userShort.setStatus(true);
                        }
                    }
                }
            } else {
                getAllContactsProgressBar.setVisibility(View.GONE);
            }
        } else {
            noContactsVisibility();
        }
        if (first < contacts.size()) {
            adapter.notifyDataSetChanged();
        }
    }

    private void getAllContactOfServer() {
        // local request
        //setDataAndUpdate(ContactBook.tempPhoneContacts);
        //connectHelper.getAllContacts();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initActionBar() {
        ImageButton leftButton = (ImageButton) v.findViewById(R.id.backBtn);
        ImageButton rightButton = (ImageButton) v.findViewById(R.id.doneBtn);
        TextView headerText = (TextView) v.findViewById(R.id.titleCreateChatRoom);
        headerText.setText("All Contacts");
        //headerText.setTextColor(getResources().getColor(ColorResorces.getColor(R.attr.breezy_white)));

        leftButton.setImageResource(R.mipmap.app_back);
        rightButton.setImageResource(R.mipmap.ic_done);


        leftButton.setOnClickListener(this);
        rightButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bar_left_button:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.action_bar_right_button:
                closeFragment();
                break;
            case R.id.addNewContacts:
                openContactListFragment();
                break;
        }
    }

    private void openContactListFragment() {
        /*MainContactFragment fragment = new MainContactFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, fragment);
        ft.commit();*/
        //getActivity().getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private ArrayList<UserShort> getMarkedArrayContacts(ArrayList<UserShort> contacts) {
        ArrayList<UserShort> markedContacts = new ArrayList<>();
        int size = contacts.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                UserShort us = contacts.get(i);
                if (us.isStatus()) {
                    markedContacts.add(us);
                }
            }
        }
        return markedContacts;
    }

    private void closeFragment() {
        NewUserShortArrayListener fr = (NewUserShortArrayListener) getTargetFragment();
        userShortArrayList.clear();
        userShortArrayList.addAll(getMarkedArrayContacts(contacts));
        if (userShortArrayList != null && !userShortArrayList.isEmpty()) {
            fr.onFinishGetContact(userShortArrayList);
            getActivity().getSupportFragmentManager().popBackStack();
        } else {
            Toast.makeText(getActivity(), R.string.chat_no_selected_contact, Toast.LENGTH_SHORT).show();
        }
    }

    private void setAdapter() {
        getIndexList(contacts);
        adapter = new ContactsForChatAdapter(v.getContext(), contacts, mapIndex);
        myContactList.setAdapter(adapter);
        myContactList.setFastScrollEnabled(true);
        myContactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
    }

    // get ABC to scroll
    private void getIndexList(ArrayList<UserShort> contacts) {
        mapIndex = new LinkedHashMap<String, Integer>();
        if (contacts != null) {
            for (int i = 0; i < contacts.size(); i++) {
                String name = contacts.get(i).getName();
                String index = String.valueOf(Character.toUpperCase(name.charAt(0)));

                if (mapIndex.get(index) == null)
                    mapIndex.put(index, i);
            }
        }
    }
}
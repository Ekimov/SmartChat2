package com.smartchat.contacts;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartchat.R;
import com.smartchat.utils.AppConst;

/**
 * Created by tropic4 on 14.04.15.
 */
public class ContactFragment extends Fragment {
    private int pageNumber;
    private View view;


    public Fragment newInstance(int page) {
        ContactFragment pageFragment = new ContactFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(AppConst.ARGUMENT_PAGE_NUMBER, page);
       // arguments.putSerializable("key",);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.chats_chat_list_fragment, container, false);

        pageNumber = getArguments().getInt(AppConst.ARGUMENT_PAGE_NUMBER);


        return view;
    }



}

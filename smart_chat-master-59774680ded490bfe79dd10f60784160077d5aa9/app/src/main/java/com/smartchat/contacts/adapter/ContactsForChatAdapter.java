package com.smartchat.contacts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.objects.UserShort;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class ContactsForChatAdapter extends BaseAdapter implements SectionIndexer {
    private Context mContext;
    private final ArrayList<UserShort> array;
    private Map<String, Integer> mapIndex;
    String[] sections;


    public ContactsForChatAdapter(Context c, ArrayList<UserShort> array, Map<String, Integer> mapIndex) {
        mContext = c;
        this.array = array;
        this.mapIndex = mapIndex;

        Set<String> sectionLetters = mapIndex.keySet();
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);

        sections = new String[sectionList.size()];
        sectionList.toArray(sections);
    }

    @Override
    public int getCount() {
        if (array !=null)
            return array.size();
        else return 0;
    }

    @Override
    public Object getItem(int arg0) {
        return array.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int arg0, View view, ViewGroup arg2) {

        View myView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myView = inflater.inflate(R.layout.contacts_for_chat_item, null);

        TextView name = (TextView) myView.findViewById(R.id.item_name);
        name.setText(array.get(arg0).getName());
        TextView description = (TextView) myView.findViewById(R.id.item_description);
        //description.setText(DataManager.getValueOfType(array.get(arg0).getFields(), SmartChat.FieldType.));

        CheckBox checkBox = (CheckBox) myView.findViewById(R.id.item_check_box);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                array.get(arg0).setStatus(isChecked);
            }
        });
        checkBox.setTag(arg0);
        checkBox.setChecked(array.get(arg0).isStatus());

        return myView;
    }

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mapIndex.get(sections[sectionIndex]);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

}

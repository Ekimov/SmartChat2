package com.smartchat.contacts.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.contacts.adapter.ContactsForChatAdapter;
import com.smartchat.objects.Contact;
import com.smartchat.objects.Field;
import com.smartchat.objects.PostObject;
import com.smartchat.objects.UserShort;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.DataManager;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class ChangeListUsersInChatDialogFragment extends DialogFragment {

    private ArrayList<UserShort> userShortArrayList = new ArrayList<>();
    private ArrayList<Contact> contactsArrayList = new ArrayList<>();
    private Map<String, Integer> mapIndex;

    private ProgressBar getUsersListProgressBar;
    private ListView usersListListView;
    private ContactsForChatAdapter adapter;

    private ConnectHelper connectHelper = MainActivity.connectHelper;

    private long mostPUUID, leastPUUID;
    private boolean status = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBaseArguments();
        getAllUsers();
    }

    private void getBaseArguments() {
        Bundle arg = getArguments();
        try {
            mostPUUID = arg.getLong(AppConst.PUUID_MOST);
            leastPUUID = arg.getLong(AppConst.PUUID_LEAST);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getResources().getString(R.string.chat_users_list));
        View view = inflater.inflate(R.layout.chat_change_list_users_in_chat_dialog_fragment, container, false);

        init(view);
        setListener();
        setBaseVisibility();
        setAdapter(view);

        return view;
    }

    private void init(View v) {
        getUsersListProgressBar = (ProgressBar) v.findViewById(R.id.getUsersListProgressBar);
        getUsersListProgressBar.setIndeterminate(true);

        usersListListView = (ListView) v.findViewById(R.id.usersListListView);
    }

    private void setListener() {
        usersListListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
    }

    private void setBaseVisibility() {
        getUsersListProgressBar.setVisibility(View.VISIBLE);
    }

    private void setUsersListVisibility() {
        getUsersListProgressBar.setVisibility(View.GONE);
    }

    private void setAdapter(View v) {
        getIndexList(userShortArrayList);
        adapter = new ContactsForChatAdapter(v.getContext(), userShortArrayList, mapIndex);
        usersListListView.setAdapter(adapter);
        usersListListView.setFastScrollEnabled(true);
    }

    // get ABC to scroll
    private void getIndexList(ArrayList<UserShort> contacts) {
        mapIndex = new LinkedHashMap<String, Integer>();
        if (contacts != null) {
            for (int i = 0; i < contacts.size(); i++) {
                String name = contacts.get(i).getName();
                String index = String.valueOf(Character.toUpperCase(name.charAt(0)));

                if (mapIndex.get(index) == null)
                    mapIndex.put(index, i);
            }
        }
    }

    private void getAllUsers() {
        connectHelper.getAllUsersForRoomRequest(connectHelper.setPUUID(mostPUUID, leastPUUID), 1000, 0);
        connectHelper.getAllContacts();
    }

    // Called in Android UI's main thread
    public void onEventMainThread(Object object) {
        PostObject postObject = (PostObject) object;
        switch (postObject.getKey()) {
            case SmartChat.CallType.GET_ALL_USERS_FOR_ROOM_VALUE:
                refreshUsersList(getOldUsers((ArrayList<UserShort>) postObject.getObject()));
                getUsersListProgressBar.setVisibility(View.GONE);
                break;
            case SmartChat.CallType.CONTACTS_GET_VALUE:
                contactsArrayList.clear();
                contactsArrayList.addAll((ArrayList<Contact>) postObject.getObject());
                checkStatus();
                break;
        }
    }

    private void refreshUsersList(ArrayList<UserShort> users) {
        userShortArrayList.addAll(users);
        adapter.notifyDataSetChanged();

        status = !status;
        checkStatus();
    }

    private ArrayList<UserShort> getOldUsers(ArrayList<UserShort> old) {
        ArrayList<UserShort> olds = new ArrayList<>();
        int size = old.size();
        for (int i = 0; i < size; i++) {
            UserShort us = old.get(i);
            us.setStatus(true);
            olds.add(us);
        }
        return olds;
    }

    private void checkStatus() {
        if(status) {
            refreshUsersList(sortUsersList(userShortArrayList, contactsArrayList));
        }
    }

    private ArrayList<UserShort> sortUsersList(ArrayList<UserShort> usersInRoom, ArrayList<Contact> allusers) {
        ArrayList<UserShort> sortUsersList = new ArrayList<>();
        int size = allusers.size();
        for(int i = 0; i < size; i++) {
            Contact c = allusers.get(i);
            SmartChat.PUUID puuid = c.getContactGlobalId();
            int s = usersInRoom.size();
            for(int j = 0; j < s; j++) {
                if (!puuid.equals(connectHelper.setPUUID(usersInRoom.get(j).getMost(), usersInRoom.get(j).getLeast()))) {
                    ArrayList<Field> fields = c.getFields();
                    sortUsersList.add(new UserShort(puuid.getLeastSignificantBits(),
                            puuid.getMostSignificantBits(),
                            DataManager.getValueOfType(fields, SmartChat.FieldType.FIRSTNAME),
                            DataManager.getValueOfType(fields, SmartChat.FieldType.IMAGE_ID)));
                }
            }
        }
        return sortUsersList;
    }
}
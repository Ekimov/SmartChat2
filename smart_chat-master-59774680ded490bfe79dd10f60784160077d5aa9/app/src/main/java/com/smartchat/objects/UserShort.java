package com.smartchat.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class UserShort implements Parcelable {

    private long least, most;
    private String name, imageSource;
    private boolean status;

    public UserShort(long least, long most, String name, String imageSource) {
        this.least = least;
        this.most = most;
        this.name = name;
        this.imageSource = imageSource;
        this.status = false;
    }

    public long getLeast() {
        return least;
    }

    public void setLeast(long least) {
        this.least = least;
    }

    public long getMost() {
        return most;
    }

    public void setMost(long most) {
        this.most = most;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(least);
        dest.writeLong(most);
        dest.writeString(name);
        dest.writeString(imageSource);
    }

    public UserShort(Parcel parcel) {
        this.least = parcel.readLong();
        this.most = parcel.readLong();
        this.name = parcel.readString();
        this.imageSource = parcel.readString();
    }

    public static final Creator<UserShort> CREATOR = new Creator<UserShort>() {
        public UserShort createFromParcel(Parcel in) {
            return new UserShort(in);
        }
        public UserShort[] newArray(int size) {
            return new UserShort[size];
        }
    };
}
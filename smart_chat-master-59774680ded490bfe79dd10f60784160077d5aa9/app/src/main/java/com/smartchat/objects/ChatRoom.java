package com.smartchat.objects;

import com.smartchat.server.SmartChat;
public class ChatRoom {

    private SmartChat.PUUID puuidChatRoom, imageId;
    private String name, autor, lastMessage, description;
    private int type, countUnreadMessage, sequrityLevel;
    private long timeDestroy;
    private boolean lastMessageStatus;

    public ChatRoom(SmartChat.PUUID puuidChatRoom, String name) {
        this.puuidChatRoom = puuidChatRoom;
        this.name = name;
    }

    public void setPuuidChatRoom(SmartChat.PUUID puuidChatRoom) {
        this.puuidChatRoom = puuidChatRoom;
    }

    public void setImageId(SmartChat.PUUID imageId) {
        this.imageId = imageId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setCountUnreadMessage(int countUnreadMessage) {
        this.countUnreadMessage = countUnreadMessage;
    }

    public void setSequrityLevel(int sequrityLevel) {
        this.sequrityLevel = sequrityLevel;
    }

    public void setTimeDestroy(long timeDestroy) {
        this.timeDestroy = timeDestroy;
    }

    public void setLastMessageStatus(boolean lastMessageStatus) {
        this.lastMessageStatus = lastMessageStatus;
    }

    public SmartChat.PUUID getPuuidChatRoom() {
        return puuidChatRoom;
    }

    public SmartChat.PUUID getImageId() {
        return imageId;
    }

    public String getName() {
        return name;
    }

    public String getAutor() {
        return autor;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public String getDescription() {
        return description;
    }

    public int getType() {
        return type;
    }

    public int getCountUnreadMessage() {
        return countUnreadMessage;
    }

    public int getSequrityLevel() {
        return sequrityLevel;
    }

    public long getTimeDestroy() {
        return timeDestroy;
    }

    public boolean isLastMessageStatus() {
        return lastMessageStatus;
    }
}
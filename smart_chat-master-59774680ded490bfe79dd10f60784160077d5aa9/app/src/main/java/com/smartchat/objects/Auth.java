package com.smartchat.objects;

import com.smartchat.server.SmartChat;

public class Auth {

    private SmartChat.AuthType type;
    private String value;
    private boolean confirmed;

    public Auth(SmartChat.AuthType type, String value, boolean confirmed) {
        this.type = type;
        this.value = value;
        this.confirmed = confirmed;
    }

    public Auth(SmartChat.AuthType type, String value) {
        this.type = type;
        this.value = value;
    }

    public SmartChat.AuthType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public boolean isConfirmed() {
        return confirmed;
    }
}
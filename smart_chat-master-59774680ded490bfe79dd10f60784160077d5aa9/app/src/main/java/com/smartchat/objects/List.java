package com.smartchat.objects;

import com.smartchat.server.SmartChat;
import java.io.Serializable;

public class List implements Serializable{

    private SmartChat.PUUID listID;
    public String name, description, imageListSource, author;
    private int contentType, language, topicCode, rank, status, buyType;

    public List(SmartChat.PUUID _listID, String _name, String _description, String _imageListSource,
                String _author, int _contentType, int _language, int _topicCode, int _rank,
                int _status, int _buyType) {
        this.listID = _listID;
        this.name = _name;
        this.description = _description;
        this.imageListSource = _imageListSource;
        this.author = _author;
        this.contentType = _contentType;
        this.language = _language;
        this.topicCode = _topicCode;
        this.rank = _rank;
        this.status = _status;
        this.buyType = _buyType;
    }

    public SmartChat.PUUID getListID() {
        return listID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImageListSource() {
        return imageListSource;
    }

    public String getAuthor() {
        return author;
    }

    public int getContentType() {
        return contentType;
    }

    public int getLanguage() {
        return language;
    }

    public int getTopicCode() {
        return topicCode;
    }

    public int getRank() {
        return rank;
    }

    public int getStatus() {
        return status;
    }

    public int getBuyType() {
        return buyType;
    }
}
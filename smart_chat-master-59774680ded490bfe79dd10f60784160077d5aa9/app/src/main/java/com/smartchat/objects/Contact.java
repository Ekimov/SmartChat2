package com.smartchat.objects;

import com.smartchat.server.SmartChat;
import java.util.ArrayList;

public class Contact {

    private SmartChat.PUUID contactLocalId, contactGlobalId;
    private ArrayList<Field> fields;
    private boolean status;
    /*private String loginName, nickName, firstName, lastName, sex, birthDate, imageLink, email,
            phone, language, timezone, country, about, website, street, city, postCode,
            appartaments, house;*/

    public Contact(SmartChat.PUUID contactLocalId, SmartChat.PUUID contactGlobalId, ArrayList<Field> fields) {
        this.contactLocalId = contactLocalId;
        this.contactGlobalId = contactGlobalId;
        this.fields = fields;
        this.status = false;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public SmartChat.PUUID getContactLocalId() {
        return contactLocalId;
    }

    public SmartChat.PUUID getContactGlobalId() {
        return contactGlobalId;
    }

    public ArrayList<Field> getFields() {
        return fields;
    }

    public void setContactLocalId(SmartChat.PUUID contactLocalId) {
        this.contactLocalId = contactLocalId;
    }

    public void setContactGlobalId(SmartChat.PUUID contactGlobalId) {
        this.contactGlobalId = contactGlobalId;
    }

    public void setFields(ArrayList<Field> fields) {
        this.fields = fields;
    }
}
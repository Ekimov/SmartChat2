package com.smartchat.objects;

import com.smartchat.server.SmartChat;
public class Field {

    private SmartChat.FieldType type;
    private String value;

    public Field(SmartChat.FieldType type, String value) {
        this.type = type;
        this.value = value;
    }

    public SmartChat.FieldType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setType(SmartChat.FieldType type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
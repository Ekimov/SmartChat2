package com.smartchat.objects;

import com.smartchat.server.SmartChat;

public class Profile {

    private SmartChat.PUUID contactId;
    private SmartChat.PUUID favoriteListId;
    private String empty = "";
    private String authType, loginName = empty, nickName = empty, firstName = empty, lastName = empty, sex = empty,
            birthDate = empty, imageLink = empty, email = empty, phone = empty, language = empty,
            timezone = empty, country = empty, about = empty, website = empty, street = empty,
            city = empty, postCode = empty, appartaments = empty, house = empty;

    public Profile(SmartChat.PUUID contactId, String loginName, String nickName, String firstName,
                   String lastName, String sex, String birthDate, String imageLink, String email,
                   String phone, String language, String timezone, String country, String about,
                   String website, String street, String city, String postCode,
                   String appartaments, String house) {
        this.contactId = contactId;
        this.loginName = loginName;
        this.nickName = nickName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.birthDate = birthDate;
        this.imageLink = imageLink;
        this.email = email;
        this.phone = phone;
        this.language = language;
        this.timezone = timezone;
        this.country = country;
        this.about = about;
        this.website = website;
        this.street = street;
        this.city = city;
        this.postCode = postCode;
        this.appartaments = appartaments;
        this.house = house;
    }

    public Profile(SmartChat.PUUID contactId) {
        this.contactId = contactId;
    }

    public Profile(){};

    public SmartChat.PUUID getFavoriteListId() {
        return favoriteListId;
    }

    public void setFavoriteListId(SmartChat.PUUID favoriteListId) {
        this.favoriteListId = favoriteListId;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public void setContactId(SmartChat.PUUID contactId) {
        this.contactId = contactId;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setAppartaments(String appartaments) {
        this.appartaments = appartaments;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public SmartChat.PUUID getContactId() {
        return contactId;
    }

    public String getLoginName() { return loginName; }

    public String getNickName() {
        return nickName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSex() {
        return sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getLanguage() {
        return language;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getCountry() {
        return country;
    }

    public String getAbout() {
        return about;
    }

    public String getWebsite() {
        return website;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getAppartaments() {
        return appartaments;
    }

    public String getHouse() {
        return house;
    }
}
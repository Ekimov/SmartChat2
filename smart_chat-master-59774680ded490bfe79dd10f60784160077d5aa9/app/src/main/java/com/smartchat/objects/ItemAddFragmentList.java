package com.smartchat.objects;

import android.graphics.drawable.Drawable;
import android.media.Image;


public class ItemAddFragmentList {
    private String nameList;
    private int labelList;

    public ItemAddFragmentList(String _nameList, int _labelList){
        this.nameList=_nameList;
        this.labelList=_labelList;
    }

    public String getNameList(){
        return nameList;
    }

    public int getLabelList(){
        return labelList;
    }
}

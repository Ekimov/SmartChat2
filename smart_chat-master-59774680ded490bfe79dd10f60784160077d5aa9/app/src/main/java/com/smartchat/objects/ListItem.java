package com.smartchat.objects;

import com.smartchat.server.SmartChat;
import java.io.Serializable;

public class ListItem implements Serializable{

    private SmartChat.PUUID listItemID;
    private String text, contentSource;
    private int contentType, rank;

    public ListItem(SmartChat.PUUID _listID, String _text, String _contentSource, int _contentType,
                    int _rank) {
        this.listItemID = _listID;
        this.text = _text;
        this.contentSource = _contentSource;
        this.contentType = _contentType;
        this.rank = _rank;
    }

    public SmartChat.PUUID getListItemID() {
        return listItemID;
    }

    public String getText() {
        return text;
    }

    public String getContentSource() {
        return contentSource;
    }

    public int getContentType() {
        return contentType;
    }

    public int getRank() {
        return rank;
    }
}
package com.smartchat.objects;

import com.smartchat.server.SmartChat;
import com.smartchat.server.SmartChat.PUUID;
public class MessageBody {

    private int type;
    private String data;
    private long date;
    private PUUID check, from, chatRoom;

    public MessageBody(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getData() {
        return data;
    }

    public long getDate() {
        return date;
    }

    public PUUID getCheck() {
        return check;
    }

    public PUUID getFrom() {
        return from;
    }

    public PUUID getChatRoom() {
        return chatRoom;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setCheck(PUUID check) {
        this.check = check;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setFrom(PUUID from) {
        this.from = from;
    }

    public void setChatRoom(PUUID chatRoom) {
        this.chatRoom = chatRoom;
    }

    /*public enum SCMessageDataType {
        SCMessageDataTypeText,// чистый текст (в data строка)
        SCMessageDataTypeHtml,// текст форматированный под html  (в data html строки)
        SCMessageDataTypeAudio, // короткий звук  (в data звук)
        SCMessageDataTypeVideo, // короткое видео (в data видео)
        SCMessageDataTypePhoto, // небольшая фотография (в data фотография)
        SCMessageDataTypeAudioLink, // ссылка для загрузки звука (в data строка ссылки на звук)
        SCMessageDataTypeVideoLink, // ссылка для загрузки видео (в data строка ссылки на видео)
        SCMessageDataTypePhotoLink // ссылка для загрузки фото (в data строка ссылки на фото)
    };*/
}
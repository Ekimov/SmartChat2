package com.smartchat.objects;

import com.smartchat.server.SmartChat;
import java.util.ArrayList;

public class Message {

    private SmartChat.PUUID messageID, chatID;
    private ContactMessage contactMessage;
    private long securityLevel, timeDestroy, messageDate;
    private ArrayList<MessageBody> messageBodys;
    private boolean hasTimeDestroy;

    public Message(SmartChat.PUUID messageID, SmartChat.PUUID chatID,
                   ContactMessage contactMessage, long securityLevel, long timeDestroy,
                   long messageDate, ArrayList<MessageBody> messageBodys, boolean hasTimeDestroy) {

        this.messageID = messageID;
        this.chatID = chatID;
        this.contactMessage = contactMessage;
        this.securityLevel = securityLevel;
        this.timeDestroy = timeDestroy;
        this.messageDate = messageDate;
        this.messageBodys = messageBodys;
        this.hasTimeDestroy = hasTimeDestroy;
    }

    public SmartChat.PUUID getMessageID() {
        return messageID;
    }

    public SmartChat.PUUID getChatID() {
        return chatID;
    }

    public ContactMessage getContactMessage() {
        return contactMessage;
    }

    public long getSecurityLevel() {
        return securityLevel;
    }

    public long getTimeDestroy() {
        return timeDestroy;
    }

    public long getMessageDate() {
        return messageDate;
    }

    public ArrayList<MessageBody> getMessageBodys() {
        return messageBodys;
    }

    public boolean isHasTimeDestroy() {
        return hasTimeDestroy;
    }
}
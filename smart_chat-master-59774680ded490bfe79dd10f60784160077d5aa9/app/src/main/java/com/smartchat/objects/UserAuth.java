package com.smartchat.objects;

import com.smartchat.server.SmartChat;

public class UserAuth {

    private SmartChat.PUUID usPuuid;
    private Auth userAuth;

    public UserAuth(SmartChat.PUUID usPuuid, Auth userAuth) {
        this.usPuuid = usPuuid;
        this.userAuth = userAuth;
    }
    public UserAuth(){

    }

    public void setUsPuuid(SmartChat.PUUID usPuuid) {
        this.usPuuid = usPuuid;
    }

    public void setUserAuth(Auth userAuth) {
        this.userAuth = userAuth;
    }

    public SmartChat.PUUID getUsPuuid() {
        return usPuuid;
    }

    public Auth getUserAuth() {
        return userAuth;
    }
}
package com.smartchat.objects;

import com.smartchat.server.SmartChat;
public class ContactMessage {

    private SmartChat.PUUID contactID;
    private String contactNikName, contactImageLink;

    public ContactMessage(SmartChat.PUUID contactID, String contactNikName, String contactImageLink) {
        this.contactID = contactID;
        this.contactNikName = contactNikName;
        this.contactImageLink = contactImageLink;
    }

    public SmartChat.PUUID getContactID() {
        return contactID;
    }

    public String getContactNikName() {
        return contactNikName;
    }

    public String getContactImageLink() {
        return contactImageLink;
    }
}
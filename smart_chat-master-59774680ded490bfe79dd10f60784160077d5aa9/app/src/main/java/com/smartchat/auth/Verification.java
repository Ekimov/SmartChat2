package com.smartchat.auth;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.chats.ChatsPagerFragment;
import com.smartchat.objects.PostObject;
import com.smartchat.objects.Profile;
import com.smartchat.profile.ProfileFragment;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.LoginHelper;
import com.smartchat.server.NetworkConnection;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.LogTag;

import java.util.ArrayList;
import java.util.UUID;

import de.greenrobot.event.EventBus;

public class Verification extends Fragment {

    public static ConnectHelper connectHelper;
    private EditText edit_code1, edit_code2;
    private RippleView goToBreezy;
    private String code1, code2, id;
    private MainActivity activity;


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.auth_verification, null);

        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getString("id");
        }

        activity = ((MainActivity)getActivity());

        edit_code1 = (EditText) view.findViewById(R.id.edit_phone1);
        edit_code1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(edit_code1.getText().length()==3){
                    edit_code2.requestFocus();
                }

                return false;
            }
        });
        edit_code2 = (EditText) view.findViewById(R.id.edit_phone2);
        goToBreezy = (RippleView) view.findViewById(R.id.phoneVerificationButton);


        // mainPagerFragment = new MainPagerFragment();
        goToBreezy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                code1 = edit_code1.getText().toString();
                code2 = edit_code2.getText().toString();

                if(!code1.equals("")&&!code2.equals("")){
                    new LoginAsync().execute();
                }
            }
        });

        return view;
    }

    // Called in Android UI's main thread
    public void onEventMainThread(Object object) {
        PostObject postObject = (PostObject)object;

        switch (postObject.getKey()){

            case AppConst.SMS_KEY:

                String body = (String) postObject.getObject();
                String[] parts = body.split("-");
                code1 = parts[0];
                code2 = parts[1];
                edit_code1.setText(code1);
                edit_code2.setText(code2);

                //new LoginAsync().execute();

                LogTag.d(code1+ " | "+code2);
                break;
            case SmartChat.CallType.PROFILE_GET_VALUE:

                LogTag.d("Get Profile");
                Profile profile = (Profile) postObject.getObject();

                SmartChat.PUUID favoriteListId = profile.getFavoriteListId();
                UUID uuid = new UUID(favoriteListId.getMostSignificantBits(), favoriteListId.getLeastSignificantBits());

                getActivity().getSharedPreferences(AppConst.SMART_CHAT, Context.MODE_PRIVATE).edit()
                        .putString(AppConst.FAVORITE_LIST_UUID, uuid.toString())
                        .commit();

                String nickName = profile.getNickName();
                if (nickName.isEmpty()){
                   ProfileFragment profileFragment = new ProfileFragment();
                   profileFragment.firstStart = true;
                   getActivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.container, profileFragment)
                            .commit();
                } else {
                    getActivity().getSharedPreferences(AppConst.SMART_CHAT, Context.MODE_PRIVATE).edit()
                            .putString(AppConst.NICKNAME, nickName)
                            .commit();
                    openChatRooms();
                    LogTag.d(">> PROFILE LOADED -> " + nickName);
                }

                break;
        }
    }

    private void openChatRooms() {
        ChatsPagerFragment chatsPagerFragment = new ChatsPagerFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, chatsPagerFragment);
        fragmentTransaction.commit();
    }

    private class LoginAsync extends AsyncTask<Void, Void, Void> {

        ArrayList<String> list;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            list = new LoginHelper().makeLogin(AppConst.LOGIN_CONF, code1+"-"+code2, id, AppConst.PHONE);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (list.get(0).equals("0")){

                getActivity().getSharedPreferences(AppConst.SMART_CHAT, Context.MODE_PRIVATE).edit()
                        .putString("accessToken", id)
                        .putString("userId", list.get(2))
                        //.putString("favoriteListId", list.get(3))
                        .commit();

                connectHelper = MainActivity.connectHelper;
                connectHelper.connect();

                InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                try{
                    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                } catch (NullPointerException ex){ex.printStackTrace();}

                getProfile();

            }
            else {
                Toast.makeText(getActivity(), "Error: "+list.get(1), Toast.LENGTH_LONG).show();
            }

        }
    }

    private class MySleep extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            getProfile();
        }
    }

    private void getProfile(){
        if (NetworkConnection.connect) {
            LogTag.d("Go");
            connectHelper.profileFieldGetRequest();
        } else {
            LogTag.d("Sleep");
            new MySleep().execute();
        }
    }

}

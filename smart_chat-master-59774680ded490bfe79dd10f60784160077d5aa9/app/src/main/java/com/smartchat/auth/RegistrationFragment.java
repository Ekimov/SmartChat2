package com.smartchat.auth;



import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smartchat.R;
import com.smartchat.server.LoginHelper;
import com.smartchat.utils.AppConst;
import com.smartchat.utils.CustomToast;
import com.smartchat.utils.LogTag;

import java.util.ArrayList;

public class RegistrationFragment extends Fragment implements View.OnClickListener{

    private View view;
    private Button sendBtn, changeReg;
    private TextView headTxt, verTxt, textAccept;
    private EditText number, email;
    private Spinner countrySelect;
    private ProgressBar progressBar;
    private CheckBox acceptCheck;
    private MobileInfo mobileInfo;
    private boolean isPhone = true, firstRun = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.auth_registration_fragment, container, false);

        mobileInfo = new MobileInfo(getActivity());

        init();
        initCoutrySpinner();

        if (!mobileInfo.phoneNumber.isEmpty()){
            number.setText(mobileInfo.phoneNumber);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        typeReg(isPhone);
    }

    public void init(){

        //region button

        sendBtn = (Button) view.findViewById(R.id.registration_send_btn);
        changeReg = (Button) view.findViewById(R.id.registration_change_btn);
        sendBtn.setOnClickListener(this);
        changeReg.setOnClickListener(this);

        //endregion

        //region TextView

        headTxt = (TextView) view.findViewById(R.id.registration_head);
        verTxt = (TextView) view.findViewById(R.id.registration_verification_text);

        textAccept = (TextView) view.findViewById(R.id.textAccept);

        //endregion

        acceptCheck = (CheckBox) view.findViewById(R.id.registration_accept_check);
        ViewGroup.LayoutParams lp =acceptCheck.getLayoutParams();
        lp.height=((int)textAccept.getTextSize());
        lp.width=((int)textAccept.getTextSize());
        acceptCheck.setLayoutParams(lp);

        number = (EditText) view.findViewById(R.id.registration_number);

        email = (EditText) view.findViewById(R.id.registration_email);

        progressBar = (ProgressBar) view.findViewById(R.id.registration_progress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(R.color.day_action_bar), PorterDuff.Mode.SRC_IN);
    }

    private void initCoutrySpinner(){
        countrySelect = (Spinner) view.findViewById(R.id.registration_country);
        ArrayAdapter<String> adapter = mobileInfo.getCountryNames();
        countrySelect.setAdapter(adapter);

        if(!mobileInfo.country.isEmpty()){
            countrySelect.setSelection(adapter.getPosition(mobileInfo.country));
        } else if (!mobileInfo.countryByIso.isEmpty()){
            countrySelect.setSelection(adapter.getPosition(mobileInfo.countryByIso));
        }

        countrySelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!firstRun){
                    number.setText(mobileInfo.countryHashMap.get(adapterView.getSelectedItem().toString()));
                }
                firstRun = false;
                number.setSelection(number.getText().length());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.registration_send_btn:
                if(acceptCheck.isChecked()){
                    sendAuth();
                }else {
                    CustomToast.show(getActivity(), view.getResources().getString(R.string.accept_error));
                }
                return;
            case R.id.registration_change_btn:
                isPhone = !isPhone;
                typeReg(isPhone);
        }
    }

    private void sendAuth(){
        progressBar.setVisibility(View.VISIBLE);
        sendBtn.setEnabled(false);
        sendBtn.setBackgroundResource(R.color.day_action_bar_dark);

        LoginAsync loginAsync = new LoginAsync();

        if(isPhone){
            LogTag.d( "send auth -> phone");
            String _number = number.getText().toString();
            if(_number.length() <= 3){
                CustomToast.show(getActivity(), view.getResources().getString(R.string.number_error));
                return;
            }
            loginAsync.execute(AppConst.PHONE);
        }else {
            LogTag.d("send auth -> email");
            String _email = email.getText().toString();

            if(_email.length() <= 3 || !_email.contains("@")){
                CustomToast.show(getActivity(), view.getResources().getString(R.string.email_error));
                return;
            }
            loginAsync.execute(AppConst.EMAIL);
        }
    }

    private void typeReg(boolean phone){
        if(phone){
            email.setVisibility(View.GONE);
            number.setVisibility(View.VISIBLE);
            countrySelect.setVisibility(View.VISIBLE);

            headTxt.setText(view.getResources().getString(R.string.phone_register));
            verTxt.setText(view.getResources().getString(R.string.send_sms_verification_text));
            changeReg.setText(view.getResources().getString(R.string.email_button));
        } else {
            email.setVisibility(View.VISIBLE);
            number.setVisibility(View.GONE);
            countrySelect.setVisibility(View.GONE);

            headTxt.setText(view.getResources().getString(R.string.email_register));
            verTxt.setText(view.getResources().getString(R.string.send_email_verification_text));
            changeReg.setText(view.getResources().getString(R.string.phone_button));
        }
    }

    private class LoginAsync extends AsyncTask<Integer, Void, Void> {

        ArrayList<String> list;

        @Override
        protected Void doInBackground(Integer... params) {

            if (params[0] == AppConst.PHONE){
                list = new LoginHelper().makeLogin(AppConst.LOGIN_REG, number.getText().toString(), null, params[0]);
            }else if(params[0] == AppConst.EMAIL){
                list = new LoginHelper().makeLogin(AppConst.LOGIN_REG, email.getText().toString(), null, params[0]);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (list.get(0).equals("0")){

                Verification phone_verification = new Verification();
                Bundle bundle = new Bundle();
                bundle.putString("id", list.get(1));
                phone_verification.setArguments(bundle);

                getActivity().getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,
                                R.anim.slide_back_in_left, R.anim.slide_back_out_right)
                        .replace(R.id.container, phone_verification)
                        .addToBackStack(null)
                        .commit();
            }
            else {
                Toast.makeText(getActivity(), "Error: " + list.get(1), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);
                sendBtn.setEnabled(true);
                sendBtn.setBackgroundResource(R.color.day_action_bar);
            }
        }

    }
}

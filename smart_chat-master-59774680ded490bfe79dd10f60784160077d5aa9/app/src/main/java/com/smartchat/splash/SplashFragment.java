package com.smartchat.splash;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.chats.ChatsPagerFragment;
import com.smartchat.server.ConnectHelper;
import com.smartchat.utils.AppConst;

import java.io.File;

public class SplashFragment extends Fragment {

    private LinearLayout splashLogo;
    private Animation anim;
    private static File rootDirectory;
    private String accessToken;
    public static ConnectHelper connectHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.splash_base_layout, container, false);

        ((MainActivity)getActivity()).menuBarIsGone();

        init(v);
        createDirectory();
        waitAnim();

        return v;
    }

    private void init(View view) {
        splashLogo = (LinearLayout) view.findViewById(R.id.splashLogo);

        anim = AnimationUtils.loadAnimation(view.getContext(), R.anim.splash_anim);
        splashLogo.setAnimation(anim);
    }

    private void waitAnim() {
        new Handler().postDelayed(new Runnable() {
            // Using handler with postDelayed called runnable run method
            @Override
            public void run() {
                goToNextFragment();
            }
        }, 2 * 1000); // wait for 2 seconds
    }

    private void goToNextFragment() {

        accessToken = getActivity().getSharedPreferences(AppConst.SMART_CHAT, Context.MODE_PRIVATE)
                .getString("accessToken", "");

        Log.d("TOKEN", "token -> " + accessToken);
//        if (accessToken.isEmpty()){
//            RegistrationFragment regFrag = new RegistrationFragment();
//            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.container, regFrag);
//            ft.commit();
//        } else {
//            connectHelper = MainActivity.connectHelper;
//
//            if(!NetworkConnection.isConnected()){
//                connectHelper.connect();
//            }

            ChatsPagerFragment chatsPagerFragment = new ChatsPagerFragment();
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, chatsPagerFragment);
            fragmentTransaction.commitAllowingStateLoss();
        //}
    }

    public static File getRootDirectory() {
        return rootDirectory;
    }

    private void createDirectory() {
        File directory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "SmartChat");
        if (!directory.exists())
            directory.mkdirs();
        rootDirectory = directory;
        File directoryPhoto = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/SmartChat"),
                "photo");
        if (!directoryPhoto.exists())
            directoryPhoto.mkdirs();
        File directoryVideo = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/SmartChat"),
                "video")        ;
        if (!directoryVideo.exists())
            directoryVideo.mkdirs();
        File directoryVoice = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/SmartChat"),
                "voice");
        if (!directoryVoice.exists())
            directoryVoice.mkdirs();
    }
}

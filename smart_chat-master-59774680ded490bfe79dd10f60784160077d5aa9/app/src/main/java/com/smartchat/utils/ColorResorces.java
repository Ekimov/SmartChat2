package com.smartchat.utils;

import android.content.res.Resources;
import android.util.TypedValue;

public class ColorResorces {

    private static Resources.Theme theme = null;

    public static int getColor(int attr) {
        TypedValue typedValueAttr = new TypedValue();
        theme.resolveAttribute(attr, typedValueAttr, true);
        return typedValueAttr.resourceId;
    }
}
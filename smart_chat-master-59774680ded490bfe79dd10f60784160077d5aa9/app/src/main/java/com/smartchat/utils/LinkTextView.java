package com.smartchat.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.smartchat.R;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LinkTextView extends TextView {

    TextLinkClickListener mListener;
    Pattern hashTagsPattern = Pattern.compile("(#[a-zA-Z0-9_-]+)");
    Pattern hyperLinksPattern = Pattern.compile("([Hh][tT][tT][pP][sS]?:\\/\\/[^ ,'\'>\\]\\)]*[^\\. ,'\'>\\]\\)])");
    private ArrayList<Hyperlink> listOfLinks;
    private boolean isLongLink;

    public enum Type { HASHTAG, URL }

    public LinkTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        listOfLinks = new ArrayList<>();

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.LinkTextView);

        setLinkText(typedArray.getString(R.styleable.LinkTextView_link_text));

        typedArray.recycle();
    }

    public void setLinkText(String text) {
        if ((text == null) || (text.isEmpty()))
            return;

        SpannableString linkableText = new SpannableString(text);

        gatherLinks(listOfLinks, linkableText, hashTagsPattern, Type.HASHTAG);
        gatherLinks(listOfLinks, linkableText, hyperLinksPattern, Type.URL);

        try {
            for (int i = 0; i < listOfLinks.size(); i++) {
                Hyperlink linkSpec = listOfLinks.get(i);
                //Log.d("LINKSTEST", "linkSpec -> " + linkSpec);
                linkableText.setSpan(linkSpec.span, linkSpec.start, linkSpec.end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        MovementMethod m = this.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            if (this.getLinksClickable()) {
                this.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }

        setText(linkableText);
    }

    public void setOnTextLinkClickListener(TextLinkClickListener newListener) {
        mListener = newListener;
    }

    private void gatherLinks(ArrayList<Hyperlink> links, Spannable s, Pattern pattern, Type type) {
        Matcher m = pattern.matcher(s);

        while (m.find()) {
            int start = m.start();
            int end = m.end();

            Hyperlink spec = new Hyperlink();

            spec.textSpan = s.subSequence(start, end);
            spec.span = new InternalURLSpan(spec.textSpan.toString(), type);
            spec.start = start;
            spec.end = end;

            links.add(spec);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        long eventDuration = event.getEventTime() - event.getDownTime();

        if(eventDuration > 500){
            isLongLink = true;
        } else{
            isLongLink = false;
        }
        return super.onTouchEvent(event);
    }

    public class InternalURLSpan extends ClickableSpan {
        private String clickedSpan;
        private Type type;

        public InternalURLSpan(String clickedString, Type type) {
            clickedSpan = clickedString;
            this.type = type;
        }

        @Override
        public void onClick(View textView) {
            if(!isLongLink){
                mListener.onTextLinkClick(textView, clickedSpan, type);
            }
        }
    }

    class Hyperlink {
        CharSequence textSpan;
        InternalURLSpan span;
        int start;
        int end;
    }

    public interface TextLinkClickListener {
        void onTextLinkClick(View textView, String clickedString, Type type);
    }

}

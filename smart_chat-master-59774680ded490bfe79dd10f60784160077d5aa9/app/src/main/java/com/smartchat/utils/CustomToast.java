package com.smartchat.utils;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.smartchat.R;


public class CustomToast {

    public static void show(final Context context, final String text) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable() {
                    @Override
                    public void run() {
                        LayoutInflater inflater = LayoutInflater.from(context);

                        View toastRoot = inflater.inflate(R.layout.util_custom_toast, null);

                        TextView textView = (TextView) toastRoot.findViewById(R.id.toast_text);
                        textView.setText(text);

                        Toast toast = new Toast(context);
                        toast.setView(toastRoot);

                        toast.setGravity(Gravity.BOTTOM, 0, 50);
                        toast.show();
                    }
                }
        );
    }
}

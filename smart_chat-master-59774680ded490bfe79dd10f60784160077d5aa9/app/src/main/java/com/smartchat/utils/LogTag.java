package com.smartchat.utils;

import android.util.Log;

public class LogTag {

    private static final String LOG_TAG = "LOG_TAG";

    // show log.d with message
    public static void d(String message) {
        if(AppConst.DEBUG_MODE) {
            Log.d(LOG_TAG, message);
        }
    }

    public static void w(String message) {
        if(AppConst.DEBUG_MODE) {
            Log.w(LOG_TAG, message);
        }
    }

    // show log.e with message
    public static void e(String message) {
        if(AppConst.DEBUG_MODE) {
            Log.e(LOG_TAG, message);
        }
    }

    // show log.i with message
    public static void i(String message) {
        if(AppConst.DEBUG_MODE) {
            Log.i(LOG_TAG, message);
        }
    }

    // show log.v with message
    public static void v(String message) {
        if(AppConst.DEBUG_MODE) {
            Log.v(LOG_TAG, message);
        }
    }
}
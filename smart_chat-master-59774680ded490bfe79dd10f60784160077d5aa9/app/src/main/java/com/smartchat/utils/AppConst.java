package com.smartchat.utils;

import android.util.Log;

public class AppConst {

    public static final boolean DEBUG_MODE = true;

    public static final String SERVER_IP = "54.148.194.39";

    //Servers
    public static final String RELEASE = "ws://" + SERVER_IP + "/api/v1/chat?accessToken=";
    public static final String PRE_RELEASE = "ws://54.93.222.229/api/v1/chat?accessToken=";

    public static final String RELEASE_REG = "http://" + SERVER_IP + "/api/v1/register";
    public static final String PRE_RELEASE_REG = "http://54.93.222.229/api/v1/register";

    public static final String RELEASE_CONF = "http://" + SERVER_IP + "/api/v1/register_confirm";
    public static final String PRE_RELEASE_CONF = "http://54.93.222.229/api/v1/register_confirm";

    public static final String RELEASE_UPLOAD_IMAGE = "http://" + SERVER_IP + "/api/v1/files?accessToken=";
    public static final String RELEASE_GET_IMAGE_LINK = "http://" + SERVER_IP + "/api/v1/files/";

    //TITLE BAR CONSTANTS
    public static final int CHATS = 1;
    public static final int LISTS = 2;
    public static final int FAVORITES = 3;
    public static final int CONTACTS = 4;
    public static final int PROFILE = 5;

    //STRIP TEXT
    public static final String[] CHATS_STRIP = {"Friends", "Groups", "Global"};
    public static final String[] LISTS_STRIP = {"MyList", "Local", "Global"};

    // SMSReceiver
    public static final String SMS_ADDRESS = "SMS";
    public static final int SMS_KEY = 467;

    //CHATS PAGER
    public static final int PAGE_COUNT = 3;
    public static final String[] CHATS_PAGE_TYPE = {"Friends", "Groups", "Global"};
    public static final String ARGUMENT_PAGE_NUMBER = "page_number";

    // ListListsFragment
    public static final String SMART_CHAT = "smart";
    public static final String CAT_TAG = "CAT_TAG";
    public static final String SCREEN_FRAGMENT = "SCREEN_FRAGMENT";
    public static final String SCREEN_FRAGMENT_BACK_STACK = "SCREEN_FRAGMENT_BACK_STACK";
    public static final int CAT_ONE = 0;
    public static final int CAT_TWO = 1;
    public static final int CAT_THREE = 2;
    public static final int REQUEST_CODE = 3;
    public static final String CHILD_TAG = "CHILD_TAG";
    public static final String ITEM_CHANGE_TAG = "ITEM_CHANGE_TAG";
    public static final String ITEM_ID_CHANGE_TAG = "ITEM_ID_CHANGE_TAG";
    public static final String ADD_LIST_DIALOG = "ADD_LIST_DIALOG";
    public static enum ListType {TEXTLIST, IMAGELIST, MIXLIST};

    // EVENT BUS
    public static final int IMAGE_STATUS = 1488;
    public static final String IMAGE_ERROR = "error";

    // NIGHT MODE
    public static final String NIGHT_MODE = "NIGHT_MODE";


    //LOGIN
    public static final String LOGIN_REG = "REGISTRATION";
    public static final String LOGIN_CONF = "CONFIRMATION";

    //EMAIL or PHONE registration
    public static final int PHONE = 0;
    public static final int EMAIL = 1;

    // List type
    public static final String LIST_TYPE_TEXT = "LIST_TYPE_TEXT";
    public static final String LIST_TYPE_IMG = "LIST_TYPE_IMG";
    public static final String LIST_TYPE_MIX_IMG_TEXT = "LIST_TYPE_MIX_IMG_TEXT";

    // List content type
    public static final int LIST_CONTENT_TYPE_TEXT_ONLY = 11;
    public static final int LIST_CONTENT_TYPE_EMOTICON_ONLY = 12;
    public static final int LIST_CONTENT_TYPE_MIXED = 13;

    // List buy type
    public static final int LIST_BUY_TYPE_BOUGHT = 21;
    public static final int LIST_BUY_TYPE_GIFT = 22;
    public static final int LIST_BUY_TYPE_FREE = 23;

    // DEFAULT LIST DATA
    public static final String LIST_DEF_DESCRIPTION = "DEFAULT LIST DESCRIPTION";
    public static final String LIST_DEF_NAME = "DEFAULT NAME";
    public static final String LIST_DEF_IMAGE_LIST_SOURCE = "http://icons.iconarchive.com/icons/yellowicon/game-stars/256/Mario-icon.png";
    public static final String LIST_DEF_AUTOR = "John";
    public static final int LIST_DEF_LANGUAGE = 301;
    public static final int LIST_DEF_TOPIC_CODE = 95;
    public static final int LIST_DEF_RANK = 0;
    public static final int LIST_DEF_STATUS = 85;
    public static final int LIST_DEF_BUYTYPE = LIST_BUY_TYPE_FREE;

    // FRAGMENT TAG
    public static final String KEYBOARD_FRAGMENT="Key Fragment";
    public static final String CHAT_FRAGMENT="Chat Fragment";

    //contact fragment tabs
    public static final String APP_CONTACTS="In Smartchat";
    public static final String PHONE_CONTACTS="Without Smartchat";


    //add contact

    //get request to start activity for result ContactAaaActivity
    public static final int GET_NEW_CONTACT_TO_CHAT =1;
    public static final int GET_NEW_CONTACT_TO_MY_CONTACTS=2;

    //return select contact
    public static final int RETURN_MY_CONTACT =3;
    public static final int RETURN_NEW_BREEZY_CONTACT =4;
    public static final int RETURN_NEW_INVITE_CONTACT =5;

    //tabs constants
    public static final int MY_PHONE_CONTACTS = 6;
    public static final int GLOBAL_NUMBERS =  7;
    public static final int ALL_APP_CONTACTS =  8;
    public static final int ONLINE_APP_CONTACTS =  9;

    // def chat
    public static final String DEF_LAST_MESSAGE = "Hi!";
    public static final int DEF_LIFETIME = 1200;
    public static final int DEF_COUNT_UNREAD_MESSAGE = 17;
    public static final boolean DEF_LAST_MESSAGE_STATUS = false;

    // create new chat
    public static final String CNC_NAME = "CNC_NAME";
    public static final String CNC_IMAGE_MOST = "CNC_IMAGE_MOST";
    public static final String CNC_IMAGE_LEAST = "CNC_IMAGE_LEAST";
    public static final String CNC_DESCRIPTION = "CNC_DESCRIPTION";
    public static final String CNC_CHAT_TYPE = "CNC_CHAT_TYPE";
    public static final String CNC_SECURITY_LEVEL = "CNC_SECURITY_LEVEL";
    public static final String CNC_CREATE_STATUS = "CNC_CREATE_STATUS";
    public static final String CNC_USER_SHORT_ARRAY = "CNC_USER_SHORT_ARRAY";

    // PUUID
    public static final String PUUID_MOST = "PUUID_MOST";
    public static final String PUUID_LEAST = "PUUID_LEAST";

    // UUID
    public static final String FAVORITE_LIST_UUID = "FAVORITE_LIST_UUID";

    // USER NICKNAME
    public static final String NICKNAME = "NICKNAME";

    public static class LogTag {

        private static final String LOG_TAG = "LOG_TAG";

        // show log.d with message
        public static void d(String message) {
            if(DEBUG_MODE) {
                Log.d(LOG_TAG, message);
            }
        }

        public static void w(String message) {
            if(DEBUG_MODE) {
                Log.w(LOG_TAG, message);
            }
        }

        // show log.e with message
        public static void e(String message) {
            if(DEBUG_MODE) {
                Log.e(LOG_TAG, message);
            }
        }

        // show log.i with message
        public static void i(String message) {
            if(DEBUG_MODE) {
                Log.i(LOG_TAG, message);
            }
        }

        // show log.v with message
        public static void v(String message) {
            if(DEBUG_MODE) {
                Log.v(LOG_TAG, message);
            }
        }
    }
}

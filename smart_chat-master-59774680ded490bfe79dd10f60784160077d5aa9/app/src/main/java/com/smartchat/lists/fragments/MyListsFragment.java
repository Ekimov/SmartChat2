package com.smartchat.lists.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.lists.adapters.MyListsTextAdapter;
import com.smartchat.objects.List;
import com.smartchat.objects.ListItem;
import com.smartchat.objects.PostObject;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyListsFragment extends Fragment {

    private static final String TAG = "MyListsTAG";
    private ExpandableListView listView;
    private MyListsTextAdapter adapter;
    private ArrayList<List> listLists = new ArrayList<List>();
    private ArrayList<ListItem> listItems;
    private Map<ArrayList<List>,ArrayList<ListItem>> listMap;
    private ProgressBar progressBar;
    private ConnectHelper connectHelper =MainActivity.connectHelper;

    public static MyListsFragment newInstance(){
        MyListsFragment fragment = new MyListsFragment();
        return fragment;
    }
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
//        fill();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view =  inflater.inflate(R.layout.fragment_my_lists, container, false);
        listView = (ExpandableListView) view.findViewById(R.id.MyList_listView);
        adapter = new MyListsTextAdapter(listLists,getActivity());
        progressBar = (ProgressBar) view.findViewById(R.id.myListBar);
        getLists();

        return view;
    }


    private void getLists(){
        connectHelper.listGetRequest();
        progressBar.setVisibility(View.VISIBLE);
    }
    private void getListItems(SmartChat.PUUID puuid){
        connectHelper.listAllItemsGetRequest(puuid);
    }
    private void getItemsTask(final ArrayList<List> list){
        Thread thread = new Thread(){
            @Override
            public void run() {
                for (int i=0;i<list.size();i++){
                    try {
                        connectHelper.listAllItemsGetRequest(list.get(i).getListID());
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }
    public void onEventMainThread(Object object) {
        PostObject myObject = (PostObject)object;

        switch (myObject.getKey()){

            case SmartChat.CallType.LISTS_GET_VALUE:
                listLists.clear();
                listLists.addAll((ArrayList<List>) myObject.getObject());
                Log.d(TAG,"Array size "+listLists.size());
                Collections.reverse(listLists);
                progressBar.setVisibility(View.GONE);
                listView.setAdapter(adapter);
//                getItemsTask(listLists);
                break;
            case SmartChat.CallType.LIST_ADD_VALUE:
                getLists();
                break;
            case SmartChat.CallType.LIST_REMOVE_VALUE:
                getLists();
                break;
            case SmartChat.CallType.LIST_UPDATE_VALUE:
//                Log.d("TEST", "UPDATE");
                getLists();
                break;
            case SmartChat.CallType.LIST_ITEMS_GET_VALUE:
                break;
            case SmartChat.CallType.LIST_ITEM_UPDATE_VALUE:
                break;
            case SmartChat.CallType.LIST_ITEM_REMOVE_VALUE:
                break;
            case SmartChat.CallType.LIST_ITEM_ADD_VALUE:
                break;

        }

    }


}

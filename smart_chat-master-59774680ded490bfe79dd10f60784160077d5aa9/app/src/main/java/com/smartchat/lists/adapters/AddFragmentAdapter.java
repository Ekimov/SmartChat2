package com.smartchat.lists.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.objects.ItemAddFragmentList;

import java.util.ArrayList;


public class AddFragmentAdapter extends BaseAdapter {
    private Activity context;
    private View rowView;
    private ArrayList<ItemAddFragmentList> items;

    public AddFragmentAdapter (Activity _context, ArrayList<ItemAddFragmentList> _items) {
        this.context=_context;
        this.items= _items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      rowView=convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null){
          ViewHolder viewHolder=new ViewHolder();
           rowView = inflater.inflate(R.layout.add_fragment_item_lists, null);
            viewHolder.imView = (ImageView) rowView.findViewById(R.id.labelList);
            viewHolder.textContent=(TextView)rowView.findViewById(R.id.nameList);
            rowView.setTag(viewHolder);
        }
        ViewHolder vHolder=(ViewHolder)rowView.getTag();
        vHolder.textContent.setText(items.get(position).getNameList());
        vHolder.imView.setImageResource(items.get(position).getLabelList());

        return rowView;
    }

    public class ViewHolder {
        public ImageView imView;
        public TextView textContent;
    }
}

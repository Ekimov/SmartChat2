package com.smartchat.lists.adapters;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.objects.ListItem;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;

import java.util.ArrayList;

public class CreateItemsListAdapter extends BaseAdapter {

    private ArrayList<ListItem> items;
    private Context context;
    private MainActivity activity;
    private ConnectHelper connectHelper;
    private SmartChat.PUUID list_PUUID;

    public CreateItemsListAdapter(Context _ctx, ArrayList<ListItem> _items, SmartChat.PUUID list_PUUID) {
        this.context = _ctx;
        this.items = _items;
        activity =((MainActivity) context);
        connectHelper = activity.connectHelper;
        this.list_PUUID = list_PUUID;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View myView;
        CreateItemsListHolder holder = new CreateItemsListHolder();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            myView = inflater.inflate(R.layout.lists_create_item_list, null);
        } else {
            myView = convertView;
        }

        String item = items.get(position).getText();
        holder.title = (TextView) myView.findViewById(R.id.textItem);
        holder.del = (ImageButton) myView.findViewById(R.id.delItem);
        holder.del.setColorFilter(context.getResources().getColor(R.color.day_action_bar));
        //MainActivity.getColor(R.attr.breezy_action_bar));
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectHelper.listItemTextRemoveRequest(list_PUUID, items.get(position).getListItemID());
                //items.remove(position);
                //notifyDataSetChanged();
            }
        });

        holder.title.setText(item);

        return myView;
    }

    private static class CreateItemsListHolder {
        private TextView title;
        private ImageButton del;
    }
}
package com.smartchat.lists.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.smartchat.R;
import com.smartchat.lists.adapters.CreateItemImageListAdapter;
import com.smartchat.objects.ListItem;

import java.util.ArrayList;

/**
 * Created by anton on 07.04.15.
 */
public class CreateNewImageListFragment extends Fragment implements View.OnClickListener {


    private static final int TYPE_LOGO_PICK = 313;
    private ArrayList<ListItem> items;
    private ImageView setLogoButton;
    private EditText etNameList,etItemList;
    private ImageButton addItem;
    private ImageView doneBtn;
    private ImageView backBtn;
    private GridView listItemsGridView;
    private CreateItemImageListAdapter adapter;
    private TextView actionBarTitle;
    private Intent intent;
    private String TAG="CreateNewImageList";
    private final static int TYPE_CAMERA_PICK=113;
    private final static int TYPE_GALLERY_PICK=213;
    private Uri pictureUri;
    private Camera mCamera;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        items = new ArrayList<ListItem>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.lists_create_new_image_list_fragment, container, false);
//        ((MainActivity)getActivity()).menuGone();
        init(rootView);
        TextureView mTextureView = new TextureView(getActivity());
        mCamera = Camera.open();
        setAdapter();
        setBaseParam(getString(R.string.lists_create_list));
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.listLogoEditButton:
                intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, TYPE_LOGO_PICK);
                break;
            case R.id.action_bar_right_button:
                doneEdit();
                break;
            case R.id.action_bar_left_button:
                getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private void setBaseParam(String string) {
        actionBarTitle.setText(string);
        doneBtn.setImageResource(R.mipmap.ic_done);
    }

    private void setAdapter() {
        adapter = new CreateItemImageListAdapter(getActivity(),items);
        listItemsGridView.setAdapter(adapter);
    }

    private void doneEdit() {
        // TODO save edited item to DataBase
        getActivity().getSupportFragmentManager().popBackStack();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==getActivity().RESULT_OK) {
            switch (requestCode){
                case TYPE_CAMERA_PICK:
//                        items.add(new ListItem(pictureUri.toString()));

                    items.add(new ListItem(null,null,pictureUri.toString(),0,0));
                    // TODO additems request here
                    adapter.notifyDataSetChanged();
                    break;
                case TYPE_GALLERY_PICK:
                    Uri uri = Uri.parse(data.getData().toString());
//                        TODO addItem Request here
                        items.add(new ListItem(null,null,uri.toString(),0,0));
                        adapter.notifyDataSetChanged();
                    break;
                case TYPE_LOGO_PICK:
                ImageLoader.getInstance().displayImage(data.getData().toString(),setLogoButton);

            }
        }

    }

    private void init(View rootView) {
        setLogoButton = (ImageView) rootView.findViewById(R.id.listLogoEditButton);
        setLogoButton.setOnClickListener(this);
        etNameList = (EditText) rootView.findViewById(R.id.etNameList);
        addItem = (ImageButton) rootView.findViewById(R.id.addItem);
        addItem.setOnClickListener(new AddImageButton());
        doneBtn = (ImageView) rootView.findViewById(R.id.action_bar_right_button);

        doneBtn.setOnClickListener(this);
        backBtn = (ImageView) rootView.findViewById(R.id.action_bar_left_button);
        backBtn.setOnClickListener(this);
        listItemsGridView = (GridView) rootView.findViewById(R.id.imagesGridView);
        actionBarTitle = (TextView) rootView.findViewById(R.id.action_bar_title);


    }



    private class AddImageButton implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(getResources().getString(R.string.add_image_alert));

            alertDialog.setMessage(getResources().getString(R.string.choose_alert));

            alertDialog.setIcon(R.mipmap.ic_media);

            alertDialog.setPositiveButton(getResources().getString(R.string.make_alert),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            File file = new File(MainActivity.getRootDirectory() + "/photo/" + "photo_"
//                                    + System.currentTimeMillis() + ".jpg");
//                            pictureUri = Uri.fromFile(file);
//                            Log.d(TAG, "Camera Uri " + pictureUri);
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
                            startActivityForResult(intent, TYPE_CAMERA_PICK);
                        }
                    });

            alertDialog.setNegativeButton(getResources().getString(R.string.gallery_alert),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, TYPE_GALLERY_PICK);
                        }
                    });

            alertDialog.show();
        }
    }


}

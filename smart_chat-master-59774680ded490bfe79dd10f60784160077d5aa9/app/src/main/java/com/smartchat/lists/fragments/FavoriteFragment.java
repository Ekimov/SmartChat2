package com.smartchat.lists.fragments;


import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.smartchat.R;
import com.smartchat.actionbar.TitleBar;
import com.smartchat.activity.MainActivity;
import com.smartchat.lists.adapters.FavoriteListAdapter;
import com.smartchat.objects.ListItem;
import com.smartchat.objects.PostObject;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;

import java.util.ArrayList;
import java.util.UUID;

import de.greenrobot.event.EventBus;

public class FavoriteFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemLongClickListener{

    private static ConnectHelper connectHelper;
    private View view;
    private ListView favorList;
    private ArrayList<ListItem> arrayList = new ArrayList<>();
    private FavoriteListAdapter favorAdapter;
    private SmartChat.PUUID list_PUUID;
    private ProgressBar progressBar;
    private FrameLayout decorView;
    private TextView title;
    private ImageView title_image_left, title_image_right;
    private TitleBar titleBar;
    private int swipePos = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.lists_favorite_fragment, container, false);
        connectHelper = MainActivity.connectHelper;

        ((MainActivity)getActivity()).menuBarIsVisible();

        init();
        setActionBar();

        if (getUUID()) {
            connectHelper.listItemTextAddRequest(list_PUUID, "1 test #hash tag and http://google.com  web link");
            connectHelper.listItemTextAddRequest(list_PUUID, "2 test #hash tag and http://google.com  web link");
            connectHelper.listItemTextAddRequest(list_PUUID, "3 test #hash tag and http://google.com  web link");
/*            connectHelper.listItemTextAddRequest(list_PUUID, "1");
            connectHelper.listItemTextAddRequest(list_PUUID, "2");
            connectHelper.listItemTextAddRequest(list_PUUID, "3");*/
            getListData();
        }

        favorAdapter = new FavoriteListAdapter(arrayList, getActivity(), connectHelper, list_PUUID);

        favorList.setAdapter(favorAdapter);

        return view;
    }

    public void onEventMainThread(Object object) {
        PostObject myObject = (PostObject) object;

        switch (myObject.getKey()) {
            case SmartChat.CallType.LIST_ITEMS_GET_VALUE:
                arrayList.clear();
                arrayList.addAll((ArrayList<ListItem>) myObject.getObject());
                favorAdapter.notifyDataSetChanged();
                closeAll();
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case SmartChat.CallType.LIST_ITEM_UPDATE_VALUE:
                getListData();
                break;
            case SmartChat.CallType.LIST_ITEM_REMOVE_VALUE:
                getListData();
                break;
            case SmartChat.CallType.LIST_ITEM_ADD_VALUE:
                getListData();
                break;
        }
    }

    private boolean getUUID() {
        String str_uuid = getActivity().getSharedPreferences(AppConst.SMART_CHAT, Context.MODE_PRIVATE)
                .getString(AppConst.FAVORITE_LIST_UUID, "");

        if (str_uuid.isEmpty())
            return false;
        try {
            UUID uuid = UUID.fromString(str_uuid);
            list_PUUID = connectHelper.setPUUID(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits());
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private void getListData() {
        connectHelper.listAllItemsGetRequest(list_PUUID);
       // progressBar.setVisibility(View.VISIBLE);

    }

    private void init() {
        favorList = (ListView) view.findViewById(R.id.lists_favorite_listview);

//        progressBar = (ProgressBar) view.findViewById(R.id.lists_favorite_progress);
//        progressBar.getIndeterminateDrawable().setColorFilter(getResources()
//                .getColor(R.color.day_action_bar), PorterDuff.Mode.SRC_IN);
        setProgressBar();
        title = (TextView) view.findViewById(R.id.title);
        title_image_left = (ImageView) view.findViewById(R.id.title_image_left);
        title_image_right = (ImageView) view.findViewById(R.id.title_image_right);

        Log.d("addlist", "getTitle Height ");

        favorList.setOnItemLongClickListener(this);
    }

    private void setActionBar(){
        titleBar = new TitleBar(title, title_image_left, title_image_right, AppConst.FAVORITES);
    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        closeAll();
        int pos = position - favorList.getFirstVisiblePosition();

        if(swipePos != pos){
            ((SwipeLayout) (favorList.getChildAt(position - favorList.getFirstVisiblePosition()))).open(true);
            swipePos = pos;
        }else{
            swipePos = -1;
        }
        return true;
    }

    public void closeAll() {
        try {
            for (int i = 0; i < favorList.getLastVisiblePosition() - favorList.getFirstVisiblePosition() + 1; i++) {
                ((SwipeLayout) (favorList.getChildAt(i))).close(true);
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }
    public void setProgressBar(){
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleHorizontal);
      /// progressBar.setVisibility(View.INVISIBLE);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1000));
        progressBar.setIndeterminate(true);
        decorView = (FrameLayout)getActivity().getWindow().getDecorView();
        decorView.addView(progressBar);

        ViewTreeObserver observer = progressBar.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                View contentView = decorView.findViewById(android.R.id.content);
                progressBar.setY(contentView.getY()+90);
                ViewTreeObserver observer = progressBar.getViewTreeObserver();
                observer.removeGlobalOnLayoutListener(this);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        progressBar.setVisibility(View.INVISIBLE);
    }


}



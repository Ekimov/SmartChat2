package com.smartchat.lists.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import com.smartchat.objects.Comment;
import com.smartchat.objects.List;
import com.smartchat.objects.ListItem;

import java.util.ArrayList;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by anton on 08.04.15.
 */
public class MyListsTextAdapter extends BaseExpandableListAdapter {
    private ArrayList<List> listsList;
//    private ArrayList<ListItem> itemArrayList;
    private Map<List,ArrayList<ListItem>> listMap;
    private Context context;
    private LayoutInflater inflater;

    public MyListsTextAdapter(ArrayList<List> itemArrayList, Context context) {
        this.listsList = itemArrayList;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return listsList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listMap.get(listsList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listsList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listMap.get(listsList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_expandable_list_item_1, null);
        TextView listTitle = (TextView) convertView.findViewById(android.R.id.text1);
        listTitle.setText(listsList.get(groupPosition).getName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,null);
        TextView title = (TextView) convertView.findViewById(android.R.id.text1);
        title.setText(listMap.get(listsList.get(groupPosition)).get(childPosition).getText());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}

package com.smartchat.lists.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.smartchat.R;
import com.smartchat.lists.adapters.AddFragmentAdapter;
import com.smartchat.objects.ItemAddFragmentList;
import com.smartchat.utils.AppConst;

import java.util.ArrayList;

/**
 * Created by anton on 07.04.15.
 */
public class AddListDialog extends DialogFragment  {

    private ArrayList<ItemAddFragmentList> items;
    private ListView lvLabel;
    private AddFragmentAdapter adapter;
    int [] image = {R.mipmap.ic_playlist_add,R.mipmap.ic_download, R.mipmap.ic_purchase};
    String [] nameList ={"Create","Get","Purchased"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        items = new ArrayList();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getResources().getString(R.string.add_list));

        View rootView = inflater.inflate(R.layout.lists_add_list_dialog_fragment, container, false);
        init(rootView);
        setItemsData();
        setListener();

        return rootView;
    }

    private void setListener() {
        lvLabel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("addlist", "OnItemClik");
                showTypeListDialog(position);
            }
        });
    }

    private void init(View rootView) {
        lvLabel=(ListView)rootView.findViewById(R.id.listLabel);
        adapter=new AddFragmentAdapter(getActivity(),items);
        lvLabel.setAdapter(adapter);

    }
    private void setItemsData() {
        for(int i=0;i< nameList.length; i++){
            ItemAddFragmentList item = new ItemAddFragmentList(nameList[i],image[i]);
            items.add(item);
            adapter.notifyDataSetChanged();
        }
    }
    private void showTypeListDialog(int position) {
        getDialog().cancel();
        switch (position) {
            case 0:
                DialogFragment dialog = new TypeNewListDialog();
                dialog.show(getActivity().getSupportFragmentManager(), AppConst.ADD_LIST_DIALOG);
                break;
            case 1:
                Toast.makeText(getActivity(), "getNewList", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(getActivity(), "purchasedNewList", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}

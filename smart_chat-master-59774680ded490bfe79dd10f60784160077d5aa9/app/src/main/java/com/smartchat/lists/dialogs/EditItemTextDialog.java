package com.smartchat.lists.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;

public class EditItemTextDialog extends DialogFragment {

    private EditText etTextItemList;
    private ImageButton saveItemList;
    private String content;
    private int itemId;
    private SmartChat.PUUID list_PUUID, item_PUUID;
    private ConnectHelper connectHelper = MainActivity.connectHelper;


    public interface EditNameDialogListener {
        void onFinishEditDialog(String _inputText, int _itemId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getArgumentsData();
    }

    private void getArgumentsData() {
        Bundle arg = getArguments();
        try {
            content = arg.getString(AppConst.ITEM_CHANGE_TAG);
            itemId = arg.getInt(AppConst.ITEM_ID_CHANGE_TAG);

            long list_most = arg.getLong("listMostSignificant");
            long list_least = arg.getLong("listLeastSignificant");
            list_PUUID = connectHelper.setPUUID(list_most, list_least);

            long item_most = arg.getLong("itemMostSignificant");
            long item_least = arg.getLong("itemLeastSignificant");
            item_PUUID = connectHelper.setPUUID(item_most, item_least);

        } catch (NullPointerException e) {
            content = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lists_edit_item_text_list, container, false);

        init(view);
        setListener();
        setBaseParam();

        // set Dialog param
        getDialog().setTitle(R.string.lists_edit_item);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return view;
    }

    private void setBaseParam () {
        etTextItemList.requestFocus();
        //etTextItemList.setOnEditorActionListener(this);
        if(content != null) {
            etTextItemList.setText(content);
        } else {
            etTextItemList.setText("");
        }
    }

    private void init(View _view) {
        etTextItemList = (EditText) _view.findViewById(R.id.etTextItemList);

        saveItemList = (ImageButton) _view.findViewById(R.id.saveItemList);
    }

    private void setListener() {
        saveItemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });
    }

    private void saveChanges() {
        /*EditNameDialogListener fragment = (EditNameDialogListener) getTargetFragment();
        fragment.onFinishEditDialog(etTextItemList.getText().toString(), itemId);*/
        if (!etTextItemList.getText().toString().isEmpty()) {
            connectHelper.listItemTextUpdateRequest(list_PUUID, item_PUUID, etTextItemList.getText().toString());
        }
        this.dismiss();
    }
}
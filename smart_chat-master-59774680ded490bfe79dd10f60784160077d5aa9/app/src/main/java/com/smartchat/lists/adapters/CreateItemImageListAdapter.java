package com.smartchat.lists.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.smartchat.R;
import com.smartchat.objects.ListItem;

import java.io.IOException;
import java.util.ArrayList;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;


public class CreateItemImageListAdapter extends BaseAdapter{

    private ArrayList<ListItem> items;
    private Context context;
    private String TAG="CreateNewImageList";
    private ImageView imageView=null;
    private GifImageView gifImageView=null;
    public CreateItemImageListAdapter(Context _ctx, ArrayList<ListItem> _items) {
        this.context = _ctx;
        this.items = _items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.lists_create_item_image_list, null);
        imageView = (ImageView) convertView.findViewById(R.id.imageItem);
        gifImageView = (GifImageView) convertView.findViewById(R.id.gifItem);
        String  resourceFilePath = getRealPathFromURI(context, Uri.parse(items.get(position).getContentSource()));
        Log.d(TAG, "File Uri " + resourceFilePath);
        if (resourceFilePath.contains(".gif")){
            GifDrawable gifDrawable = null;
            try {
                gifDrawable = new GifDrawable(context.getContentResolver(),Uri.parse(items.get(position).getContentSource()));
                gifImageView.setImageDrawable(gifDrawable);
            } catch (IOException e) {
                e.printStackTrace();
            }catch (IllegalArgumentException i){
                i.printStackTrace();
                gifImageView.setImageResource(R.drawable.ic_launcher);
            }


            gifImageView.setVisibility(View.VISIBLE);
        }else {
            ImageLoader.getInstance().displayImage(items.get(position).getContentSource(), imageView, getDisplayImageOptions());
            imageView.setVisibility(View.VISIBLE);
        }

        ImageButton del = (ImageButton) convertView.findViewById(R.id.delItem);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (NullPointerException e){
            e.printStackTrace();
        }finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contentUri.toString();
    }


    public DisplayImageOptions getDisplayImageOptions() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .showImageOnLoading(R.drawable.ic_launcher)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        return options;
    }
}

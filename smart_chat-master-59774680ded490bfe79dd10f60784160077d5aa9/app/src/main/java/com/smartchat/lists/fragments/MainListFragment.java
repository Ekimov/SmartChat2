package com.smartchat.lists.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.lists.dialogs.AddListDialog;
import com.smartchat.profile.ProfileFragment;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;

import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainListFragment extends Fragment implements View.OnClickListener{
    private ConnectHelper connectHelper = MainActivity.connectHelper;
    private SmartChat.PUUID list_PUUID;
    private ViewPager viewPager;
    private ImageView leftBarImage,rightBarImage;
    private View firstView,secondView,thirdView;
    private LinearLayout firstLinear,secondLinear,thirdLinear;
    private TextView title,tab1,tab2,tab3;
    private String[] tabs= new String[3];
    private PagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTabs();
//        getFavouritesPuuid();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_main_list, container, false);
        init(rootView);
        setListeners();
        return rootView;
    }

    private void setListeners() {
        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);
        rightBarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMyListDialog();
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                unselectTabs();
                switch (position) {
                    case 0:
                        tab1.setTextColor(getActivity().getResources()
                                .getColor(R.color.day_action_bar));
                        firstView.setVisibility(View.VISIBLE);
                        tab1.setTypeface(Typeface.DEFAULT_BOLD);
                        rightBarImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showMyListDialog();
//                                Fragment fragment = new CreateNewListFragment();
//                                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                                ft.addToBackStack(AppConst.SCREEN_FRAGMENT);
//                                ft.replace(R.id.container, fragment);
//                                ft.commit();
                            }
                        });
                        break;
                    case 1:
                        tab2.setTextColor(getActivity().getResources()
                                .getColor(R.color.day_action_bar));
                        secondView.setVisibility(View.VISIBLE);
                        tab1.setTypeface(Typeface.DEFAULT_BOLD);
                        rightBarImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showLocalDialog();
                            }
                        });
                        break;
                    case 2:
                        tab3.setTextColor(getActivity().getResources()
                                .getColor(R.color.day_action_bar));
                        thirdView.setVisibility(View.VISIBLE);
                        tab3.setTypeface(Typeface.DEFAULT_BOLD);
                        rightBarImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showGlobalDialog();
                            }
                        });
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });
    }

    private void showLocalDialog() {

    }

    private void showMyListDialog() {
        DialogFragment dialog = new AddListDialog();
        dialog.show(getActivity().getSupportFragmentManager(), AppConst.ADD_LIST_DIALOG);
    }

    private void showGlobalDialog() {

    }

    private void init(View rootView) {
        viewPager = (ViewPager) rootView.findViewById(R.id.ListsViewPager);
        leftBarImage = (ImageView) rootView.findViewById(R.id.action_bar_left_button);
        rightBarImage = (ImageView) rootView.findViewById(R.id.action_bar_right_button);
        firstView = (View) rootView.findViewById(R.id.firstView);
        secondView = (View) rootView.findViewById(R.id.secondView);
        thirdView = (View) rootView.findViewById(R.id.thirdView);
        firstLinear = (LinearLayout) rootView.findViewById(R.id.firstLinear);
        secondLinear = (LinearLayout) rootView.findViewById(R.id.secondLinear);
        thirdLinear = (LinearLayout) rootView.findViewById(R.id.thirdLinear);
        title = (TextView) rootView.findViewById(R.id.actionbarTitle);
        title.setText(getActivity().getResources().getString(R.string.Lists));
        tab1 = (TextView) rootView.findViewById(R.id.action_bar_tab_1);
        tab1.setText(tabs[0]);
        firstLinear.setVisibility(View.VISIBLE);
        tab2 = (TextView) rootView.findViewById(R.id.action_bar_tab_2);
        tab2.setText(tabs[1]);
        secondLinear.setVisibility(View.VISIBLE);
        tab3 = (TextView) rootView.findViewById(R.id.action_bar_tab_3);
        tab3.setText(tabs[2]);
        thirdLinear.setVisibility(View.VISIBLE);
        unselectTabs();
        tab1.setTextColor(getActivity().getResources()
                .getColor((R.color.day_action_bar)));
        tab1.setTypeface(Typeface.DEFAULT_BOLD);
        firstView.setVisibility(View.VISIBLE);

        adapter = new PagerAdapter(getFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    private void setTabs() {

        tabs[0] = getActivity().getResources().getString(R.string.mylist_tab);
        tabs[1] = getActivity().getResources().getString(R.string.local_tab);
        tabs[2] = getActivity().getResources().getString(R.string.global_tab);
    }
    private void getFavouritesPuuid(){

        String favoriteListId = getActivity().getPreferences(getActivity().MODE_PRIVATE)
                .getString("favoriteListId", "");
        UUID uuid = UUID.fromString(favoriteListId);
        list_PUUID = connectHelper.setPUUID(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits());
    }
    private void unselectTabs(){
        firstView.setVisibility(View.GONE);
        secondView.setVisibility(View.GONE);
        thirdView.setVisibility(View.GONE);

        tab1.setTypeface(Typeface.DEFAULT);
        tab1.setTextColor(getActivity().getResources()
                .getColor(R.color.day_description));
        tab2.setTypeface(Typeface.DEFAULT);
        tab2.setTextColor(getActivity().getResources()
                .getColor(R.color.day_description));
        tab3.setTypeface(Typeface.DEFAULT);
        tab3.setTextColor(getActivity().getResources()
                .getColor(R.color.day_description));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_bar_tab_1:
                viewPager.setCurrentItem(0);
                break;
            case R.id.action_bar_tab_2:
                viewPager.setCurrentItem(1);
                break;
            case R.id.action_bar_tab_3:
                viewPager.setCurrentItem(2);
                break;
            case R.id.action_bar_left_button:
                Fragment profile = new ProfileFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_back_in_left, R.anim.slide_back_out_right,
                        R.anim.slide_in_left, R.anim.slide_out_right);
                transaction.replace(R.id.container, profile);
                transaction.addToBackStack("tag");
                transaction.commit();
                break;
        }
    }
    private class PagerAdapter extends FragmentPagerAdapter{
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i){
                case 0:
                    return new MyListsFragment().newInstance();
                case 1:
                    return new LocalFragment().newInstance();
                case 2:
                    return new GlobalFragment().newInstance();
            }
            return new Fragment();
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}

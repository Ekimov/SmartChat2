package com.smartchat.lists.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.lists.adapters.CreateItemsListAdapter;
import com.smartchat.lists.dialogs.EditItemTextDialog;
import com.smartchat.objects.ListItem;
import com.smartchat.objects.PostObject;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class CreateNewListFragment extends Fragment implements EditItemTextDialog.EditNameDialogListener {

    private static final String TAG = "Lists TAG";
    private EditText name, description, item;
    private TextView childrenScreenTitle;
    private ImageView addItem, doneBtn;
    private ListView itemsList;
    private ArrayList<ListItem> items;
    private CreateItemsListAdapter adapter;
    private int pos;
    private ProgressBar PuuidProgressBar;
    private ConnectHelper connectHelper = MainActivity.connectHelper;
    private SmartChat.PUUID list_PUUID;
    private ImageView backBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        items = new ArrayList<ListItem>();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }



    @Override
    public void onPause() {
        super.onPause();
        if (name.getText().toString().isEmpty()){
            if (list_PUUID!=null) {
                connectHelper.listRemoveRequest(list_PUUID);
            }
        }
        else {
            if (list_PUUID!=null)
                connectHelper.listUpdateRequest(list_PUUID, name.getText().toString(),description.getText().toString());
        }
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.lists_create_new_list_fragment, container, false);

        connectHelper.listAddRequest("","");
//        ((MainActivity)getActivity()).menuGone();

        init(rView);
        setListener();
        setOnItemClickListener();
        //setAdapter();
        setBaseParam(getString(R.string.lists_create_list));

        return rView;
    }

    // Called in Android UI's main thread
    public void onEventMainThread(Object object) {
        PostObject myObject = (PostObject)object;

        switch (myObject.getKey()){

            case SmartChat.CallType.LIST_ADD_VALUE:
                Log.i(TAG, "LIST_ADD_VALUE");
                list_PUUID = (SmartChat.PUUID)myObject.getObject();
                addItem.setVisibility(View.VISIBLE);
                PuuidProgressBar.setVisibility(View.GONE);
                doneBtn.setColorFilter(Color.WHITE);         ///////////////////Change to another color
                doneBtn.setEnabled(true);
                setAdapter();
                break;
            case SmartChat.CallType.LIST_ITEM_ADD_VALUE:
                Log.i(TAG,"LIST_ITEM_ADD_VALUE");
                getAllItems();
                break;
            case SmartChat.CallType.LIST_ITEMS_GET_VALUE:
                Log.i(TAG,"LIST_ITEMS_GET_VALUE");
                items.clear();
                items.addAll((ArrayList<ListItem>)myObject.getObject());
                item.setText("");
                adapter.notifyDataSetChanged();
                break;
            case SmartChat.CallType.LIST_ITEM_REMOVE_VALUE:
                Log.i(TAG,"LIST_ITEM_REMOVE_VALUE");
                getAllItems();
                break;
            case SmartChat.CallType.LIST_ITEM_UPDATE_VALUE:
                Log.i(TAG,"LIST_ITEM_UPDATE_VALUE");
                getAllItems();
                break;

        }

    }

    private void getAllItems(){
        connectHelper.listAllItemsGetRequest(list_PUUID);
    }

    private void init(View _view) {
        name = (EditText) _view.findViewById(R.id.etTitleList);
        description = (EditText) _view.findViewById(R.id.etListDescription);
        item = (EditText) _view.findViewById(R.id.etNewItemList);
        PuuidProgressBar = (ProgressBar) _view.findViewById(R.id.getPUUIDprogressBar);
        childrenScreenTitle = (TextView) _view.findViewById(R.id.title);

        addItem = (ImageButton) _view.findViewById(R.id.addNewItem);
        doneBtn = (ImageView) _view.findViewById(R.id.title_image_right);
        doneBtn.setColorFilter(Color.RED);
        doneBtn.setEnabled(false);
        backBtn = (ImageView) _view.findViewById(R.id.title_image_left);
        backBtn.setImageResource(R.mipmap.ic_back);
        itemsList = (ListView) _view.findViewById(R.id.itemsList);
    }

    private void setOnItemClickListener() {
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
                showEditItemTextListDialog(items.get(pos).getText(), pos, list_PUUID, items.get(pos).getListItemID());
            }
        };
        itemsList.setOnItemClickListener(itemClickListener);
    }

    private void setListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.addNewItem:
                        addItemToLIst();
                        break;
                    case R.id.title_image_right:
                        doneEdit();
                        break;
                    case R.id.title_image_left:
                        getActivity().onBackPressed();
                        break;
                }
            }
        };
        addItem.setOnClickListener(listener);
        doneBtn.setOnClickListener(listener);
        backBtn.setOnClickListener(listener);
    }


    private void setAdapter() {
        adapter = new CreateItemsListAdapter(getActivity(), items, list_PUUID);
        itemsList.setAdapter(adapter);
    }

    private void addItemToLIst() {
        if (!(item.getText().toString().equals(""))) {
            connectHelper.listItemTextAddRequest(list_PUUID, item.getText().toString());
/*            items.add(0, new Child(item.getText().toString()));
            item.setText("");
            adapter.notifyDataSetChanged();*/
            //connectHelper.listItemTextAddRequest(list_PUUID, item.getText().toString());
        }
    }


    private void showEditItemTextListDialog(String _edText, int _itemId, SmartChat.PUUID list_PUUID, SmartChat.PUUID item_PUUID) {
        EditItemTextDialog ed = new EditItemTextDialog();
        Bundle arg = new Bundle();
        arg.putString(AppConst.ITEM_CHANGE_TAG, _edText);
        arg.putInt(AppConst.ITEM_ID_CHANGE_TAG, _itemId);

        arg.putLong("listMostSignificant", list_PUUID.getMostSignificantBits());
        arg.putLong("listLeastSignificant", list_PUUID.getLeastSignificantBits());

        arg.putLong("itemMostSignificant", item_PUUID.getMostSignificantBits());
        arg.putLong("itemLeastSignificant", item_PUUID.getLeastSignificantBits());

        ed.setArguments(arg);
        ed.setTargetFragment(this, AppConst.REQUEST_CODE);
        ed.show(getActivity().getSupportFragmentManager(), AppConst.ADD_LIST_DIALOG);
    }

    private void setBaseParam(String _title) {
        childrenScreenTitle.setText("title");
        doneBtn.setImageResource(R.mipmap.ic_done);
    }

    private void doneEdit() {
        //getActivity().getFragmentManager().popBackStack();
        getActivity().onBackPressed();

    }

    private void changeItemList(String inputText, int itemId) {
        //items.get(itemId).setDescription(inputText);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFinishEditDialog(String _inputText, int _itemId) {
        changeItemList(_inputText, _itemId);
    }
}
package com.smartchat.lists.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.smartchat.R;
import com.smartchat.lists.adapters.AddFragmentAdapter;
import com.smartchat.lists.fragments.CreateNewImageListFragment;
import com.smartchat.lists.fragments.CreateNewListFragment;
import com.smartchat.objects.ItemAddFragmentList;
import com.smartchat.utils.AppConst;
import com.smartchat.videolists.CreateNewVideoFragment;

import java.util.ArrayList;

public class TypeNewListDialog extends DialogFragment {


    private ArrayList<ItemAddFragmentList> items;
    private ListView lvType;
    private AddFragmentAdapter adapter;
    int [] image = {R.mipmap.ic_text_list_type,R.mipmap.ic_image_list_type, R.mipmap.ic_mixed_list_type,R.mipmap.ic_mixed_list_type};
    String [] nameList ={"Text only","Image only","Mixed","Video mixed"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        items = new ArrayList();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getResources().getString(R.string.add_list));

        View rootView = inflater.inflate(R.layout.lists_add_list_dialog_fragment, container, false);
        init(rootView);
        setItemsData();
        setListener();
        return rootView;
    }

    private void setListener() {
        lvType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("addlist", "OnItemClik");
                showTypeListDialog(position);
            }
        });
    }

    private void init(View rootView) {
        lvType=(ListView)rootView.findViewById(R.id.listLabel);
        adapter=new AddFragmentAdapter(getActivity(),items);
        lvType.setAdapter(adapter);

    }
    private void setItemsData() {
        for(int i=0;i< nameList.length; i++){
            ItemAddFragmentList item = new ItemAddFragmentList(nameList[i],image[i]);
            items.add(item);
            adapter.notifyDataSetChanged();
        }
    }
    private void showTypeListDialog(int position) {
        getDialog().cancel();
        switch (position){
                case 0:
                    startCreateNewListFragment();
                    break;
                case 1:
                    startCreateNewImageListFragment();
                    Toast.makeText(getActivity(), "imageOnly", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    Toast.makeText(getActivity(), "mixedList", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    startCreateNewVideoListFragment();
                    break;
        }
    }
    private void startCreateNewVideoListFragment() {
        getDialog().cancel();
        Fragment fragment= new CreateNewVideoFragment();
        FragmentTransaction ft=getActivity().getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(AppConst.SCREEN_FRAGMENT);
        ft.replace(R.id.container,fragment);
        ft.commit();
    }

    private void startCreateNewImageListFragment() {
        getDialog().cancel();
        Fragment fragment = new CreateNewImageListFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(AppConst.SCREEN_FRAGMENT);
        ft.replace(R.id.container,fragment);
        ft.commit();

    }

    private void startCreateNewListFragment() {
        getDialog().cancel();
        Fragment fragment = new CreateNewListFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(AppConst.SCREEN_FRAGMENT);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }
}

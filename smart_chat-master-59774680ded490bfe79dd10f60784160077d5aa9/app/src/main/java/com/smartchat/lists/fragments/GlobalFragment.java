package com.smartchat.lists.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartchat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GlobalFragment extends Fragment {


    public GlobalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_global, container, false);
    }


    public Fragment newInstance() {
        GlobalFragment fragment = new GlobalFragment();
        return fragment;
    }
}

package com.smartchat.lists.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.smartchat.R;
import com.smartchat.objects.ListItem;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.CustomToast;
import com.smartchat.utils.LinkTextView;

import java.util.ArrayList;
import java.util.regex.Pattern;


public class FavoriteListAdapter extends BaseAdapter implements LinkTextView.TextLinkClickListener, View.OnClickListener{


    private ArrayList<ListItem> items;
    private Context context;
    private ConnectHelper connectHelper;
    private SmartChat.PUUID list_puuid;

    public FavoriteListAdapter(ArrayList<ListItem> items, Context context, ConnectHelper connectHelper, SmartChat.PUUID list_puuid) {
        this.items = items;
        this.context = context;
        this.connectHelper = connectHelper;
        this.list_puuid = list_puuid;
    }


    static class ViewHolder {
        private LinkTextView description;
        private ImageButton deleteBtn;
        private SwipeLayout swipeLayout;
        private ImageView item1, item2;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View myView;
        ViewHolder holder = new ViewHolder();

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            myView = inflater.inflate(R.layout.lists_favorite_item, null);
        } else {
            myView = view;
        }

        holder.description = (LinkTextView) myView.findViewById(R.id.lists_favorite_item_description);
        holder.description.setLinkText(items.get(i).getText());
        holder.description.setLinkTextColor(Color.GREEN);
        holder.description.setOnTextLinkClickListener(this);

        holder.swipeLayout = (SwipeLayout) myView.findViewById(R.id.lists_favorite_swipe_layout);
        holder.swipeLayout.setSwipeEnabled(false);


        holder.deleteBtn = (ImageButton) myView.findViewById(R.id.lists_favorite_item_delete);
        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SmartChat.PUUID item_puuid = items.get(i).getListItemID();
                if((list_puuid != null) && (item_puuid != null)){
                    connectHelper.listItemTextRemoveRequest(list_puuid, item_puuid);
                }
            }
        });


        holder.item1 = (ImageView) myView.findViewById(R.id.lists_favorite_item_button1);
        holder.item1.setOnClickListener(this);
        holder.item2 = (ImageView) myView.findViewById(R.id.lists_favorite_item_button2);
        holder.item2.setOnClickListener(this);

        return myView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lists_favorite_item_button1:
                CustomToast.show(context, "Button 1");
                break;
            case R.id.lists_favorite_item_button2:
                CustomToast.show(context, "Button 2");
                break;
        }
    }


    @Override
    public void onTextLinkClick(View textView, String clickedString, LinkTextView.Type type) {
        CustomToast.show(context, clickedString + " -> " + type);
    }

}

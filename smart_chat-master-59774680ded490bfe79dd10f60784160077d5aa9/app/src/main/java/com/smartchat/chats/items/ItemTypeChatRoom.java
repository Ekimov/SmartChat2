package com.smartchat.chats.items;

public class ItemTypeChatRoom {

    private String typeName;
    private int typeValue;

    public ItemTypeChatRoom(String typeName, int typeValue) {
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public String getTypeName() {
        return typeName;
    }

    public int getTypeValue() {
        return typeValue;
    }
}
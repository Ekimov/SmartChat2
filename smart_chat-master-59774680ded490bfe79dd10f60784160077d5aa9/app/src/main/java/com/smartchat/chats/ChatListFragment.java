package com.smartchat.chats;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.daimajia.swipe.SwipeLayout;
import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.chats.chat.ChatFragment;
import com.smartchat.objects.ChatRoom;
import com.smartchat.objects.PostObject;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ChatListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{

    private int pageNumber;
    private View view;
    private static ConnectHelper connectHelper;
    private ListView chat_list;
    private ArrayList<ChatRoom> roomList = new ArrayList();
    private ChatListAdapter chatListAdapter;
    private FragmentActivity activity;
    private int swipePos = -1;
    private ProgressBar chat_list_progress;
    private ProgressBar progressBar;
    private LinearLayout decorView;

    static ChatListFragment newInstance(int page) {
        ChatListFragment pageFragment = new ChatListFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(AppConst.ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        view = inflater.inflate(R.layout.chats_chat_list_fragment, container, false);

        pageNumber = getArguments().getInt(AppConst.ARGUMENT_PAGE_NUMBER);
        Log.d("Page", pageNumber+"");

        init();
        getAllRooms();
        return view;
    }

    private void init(){

        connectHelper = MainActivity.connectHelper;
        activity = getActivity();

        progressBar = ChatsPagerFragment.chat_list_progress;

        chat_list = (ListView) view.findViewById(R.id.chat_list);
        chatListAdapter = new ChatListAdapter(activity, roomList);
        chat_list.setAdapter(chatListAdapter);
        chat_list.setOnItemClickListener(this);
        chat_list.setOnItemLongClickListener(this);
    }

    public void onEventMainThread(Object object) {
        PostObject postObject = (PostObject) object;

        switch (postObject.getKey()){

            case SmartChat.CallType.GET_ALL_ROOMS_FOR_USER_VALUE:
                roomList.clear();
                roomList.addAll((ArrayList<ChatRoom>) postObject.getObject());
                progressBar.setVisibility(View.INVISIBLE);
                chatListAdapter.notifyDataSetChanged();
                break;
            case SmartChat.CallType.REMOVE_CHATROOM_EVENT_VALUE:

                getAllRooms();
                break;
            case SmartChat.CallType.CHANGE_INFO_CHATROOM_EVENT_VALUE:

                break;
            case SmartChat.CallType.LEAVE_USER_FROM_ROOM_EVENT_VALUE:

                break;
            case SmartChat.CallType.JOIN_USER_TO_ROOM_EVENT_VALUE:

                break;
        }
    }

    private void getAllRooms() {
        connectHelper.getAllRoomsForUserRequest(20, 0);
        //chat_list_progress.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
      // setProgressBar();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("Test", position+"");
        ChatRoom chatRoom = roomList.get(position);
        Bundle bundle = new Bundle();
        bundle.putLong(AppConst.PUUID_MOST, chatRoom.getPuuidChatRoom().getMostSignificantBits());
        bundle.putLong(AppConst.PUUID_LEAST, chatRoom.getPuuidChatRoom().getLeastSignificantBits());
        Fragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        closeAll();
        int pos = position - chat_list.getFirstVisiblePosition();

        if(swipePos != pos){
            ((SwipeLayout) (chat_list.getChildAt(position - chat_list.getFirstVisiblePosition()))).open(true);
            swipePos = pos;
        }else{
            swipePos = -1;
        }

        return true;
    }

    public void closeAll() {
        try {
            for (int i = 0; i < chat_list.getLastVisiblePosition() - chat_list.getFirstVisiblePosition() + 1; i++) {
                ((SwipeLayout) (chat_list.getChildAt(i))).close(true);
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public void setProgressBar(){
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,    ViewGroup.LayoutParams.MATCH_PARENT));
        progressBar.setIndeterminate(true);
        decorView = (LinearLayout)getActivity().getWindow().getDecorView();
        decorView.addView(progressBar);

        ViewTreeObserver observer = progressBar.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                View contentView = decorView.findViewById(android.R.id.content);
                progressBar.setY(contentView.getY()+95);

                ViewTreeObserver observer = progressBar.getViewTreeObserver();
                observer.removeGlobalOnLayoutListener(this);
            }
        });
    }
    @Override
    public void onStop() {
        super.onStop();
         progressBar.setVisibility(View.INVISIBLE);
    }

    }

package com.smartchat.chats.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.chats.items.ItemSecurityChatRoom;

import java.util.ArrayList;

public class SpinnerSecurityCreateChatRoomAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ItemSecurityChatRoom> typeChatRooms;

    public SpinnerSecurityCreateChatRoomAdapter(Context context, ArrayList<ItemSecurityChatRoom> typeChatRooms) {
        this.context = context;
        this.typeChatRooms = typeChatRooms;
    }

    @Override
    public int getCount() {
        return typeChatRooms.size();
    }

    @Override
    public Object getItem(int position) {
        return typeChatRooms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.chat_item_spenner_create_new_chat_room, null);
        }

        TextView itemName = (TextView) convertView.findViewById(R.id.itemName);
        itemName.setText(typeChatRooms.get(position).getSecurityName());

        return convertView;
    }
}
package com.smartchat.chats;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.chats.adapters.SpinnerSecurityCreateChatRoomAdapter;
import com.smartchat.chats.adapters.SpinnerTypeCreateChatRoomAdapter;
import com.smartchat.chats.items.ItemSecurityChatRoom;
import com.smartchat.chats.items.ItemTypeChatRoom;
import com.smartchat.contacts.ContactsForChatFragment;
import com.smartchat.objects.PostObject;
import com.smartchat.objects.UserShort;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.SmartChat;
import com.smartchat.utils.AppConst;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class CreateNewChatRoomFragment extends Fragment implements View.OnClickListener, ContactsForChatFragment.NewUserShortArrayListener {

    private ArrayList<UserShort> userShortArrayList = new ArrayList<UserShort>();
    private SpinnerSecurityCreateChatRoomAdapter sequrityAdapter;
    private SpinnerTypeCreateChatRoomAdapter typeAdapter;
    private ArrayList<ItemTypeChatRoom> typeArrayList = new ArrayList<ItemTypeChatRoom>();
    private ArrayList<ItemSecurityChatRoom> sequrityArrayList = new ArrayList<ItemSecurityChatRoom>();

    private EditText etChangeChatName, etChangeChatDescription;
    private TextView countContacts;
    private Spinner chatType, securityLevel;
    private ImageButton doneBtn, backBtn, openUsersList;
    private ConnectHelper connectHelper = MainActivity.connectHelper;
    private SmartChat.PUUID room_PUUID;
    private String name, description, imageSource, author = "Test Author";
    private ArrayList<SmartChat.PUUID> members;

    private int chatTypeValue = 0;
    private int securityLevelValue = 0;
    private SmartChat.PUUID chatLogoPuuid = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).menuBarIsGone();

        // fill arrays
        typeArrayList.add(new ItemTypeChatRoom("Public", 0));
        sequrityArrayList.add(new ItemSecurityChatRoom("None", 0));
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        setEditFields(userShortArrayList.size() + "");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.chat_create_new_chat_room, container, false);

        init(rView);
        setListeners();
        setAdapter(rView);

        return rView;
    }

    // Called in Android UI's main thread
    public void onEventMainThread(Object object) {
        PostObject myObject = (PostObject) object;
        switch (myObject.getKey()) {

            case SmartChat.CallType.CREATE_CHATROOM_VALUE:
                room_PUUID = ((SmartChat.PUUID) myObject.getObject());
                break;
        }

    }

    private void setFocusFalse() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(doneBtn.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void init(View v) {
        etChangeChatName = (EditText) v.findViewById(R.id.etChangeChatName);
        etChangeChatDescription = (EditText) v.findViewById(R.id.etChangeChatDescription);

        countContacts = (TextView) v.findViewById(R.id.countContacts);

        doneBtn = (ImageButton) v.findViewById(R.id.doneBtn);
        backBtn = (ImageButton) v.findViewById(R.id.backBtn);
        openUsersList = (ImageButton) v.findViewById(R.id.openUsersList);

        chatType = (Spinner) v.findViewById(R.id.chatType);
        securityLevel = (Spinner) v.findViewById(R.id.securityLevel);
    }

    private void setListeners() {
        doneBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        openUsersList.setOnClickListener(this);

        chatType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemTypeChatRoom item = typeArrayList.get(position);
                chatTypeValue = item.getTypeValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // nothing
            }
        });
        securityLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemSecurityChatRoom item = sequrityArrayList.get(position);
                securityLevelValue = item.getSecurityValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // nothing
            }
        });
    }

    private void setAdapter(View v) {
        sequrityAdapter = new SpinnerSecurityCreateChatRoomAdapter(v.getContext(), sequrityArrayList);
        typeAdapter = new SpinnerTypeCreateChatRoomAdapter(v.getContext(), typeArrayList);

        chatType.setAdapter(typeAdapter);
        securityLevel.setAdapter(sequrityAdapter);

        chatType.setSelection(0);
        securityLevel.setSelection(0);
    }

    private void showContactFragment() {
        String chatName = etChangeChatName.getText().toString();
        String chatDescription = etChangeChatDescription.getText().toString();
        ContactsForChatFragment forChatFragment = new ContactsForChatFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString(AppConst.CNC_NAME, chatName);
        args.putInt(AppConst.CNC_CHAT_TYPE, chatTypeValue);
        args.putInt(AppConst.CNC_SECURITY_LEVEL, securityLevelValue);
        if (!userShortArrayList.isEmpty() && userShortArrayList != null) {
            args.putParcelableArrayList(AppConst.CNC_USER_SHORT_ARRAY, userShortArrayList);
        }
        if (!chatDescription.isEmpty()) {
            args.putString(AppConst.CNC_DESCRIPTION, chatDescription);
        }
        if (chatLogoPuuid != null) {
            args.putLong(AppConst.CNC_IMAGE_MOST, chatLogoPuuid.getMostSignificantBits());
            args.putLong(AppConst.CNC_IMAGE_LEAST, chatLogoPuuid.getLeastSignificantBits());
        }
        forChatFragment.setArguments(args);
        forChatFragment.setTargetFragment(this, AppConst.REQUEST_CODE);
        ft.replace(R.id.container, forChatFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void setEditFields(String countContactsValue) {
        countContacts.setText(countContactsValue);
    }

    private void fieldInit() {
        name = etChangeChatName.getText().toString();
        description = etChangeChatDescription.getText().toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.doneBtn:
                doneCreate();
                break;
            case R.id.backBtn:
                setFocusFalse();
                getActivity().onBackPressed();
                break;
            case R.id.openUsersList:
                showContactFragment();
                break;
        }
    }

    private void doneCreate() {
        fieldInit();
        //membersInit();
        if (userShortArrayList == null || userShortArrayList.isEmpty()) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.chat_invite_friends), Toast.LENGTH_SHORT).show();
        } else if (name.isEmpty()) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.chat_enter_name_notification), Toast.LENGTH_SHORT).show();
        } else {
            connectHelper.createChatRoomRequest(name, description, 200, 0, connectHelper.generatePUUID(), 0, members);
            getActivity().getSupportFragmentManager().popBackStack();
        }
        setFocusFalse();
    }

    private void fillMembers(ArrayList<UserShort> users) {
        members = new ArrayList<>();
        members.clear();
        int size = users.size();
        for (int i = 0; i < size; i++) {
            members.add(connectHelper.setPUUID(users.get(i).getMost(), users.get(i).getLeast()));
        }
    }

    @Override
    public void onFinishGetContact(ArrayList<UserShort> users) {
        userShortArrayList.clear();
        userShortArrayList.addAll(users);
        for (int i = 0; i < userShortArrayList.size(); i++) {
        }
        fillMembers(users);
        setEditFields(userShortArrayList.size() + "");
    }
}
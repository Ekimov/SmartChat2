package com.smartchat.chats;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.smartchat.R;
import com.smartchat.activity.MainActivity;
import com.smartchat.objects.ChatRoom;
import com.smartchat.server.ConnectHelper;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatListAdapter extends BaseAdapter implements View.OnClickListener{

    private Context context;
    private ArrayList<ChatRoom> chatRooms;
    private View view;
    private ViewHolder holder;
    private static ConnectHelper connectHelper;
    private int position;

    public ChatListAdapter(Context context, ArrayList<ChatRoom> chatRooms){

        this.context = context;
        this.chatRooms = chatRooms;
    }

    @Override
    public int getCount() {
        return chatRooms.size();
    }

    @Override
    public Object getItem(int position) {
        return chatRooms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        holder = new ViewHolder();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.chats_chat_item, null, false);
        } else {
            view = convertView;
        }

        this.position = position;

        init();
        setInfo();
        setListener();

        return view;
    }

    private void init(){

        connectHelper = MainActivity.connectHelper;

        holder.swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe_layout);
        holder.swipeLayout.setSwipeEnabled(false);

        holder.chat_room_name = (TextView) view.findViewById(R.id.chat_room_name);
        holder.chat_room_time_to_destroy = (TextView) view.findViewById(R.id.chat_room_time_to_destroy);
        holder.content_txt = (TextView) view.findViewById(R.id.content_txt);
        holder.arrow = (ImageView) view.findViewById(R.id.arrow);
        holder.re_post = (ImageView) view.findViewById(R.id.re_post);
        holder.delete_chat = (ImageView) view.findViewById(R.id.delete_chat);
        holder.chat_settings = (ImageView) view.findViewById(R.id.chat_settings);
        holder.chat_room_image = (CircleImageView) view.findViewById(R.id.chat_room_image);

    }

    private void setInfo(){

        ChatRoom chatRoom = chatRooms.get(position);
        holder.chat_room_name.setText(chatRoom.getName());
        holder.chat_room_time_to_destroy.setText(chatRoom.getTimeDestroy()+"");//CHANGE TD
        holder.content_txt.setText(chatRoom.getLastMessage());
    }

    private void setListener(){

        holder.delete_chat.setOnClickListener(this);
        holder.chat_settings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
//TODO ADD DIALOG
            case R.id.delete_chat:
                connectHelper.removeChatRoomRequest(chatRooms.get(position).getPuuidChatRoom());
                break;
            case R.id.chat_settings:

                break;
        }
    }

    static class ViewHolder {

        private SwipeLayout swipeLayout;
        private TextView chat_room_name, chat_room_time_to_destroy, content_txt;
        private CircleImageView chat_room_image;
        private ImageView arrow, re_post, delete_chat, chat_settings;
    }

}

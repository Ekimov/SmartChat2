package com.smartchat.chats;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartchat.R;
import com.smartchat.actionbar.StripBar;
import com.smartchat.actionbar.TitleBar;
import com.smartchat.activity.MainActivity;
import com.smartchat.profile.ProfileFragment;
import com.smartchat.utils.AppConst;

public class ChatsPagerFragment extends Fragment implements View.OnClickListener{

    private View view;
    private ViewPager pager;
    private RelativeLayout strip_1, strip_2, strip_3;
    private PagerAdapter pagerAdapter;
    private TextView title;
    private ImageView title_image_left, title_image_right;
    private TitleBar titleBar;
    private StripBar stripBar;
    public static ProgressBar chat_list_progress;
    public static ProgressBar progressBar;
    private LinearLayout decorView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((MainActivity)getActivity()).menuBarIsVisible();

        view = inflater.inflate(R.layout.chats_chats_pager_fragment, container, false);

        init();
        setActionBar();
        setListener();

        return view;
    }

    private void init(){

        pager = (ViewPager) view.findViewById(R.id.chats_pager);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                stripBar.changeStrip(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        pagerAdapter = new PagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.setPageTransformer(true, new ZoomOutPageTransformer());

        title = (TextView) view.findViewById(R.id.title);
        title_image_left = (ImageView) view.findViewById(R.id.title_image_left);
        title_image_right = (ImageView) view.findViewById(R.id.title_image_right);

//       chat_list_progress = (ProgressBar) view.findViewById(R.id.chat_list_progress);
//        chat_list_progress.getIndeterminateDrawable().setColorFilter(getResources()
    //            .getColor(R.color.day_action_bar), PorterDuff.Mode.SRC_IN);
        setProgressBar();
    }

    private void setActionBar(){

        titleBar = new TitleBar(title, title_image_left, title_image_right, AppConst.CHATS);
        stripBar = new StripBar(getActivity(), view, AppConst.CHATS);
    }

    private void setListener(){

        title_image_left.setOnClickListener(this);
        title_image_right.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.title_image_left:

                Fragment fragment = new ProfileFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, fragment);
                ft.addToBackStack(null);
                ft.commit();

                break;

            case R.id.title_image_right:
                    createNewChatRoom();
                break;
        }
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {

            return new ChatListFragment().newInstance(position);
        }

        @Override
        public int getCount() {
            return AppConst.PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return AppConst.CHATS_PAGE_TYPE[position];
        }

    }

    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA +
                        (scaleFactor - MIN_SCALE) /
                                (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    private void createNewChatRoom() {
        CreateNewChatRoomFragment create = new CreateNewChatRoomFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, create);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void setProgressBar(){
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        progressBar.setIndeterminate(true);
//        decorView = (LinearLayout)getActivity().getWindow().getDecorView();
       // decorView.addView(progressBar);

        ViewTreeObserver observer = progressBar.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                View contentView = decorView.findViewById(android.R.id.content);
                progressBar.setY(contentView.getY()+180);

//                ViewTreeObserver observer = progressBar.getViewTreeObserver();
//                observer.removeGlobalOnLayoutListener(this);
            }
        });
    }
}

package com.smartchat.chats.items;

public class ItemSecurityChatRoom {

    private String securityName;
    private int securityValue;

    public ItemSecurityChatRoom(String securityName, int securityValue) {
        this.securityName = securityName;
        this.securityValue = securityValue;
    }

    public String getSecurityName() {
        return securityName;
    }

    public int getSecurityValue() {
        return securityValue;
    }
}
package com.smartchat.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.smartchat.R;
import com.smartchat.chats.ChatListFragment;
import com.smartchat.chats.ChatsPagerFragment;
import com.smartchat.lists.fragments.FavoriteFragment;
import com.smartchat.contacts.ContactsFragment;
import com.smartchat.lists.fragments.MainListFragment;
import com.smartchat.server.ConnectHelper;
import com.smartchat.server.NetworkConnection;
import com.smartchat.splash.SplashFragment;
import com.smartchat.testing.MainContactsFragment;
import com.smartchat.utils.AppConst;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements View.OnClickListener{

    public static ConnectHelper connectHelper;
    private FrameLayout container;
    private LinearLayout menu_bar;
    private RelativeLayout no_connection_layout;
    private Button no_connection_button, chats_menu_button, lists_menu_button, favorites_menu_button, contacts_menu_button;
    private ArrayList<Button> buttons;
    private ArrayList<Fragment> fragments;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity);

        initImageLoader();
        init();
        checkConnection();
        setListener();
        setMenuArrays();
        selectPosition();
    }

    private void init(){
        container = (FrameLayout) findViewById(R.id.container);
        menu_bar = (LinearLayout) findViewById(R.id.menu_bar);

        no_connection_layout = (RelativeLayout) findViewById(R.id.no_connection_layout);
        no_connection_button = (Button) findViewById(R.id.no_connection_button);

        chats_menu_button = (Button) findViewById(R.id.chats_menu_button);
        lists_menu_button = (Button) findViewById(R.id.lists_menu_button);
        favorites_menu_button = (Button) findViewById(R.id.favorites_menu_button);
        contacts_menu_button = (Button) findViewById(R.id.contacts_menu_button);
    }

    private void setListener(){
        no_connection_button.setOnClickListener(this);
        chats_menu_button.setOnClickListener(this);
        lists_menu_button.setOnClickListener(this);
        favorites_menu_button.setOnClickListener(this);
        contacts_menu_button.setOnClickListener(this);
    }

    private void showSplashFragment() {
        SplashFragment sf = new SplashFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, sf);
        ft.commit();
    }

    private void checkConnection(){

        if (new NetworkConnection(this).haveNetworkConnection()) {
            no_connection_layout.setVisibility(View.GONE);
            container.setVisibility(View.VISIBLE);
            online();
            showSplashFragment();
        } else {
            container.setVisibility(View.GONE);
            no_connection_layout.setVisibility(View.VISIBLE);
        }
    }

    private void online(){
        if(connectHelper == null){
            connectHelper = new ConnectHelper(this);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.no_connection_button:
                checkConnection();
                break;
            case R.id.chats_menu_button:
                chooseButton(0, new ChatsPagerFragment());
                break;
            case R.id.lists_menu_button:
                chooseButton(1, new MainListFragment());
                break;
            case R.id.favorites_menu_button:
                chooseButton(2, new FavoriteFragment());
                break;
            case R.id.contacts_menu_button:
                chooseButton(3, new MainContactsFragment());
                break;
        }
    }

    private void chooseButton(int number, Fragment fragment){

        clearButtons();
        buttons.get(number).setEnabled(false);
        buttons.get(number).setBackgroundResource(R.color.day_action_bar_dark);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    private void clearButtons(){

        int size = buttons.size();
        for (int i = 0; i < size; i++) {
            buttons.get(i).setEnabled(true);
            buttons.get(i).setBackgroundResource(R.color.day_action_bar);
        }
    }

    private void selectPosition(){
        buttons.get(0).setEnabled(false);
        buttons.get(0).setBackgroundResource(R.color.day_action_bar_dark);
    }

    private void setMenuArrays(){

        buttons = new ArrayList<>();
        buttons.add(chats_menu_button); buttons.add(lists_menu_button); buttons.add(favorites_menu_button); buttons.add(contacts_menu_button);
    }

    public void     menuBarIsVisible(){
        menu_bar.setVisibility(View.VISIBLE);
    }

    public void menuBarIsGone(){
        menu_bar.setVisibility(View.GONE);
    }

    private void initImageLoader() {
        File cacheDir = StorageUtils.getCacheDirectory(MainActivity.this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(MainActivity.this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCache(new UnlimitedDiscCache(cacheDir)) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(MainActivity.this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);
    }
}
